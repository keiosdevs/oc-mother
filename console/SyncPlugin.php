<?php
/**
 * Created by PhpStorm.
 * User: Łukasz Biały
 * URL: http://keios.eu
 * Date: 8/13/15
 * Time: 2:17 AM
 */

namespace Keios\PluginMother\Console;

use Illuminate\Console\Command;
use Keios\PluginMother\Classes\PluginPoller;
use Keios\PluginMother\Repositories\PluginRepository;
use Symfony\Component\Console\Input\InputOption;

class SyncPlugin extends Command
{
    /**
     * The console command name.
     */
    protected $name = 'exchange:sync-plugin';

    /**
     * The console command description.
     */
    protected $description = 'Triggers Exchange Plugin Sync';

    /**
     * Execute the console command.
     *
     * @throws \InvalidArgumentException
     * @throws \ApplicationException
     * @throws \Exception
     */
    public function handle()
    {
        $repo = new PluginRepository();
        $slug = $this->argument('slug');
        $plugin = $repo->getBySlug($slug);

        if (!$plugin) {
            $this->error('Plugin not found');

            return null;
        }
        $pluginPoller = new PluginPoller();
        $this->info('');
        $this->warn('Updating '. $plugin->name);
        \Cache::forget('plugin_list');
        $this->comment('');
        try {
            $logs = $pluginPoller->pollPlugin($plugin);
            foreach ($logs as $log) {
                $this->info($log);
            }
        } catch (\Exception $e) {
            \Log::error($e->getMessage().' '.$e->getTraceAsString());
            $this->error($plugin->name.' failed with '.$e->getMessage());
        }
        $this->comment('--------------------------------------------------------------');

        $this->info(\Lang::trans('keios.pluginmother::lang.messages.finished'));
    }

    /**
     * Get the console command arguments.
     */
    protected function getArguments()
    {
        return [
            ['slug', null, InputOption::VALUE_REQUIRED, 'Plugin slug']
        ];
    }

    /**
     * Get the console command options.
     */
    protected function getOptions()
    {
        return [

        ];
    }

}