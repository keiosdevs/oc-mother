<?php
/**
 * Created by PhpStorm.
 * User: Łukasz Biały
 * URL: http://keios.eu
 * Date: 8/13/15
 * Time: 2:17 AM
 */

namespace Keios\PluginMother\Console;

use Illuminate\Console\Command;
use Keios\PluginMother\Classes\AutoUpdater;
use Keios\PluginMother\Repositories\ProjectRepository;

class UpdateSpawns extends Command
{
    /**
     * The console command name.
     */
    protected $name = 'exchange:update-spawns';

    /**
     * The console command description.
     */
    protected $description = 'Triggers updates on projects with enabled autoupdate';

    public function handle(){
        $this->fire();
    }

    /**
     * Execute the console command.
     *
     * @throws \InvalidArgumentException
     */
    public function fire()
    {
        $repo = new ProjectRepository();
        $updater = new AutoUpdater();
        $projects = $repo->getAutoUpdatable();
        if (count($projects) === 0) {
            $this->info(\Lang::trans('keios.pluginmother::lang.messages.no_autoupdatable_projects'));
        }
        foreach ($projects as $project) {
            $this->info(\Lang::trans('keios.pluginmother::lang.messages.updating').' '.$project->domain);
            $response = $updater->update($project);
            if (array_key_exists(200, $response)) {
                $this->info(\Lang::trans('keios.pluginmother::lang.messages.updated'));
            } else {
                $this->error(
                    \Lang::trans('keios.pluginmother::lang.messages.not_updated_check_if').
                    $project->domain.
                    \Lang::trans('keios.pluginmother::lang.messages.responds_and_if_spawn_plugin_is_configured')
                );
            }
        }
        $this->info(\Lang::trans('keios.pluginmother::lang.messages.finished'));
    }


    /**
     * Get the console command arguments.
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     */
    protected function getOptions()
    {
        return [];
    }


}