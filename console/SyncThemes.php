<?php
/**
 * Created by PhpStorm.
 * User: Łukasz Biały
 * URL: http://keios.eu
 * Date: 8/13/15
 * Time: 2:17 AM
 */

namespace Keios\PluginMother\Console;

use Illuminate\Console\Command;
use Keios\PluginMother\Classes\ThemePoller;
use Keios\PluginMother\Repositories\ThemeRepository;

class SyncThemes extends Command
{
    /**
     * The console command name.
     */
    protected $name = 'exchange:sync-themes';

    /**
     * The console command description.
     */
    protected $description = 'Triggers Exchange Plugins Sync';

    public function handle(){
        $this->fire();
    }
    /**
     * Execute the console command.
     *
     * @throws \InvalidArgumentException
     * @throws \ApplicationException
     * @throws \Exception
     */
    public function fire()
    {
        $repo = new ThemeRepository();
        $themes = $repo->getAll();
        $themePoller = new ThemePoller();
        $this->info('');
        $this->warn('Updating all themes');
        \Cache::forget('theme_list');
        foreach ($themes as $theme) {
            $this->comment('');
            try {
                $logs = $themePoller->pollTheme($theme);
                foreach($logs as $log) {
                    $this->info($log);
                }
            } catch(\Exception $e){
                \Log::error($e->getMessage(). ' '. $e->getTraceAsString());
                $this->error($theme->name . ' failed with '. $e->getMessage());
            }
            $this->comment('--------------------------------------------------------------');

        }
        $this->info(\Lang::trans('keios.pluginmother::lang.messages.finished'));
    }


    /**
     * Get the console command arguments.
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     */
    protected function getOptions()
    {
        return [];
    }


}