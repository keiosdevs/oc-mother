<?php
/**
 * Created by PhpStorm.
 * User: Łukasz Biały
 * URL: http://keios.eu
 * Date: 8/13/15
 * Time: 2:17 AM
 */

namespace Keios\PluginMother\Console;

use Illuminate\Console\Command;
use Keios\PluginMother\Classes\ThemePoller;
use Keios\PluginMother\Repositories\ThemeRepository;
use Symfony\Component\Console\Input\InputOption;

class SyncTheme extends Command
{
    /**
     * The console command name.
     */
    protected $name = 'exchange:sync-theme';

    /**
     * The console command description.
     */
    protected $description = 'Triggers Exchange Plugin Sync';

    /**
     * Execute the console command.
     *
     * @throws \InvalidArgumentException
     * @throws \ApplicationException
     * @throws \Exception
     */
    public function handle()
    {
        $repo = new ThemeRepository();
        $slug = $this->argument('slug');
        $theme = $repo->getBySlug($slug);
        if (!$theme) {
            $this->error('Theme not found');

            return null;
        }
        $themePoller = new ThemePoller();
        $this->info('');
        $this->warn('Updating '. $theme->name);
        \Cache::forget('theme_list');
        $this->comment('');
        try {
            $logs = $themePoller->pollTheme($theme);
            foreach ($logs as $log) {
                $this->info($log);
            }
        } catch (\Exception $e) {
            \Log::error($e->getMessage().' '.$e->getTraceAsString());
            $this->error($theme->name.' failed with '.$e->getMessage());
        }
        $this->comment('--------------------------------------------------------------');

        $this->info(\Lang::trans('keios.pluginmother::lang.messages.finished'));
    }

    /**
     * Get the console command arguments.
     */
    protected function getArguments()
    {
        return [
            ['slug', null, InputOption::VALUE_REQUIRED, 'Theme slug']
        ];
    }

    /**
     * Get the console command options.
     */
    protected function getOptions()
    {
        return [

        ];
    }

}