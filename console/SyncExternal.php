<?php
/**
 * Created by PhpStorm.
 * User: Łukasz Biały
 * URL: http://keios.eu
 * Date: 8/13/15
 * Time: 2:17 AM
 */

namespace Keios\PluginMother\Console;

use Illuminate\Console\Command;
use Keios\PluginMother\Classes\PluginPoller;
use Keios\PluginMother\Repositories\PluginRepository;

class SyncExternal extends Command
{
    /**
     * The console command name.
     */
    protected $name = 'exchange:sync-external';

    /**
     * The console command description.
     */
    protected $description = 'Triggers Exchange Plugins Sync';

    public function handle(){
        $this->fire();
    }

    /**
     * Execute the console command.
     *
     * @throws \InvalidArgumentException
     * @throws \ApplicationException
     * @throws \Exception
     */
    public function fire()
    {
        $repo = new PluginRepository();
        $plugins = $repo->getExternal();
        $pluginPoller = new PluginPoller();
        $this->info('');
        $this->warn('Updating external plugins');
        foreach ($plugins as $plugin) {
            $this->comment('');
            try {
                $logs = $pluginPoller->pollPlugin($plugin);
                foreach($logs as $log) {
                    $this->info($log);
                }
            } catch(\Exception $e){
                \Log::error($e->getMessage(). ' '. $e->getTraceAsString());
                $this->error($plugin->name . ' failed with '. $e->getMessage());
            }
            $this->comment('--------------------------------------------------------------');

        }
        $this->info(\Lang::trans('keios.pluginmother::lang.messages.finished'));
    }


    /**
     * Get the console command arguments.
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     */
    protected function getOptions()
    {
        return [];
    }


}
