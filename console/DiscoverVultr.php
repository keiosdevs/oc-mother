<?php
/**
 * Created by PhpStorm.
 * User: Łukasz Biały
 * URL: http://keios.eu
 * Date: 8/13/15
 * Time: 2:17 AM
 */

namespace Keios\PluginMother\Console;

use Illuminate\Console\Command;
use Keios\PluginMother\Classes\Connectors\VultrConnector;
use Keios\PluginMother\Models\CloudNode;
use Keios\PluginMother\Models\Settings;
use Keios\PluginMother\Repositories\CloudNodeRepository;

/**
 * Class DiscoverVultr
 * @package Keios\PluginMother\Console
 */
class DiscoverVultr extends Command
{
    /**
     * The console command name.
     */
    protected $name = 'exchange:discover-vultr';

    /**
     * The console command description.
     */
    protected $description = 'Triggers Exchange Plugins Sync';

    /**
     * @var CloudNodeRepository
     */
    private $cloudNodeRepo;

    /**
     * DiscoverVultr constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->cloudNodeRepo = new CloudNodeRepository();
    }

    public function handle(){
        $this->fire();
    }

    /**
     * Execute the console command.
     *
     * @throws \InvalidArgumentException
     * @throws \ApplicationException
     * @throws \Exception
     */
    public function fire()
    {
        $settings = Settings::instance();
        $connector = new VultrConnector();
        $connector->authorize($settings->get('vultr_api_key'));
        $nodes = $connector->getNodes();
        $this->compareWithLocalState($nodes);

    }

    /**
     * @param $remoteNodes
     */
    public function compareWithLocalState($remoteNodes){

        $remoteIds = array_keys($remoteNodes);

        $this->softDeletePurged($remoteIds);
        $this->createNotExisting($remoteNodes);

    }

    /**
     * @param $remoteNodes
     */
    private function createNotExisting($remoteNodes){
        foreach($remoteNodes as $remoteId => $node){
            $model = $this->cloudNodeRepo->getByRemoteId($remoteId);
            if(!$model){
                $model = new CloudNode();
                $this->info('Creating node ' . $model->remote_id);
            } else {
                $this->info('Updating node ' . $model->remote_id);
            }
            $model->setupWithArray($node);
            $model->type = 'vultr';
            $model->save();
            $this->info('Finished');
            $this->info('');
        }
    }
    /**
     * @param $remoteIds
     */
    private function softDeletePurged($remoteIds){
        $notExisting = CloudNode::whereNotIn('remote_id', $remoteIds)->where('type', 'vultr')->get();
        foreach($notExisting as $node){
            $node->delete(); // we are doing soft deletes here, no worries, build a controller to browse these!
        }
    }

    /**
     * Get the console command arguments.
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     */
    protected function getOptions()
    {
        return [];
    }


}
