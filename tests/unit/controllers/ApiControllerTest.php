<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 2017-02-27
 * Time: 11:16
 */

namespace Keios\PluginMother\Tests\Unit\Controllers;


use Keios\PluginMother\Controllers\ApiController;
use Keios\PluginMother\Models\Project;
use Keios\PluginMother\Tests\Mocks\PluginMock;
use Keios\PluginMother\Tests\Mocks\ProjectMock;
use PluginTestCase;

/**
 * Class ApiControllerTest
 *
 * @package Keios\PluginMother\Tests\Unit\Controllers
 */
class ApiControllerTest extends PluginTestCase
{
    /**
     * @var ApiController
     */
    public $controller;

    /**
     *
     */
    public function setUp()
    {
        parent::setUp();
        $this->controller = new ApiController();
    }

    /**
     *
     */
    public function testAuthorize()
    {
        $deployType = 'production';
        $project = new ProjectMock();
        $project->code = 'abc';
        $project->title = 'abc';
        $_POST = [];
        $code1 = $code = 'plugin'.random_int(1, 10000000);
        $plugin1 = new PluginMock();
        $plugin1->setupMock($code1);
        $code2 = $code = 'plugin'.random_int(1, 10000000);
        $plugin2 = new PluginMock();
        $plugin2->setupMock($code2);
        $plugins = [
            $plugin1,
            $plugin2,
        ];
        $project->plugins = $plugins;
        $response = $this->controller->preparePluginsArray($plugins, $deployType, [], $project);
        $this->assertCount(2, count($response));
        $this->assertEquals($project->title, 'abc');
        $this->assertArrayHasKey($code1, $response);
        $this->assertArrayHasKey($code2, $response);
        $this->assertEquals($code1, $response[$code1]['slug']);
    }

    public function testReceiveUpdate()
    {
        $project = new Project();
        $project->code = 'abc';
        $project->title = 'abc';
        $project->user_id = 118;
        $project->save();
        $data = [
            'project_key' => 'abc',
            'message'     => "{ok: 'ok'}",
            'status'      => 'success',
        ];
        $controller = new ApiController();
        $controller->setPostData($data);
        $res = $controller->receiveUpdate();
        $array = json_decode($res->content(), true);
        $this->assertTrue(array_key_exists('message', $array));
        $this->assertEquals('OK', $array['message']);
    }

    public function testNoProjectUpdate()
    {
        $data = [
            'project_key' => 'abc',
            'message'     => "{ok: 'ok'}",
            'status'      => 'success',
        ];
        $controller = new ApiController();
        $controller->setPostData($data);
        $res = $controller->receiveUpdate();
        $array = json_decode($res->content(), true);
        $this->assertFalse(array_key_exists('message', $array));
        $this->assertTrue(array_key_exists('error', $array));
        $this->assertEquals('Not found', $array['error']);

    }

    /**
     *
     */
    public function testGetZip()
    {
        $data = [
            'project_key' => 'abc',
            'slug'        => 'abc',
        ];
        $controller = new ApiController();
        $controller->setPostData($data);
        $res = $controller->getZip();
        $array = json_decode($res->content(), true);
        $this->assertFalse(array_key_exists('error', $array));
        $this->assertTrue(array_key_exists('message', $array));
        $this->assertEquals('OK', $array['message']);
    }


    /**
     *
     */
    public function testFailedGetZip()
    {
        $data = [
            'project_key' => 'abc',
            'slug'        => 'abc',
        ];
        $controller = new ApiController();
        $controller->setPostData($data);
        $res = $controller->getZip();
        $array = json_decode($res->content(), true);
        $this->assertFalse(array_key_exists('message', $array));
        $this->assertTrue(array_key_exists('error', $array));
        $this->assertEquals('Not found', $array['error']);
    }

    /**
     *
     */
    public function testGetCurrentState()
    {

    }

    /**
     *
     */
    public function testCheckPlugin()
    {

    }
}