<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 2017-02-27
 * Time: 11:12
 */

namespace Keios\PluginMother\Tests\Unit\Components;

use Keios\PluginMother\Components\AccountHelper;
use PluginTestCase;

class AccountHelperTest extends PluginTestCase
{
    public function testSelf()
    {
        $helper = new AccountHelper(null, []);
        $this->assertEquals('AccountHelper', class_basename($helper));
    }
}