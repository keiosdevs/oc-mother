<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 2017-02-27
 * Time: 11:15
 */

namespace Keios\PluginMother\Tests\Unit\Components;


use Keios\PluginMother\Components\ProjectsComponent;
use PluginTestCase;
use RainLab\User\Models\User;

class ProjectsComponentTest extends PluginTestCase
{
    public function testMotifyProject()
    {
        $helper = new ProjectsComponent(null, []);
        $userMock = new User();
        $userMock->id = 118;
        $userMock->email = 'test@test.com';
        $createData = [
            'title' => 'blabla Project',
            'filth' => 'is here'
        ];
        $updateData = [
            'title' => 'blablabla project',
            'bullshit' => 'some more'
        ];
        $created = $helper->createProjectFromPost($userMock, $createData);
        $this->assertEquals('blabla-project', $created->slug);
        $updated = $helper->updateProjectFromPost($created, $updateData);
        $this->assertEquals('ProjectsComponent', class_basename($helper));
        $this->assertEquals('blablabla-project', $updated->slug);
    }
}