<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 2017-02-27
 * Time: 11:15
 */

namespace Keios\PluginMother\Tests\Unit\Components;


use Keios\PluginMother\Components\PluginsComponent;
use PluginTestCase;

class PluginsComponentTest extends PluginTestCase
{

    public function testSelf(){
        $helper = new PluginsComponent(null, []);
        $this->assertEquals('PluginsComponent', class_basename($helper));
    }

    public function testAddToProject()
    {
        $helper = new PluginsComponent(null, []);
        $this->assertTrue($helper->addToProject(118, 118));
    }

    public function testFailAddingToProject()
    {
        $helper = new PluginsComponent(null, []);
        $this->assertEquals('PluginsComponent', class_basename($helper));
        $this->setExpectedException('ApplicationException');
        $helper->addToProject('asdasdasd', '34123123');

    }
}