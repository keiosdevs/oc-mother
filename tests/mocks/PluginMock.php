<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 2017-02-27
 * Time: 11:32
 */

namespace Keios\PluginMother\Tests\Mocks;


class PluginMock extends \Keios\PluginMother\Models\Plugin
{
    public function setupMock($code){
        $this->id = 1;
        $this->name = $code;
        $this->slug = $code;
        $this->namespace = 'keios';
        $this->directory = $code;
        $this->excerpt = 'Soasdasodasdas';
        $this->description = 'dasdjd asdjasdjasd adjasdasd';
        $this->git_ssh_path = 'git@bitbucket.org/keiosdevs/mock-plugin.git';
        $this->git_branch = 'master';
        $this->version = null;

    }
}