<?php namespace Keios\PluginMother\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProjectOrdersTable extends Migration
{
    public function up()
    {
        Schema::create(
            'keios_pluginmother_project_orders',
            function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->integer('project_id')->index();
                $table->integer('order_id')->index();
                $table->integer('promotion_id')->index()->nullable();
                $table->text('metadata');
                $table->timestamps();
            }
        );
    }

    public function down()
    {
        Schema::dropIfExists('keios_pluginmother_project_orders');
    }
}
