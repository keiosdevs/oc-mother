<?php namespace Keios\PluginMother\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePluginsRecommendedsTable extends Migration
{
    public function up()
    {
        Schema::create('keios_pluginmother_plugins_recommendeds', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('plugin_id')->index();
            $table->integer('recommended_id')->index();
        });
    }

    public function down()
    {
        Schema::dropIfExists('keios_pluginmother_plugins_recommendeds');
    }
}
