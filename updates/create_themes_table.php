<?php namespace Keios\PluginMother\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateThemesTable extends Migration
{
    public function up()
    {
        Schema::create('keios_pluginmother_themes', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->string('namespace');
            $table->string('directory');
            $table->text('excerpt')->nullable();
            $table->text('description')->nullable();
            $table->string('git_ssh_path');
            $table->string('git_branch');
            $table->text('git_branches')->default('');
            $table->text('boot_script')->default('');
            $table->string('version')->nullable();
            $table->boolean('is_featured')->default(false);
            $table->boolean('is_visible')->default(false);
            $table->boolean('is_library')->default(false);
            $table->boolean('is_depreciated')->default(false);
            $table->boolean('on_sale')->default(false);
            $table->timestamps();
        });
        Schema::create(
            'keios_pluginmother_proj_themes',
            function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->integer('theme_id')->index();
                $table->integer('project_id')->index();
            }
        );
    }

    public function down()
    {
        Schema::dropIfExists('keios_pluginmother_themes');
        Schema::dropIfExists('keios_pluginmother_proj_themes');
    }
}
