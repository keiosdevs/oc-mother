<?php namespace Keios\PluginMother\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCloudNodesTable extends Migration
{
    public function up()
    {
        Schema::create('keios_pluginmother_cloud_nodes', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('remote_id')->index();
            $table->string('type')->index();
            $table->text('details');
            $table->string('git_branch')->nullable();
            $table->text('git_branches');
            $table->boolean('online')->nullable();
            $table->string('ip')->nullable();
            $table->string('label')->nullable();
            $table->string('location')->nullable();
            $table->string('os')->nullable();
            $table->string('uname')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('keios_pluginmother_nodes_projects', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('cloudnode_id')->index();
            $table->integer('project_id')->index();
        });
    }

    public function down()
    {
        Schema::dropIfExists('keios_pluginmother_cloud_nodes');
        Schema::dropIfExists('keios_pluginmother_nodes_projects');
    }
}
