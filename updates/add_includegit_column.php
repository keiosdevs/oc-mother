<?php namespace Keios\PluginMother\Updates;

use Illuminate\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

class AddIncludegitColumn extends Migration
{

    public function up()
    {
        Schema::table(
            'keios_pluginmother_projects',
            function (Blueprint $table) {
                $table->boolean('include_git_repo')->after('lock_console')->default(true);
            }
        );
    }

    public function down()
    {
        Schema::table(
            'keios_pluginmother_projects',
            function (Blueprint $table) {
                $table->dropColumn('include_git_repo');
            }
        );
    }

}
