<?php namespace Keios\PluginMother\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * Class CreatePluginGroupsTable
 * @package Keios\PluginMother\Updates
 */
class CreatePluginGroupsTable extends Migration
{
    /**
     * Tables up
     */
    public function up()
    {
        Schema::create('keios_pluginmother_plugin_groups', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->text('description')->default('');
            $table->timestamps();
        });
        Schema::create('keios_pluginmother_pg_pivot', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('plugin_id')->index();
            $table->integer('category_id')->index();
        });
    }

    /**
     * Tables down
     */
    public function down()
    {
        Schema::dropIfExists('keios_pluginmother_plugin_groups');
        Schema::dropIfExists('keios_pluginmother_pg_pivot');
    }
}
