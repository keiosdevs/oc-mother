<?php namespace Keios\PluginMother\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateFinancialTable extends Migration
{
    public function up()
    {
        Schema::create('keios_pluginmother_financial', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('license_id')->index();
            $table->integer('plugin_id')->index();
            $table->integer('project_id')->index();
            $table->boolean('is_paid')->default(false);
            $table->string('amount')->nullable();
            $table->dateTime('valid_to')->nullable();
            $table->dateTime('deadline')->nullable();
            $table->integer('order_id')->index()->nullable();
            $table->timestamps();
        });
    }



    public function down()
    {
        Schema::dropIfExists('keios_pluginmother_financial');
    }
}
