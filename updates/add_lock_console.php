<?php namespace Keios\PluginMother\Updates;

use Illuminate\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

class AddLockConsole extends Migration
{

    public function up()
    {
        Schema::table(
            'keios_pluginmother_projects',
            function (Blueprint $table) {
                $table->boolean('lock_console')->after('is_supported')->default(false);
            }
        );
    }

    public function down()
    {
        Schema::table(
            'keios_pluginmother_projects',
            function (Blueprint $table) {
                $table->dropColumn('lock_console');
            }
        );
    }

}