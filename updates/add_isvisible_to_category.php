<?php namespace Keios\PluginMother\Updates;

use Illuminate\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

class AddIsvisibleToCategory extends Migration
{

    public function up()
    {
        Schema::table(
            'keios_pluginmother_plugin_cat',
            function (Blueprint $table) {
                $table->boolean('is_visible')->after('metadata')->default(true);
                $table->boolean('on_sale')->after('is_visible')->default(false);
            }
        );
    }

    public function down()
    {
        Schema::table(
            'keios_pluginmother_plugin_cat',
            function (Blueprint $table) {
                $table->dropColumn('is_visible');
                $table->dropColumn('on_sale');
            }
        );
    }

}