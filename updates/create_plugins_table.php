<?php namespace Keios\PluginMother\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * Class CreatePluginsTable
 *
 * @package Keios\PluginMother\Updates
 */
class CreatePluginsTable extends Migration
{
    /**
     * Create table
     */
    public function up()
    {
        Schema::create(
            'keios_pluginmother_plugins',
            function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('name');
                $table->string('slug');
                $table->string('namespace');
                $table->string('directory');
                $table->text('excerpt')->nullable();
                $table->text('description')->nullable();
                $table->string('git_ssh_path');
                $table->string('git_branch');
                $table->text('git_branches');
                $table->string('version')->nullable();
                $table->text('metadata');
                $table->boolean('is_featured')->default(false);
                $table->boolean('is_visible')->default(false);
                $table->boolean('is_library')->default(false);
                $table->boolean('is_depreciated')->default(false);
                $table->boolean('on_sale')->default(false);
                $table->timestamps();
            }
        );
        Schema::create(
            'keios_pluginmother_proj_plug',
            function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->integer('project_id');
                $table->integer('plugin_id');
             }
        );
        Schema::create(
            'keios_pluginmother_lic_plug',
            function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->integer('license_id');
                $table->integer('plugin_id');
            }
        );

    }

    /**
     * Drop table
     */
    public function down()
    {
        Schema::dropIfExists('keios_pluginmother_plugins');
        Schema::dropIfExists('keios_pluginmother_proj_plug');
        Schema::dropIfExists('keios_pluginmother_lic_plug');
    }
}
