<?php namespace Keios\PluginMother\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateLicensesTable extends Migration
{
    public function up()
    {
        Schema::dropIfExists('keios_pluginmother_licenses');
        Schema::create('keios_pluginmother_licenses', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->integer('plugin_id')->index();
            $table->double('price');
            $table->integer('license_type_id');
            $table->dateTime('valid_to')->nullable();
            $table->timestamps();
        });
        Schema::create('keios_pluginmother_lic_users', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('user_id')->index();
            $table->integer('license_id')->index();
        });
    }



    public function down()
    {
        Schema::dropIfExists('keios_pluginmother_licenses');
        Schema::dropIfExists('keios_pluginmother_lic_users');
    }
}
