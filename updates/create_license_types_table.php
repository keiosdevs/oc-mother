<?php namespace Keios\PluginMother\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateLicenseTypesTable extends Migration
{
    public function up()
    {
        Schema::create('keios_pluginmother_license_types', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->text('content');
            $table->boolean('is_forever')->default(true);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('keios_pluginmother_license_types');
    }
}
