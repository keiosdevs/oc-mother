<?php namespace Keios\PluginSpawn\Updates;

use Illuminate\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

class AddInternalLicenses extends Migration
{

    public function up()
    {
        Schema::table(
            'keios_pluginmother_licenses',
            function (Blueprint $table) {
                $table->boolean('is_public')->after('valid_to')->default(true);
                $table->boolean('is_forever')->after('is_public')->default(false);
            }
        );
    }

    public function down()
    {
        Schema::table(
            'keios_pluginmother_licenses',
            function (Blueprint $table) {
                $table->dropColumn('is_public');
                $table->dropColumn('is_forever');
            }
        );
    }

}
