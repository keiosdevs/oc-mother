<?php namespace Keios\PluginMother\Updates;


use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * Class CreateDeployTypesTable
 *
 * @package Keios\PluginMother\Updates
 */
class CreateDeployTypesTable extends Migration
{
    /**
     *
     */
    public function up()
    {
       Schema::create(
            'keios_pluginmother_deploy_types',
            function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('code');
                $table->string('label');
                $table->boolean('is_default')->default(false);
                $table->boolean('is_available')->default(true);
                $table->timestamps();
            }
        );

        \DB::table('keios_pluginmother_deploy_types')->insert(
            [
                ['code' => 'production', 'label' => 'Production', 'is_default' => true, 'is_available' => true],
                ['code' => 'staging', 'label' => 'Staging', 'is_default' => false, 'is_available' => true],
                ['code' => 'develop', 'label' => 'Develop', 'is_default' => false, 'is_available' => true],

            ]
        );

    }

    /**
     *
     */
    public function down()
    {
        Schema::dropIfExists('keios_pluginmother_deploy_types');
    }
}
