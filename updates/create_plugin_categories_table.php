<?php namespace Keios\PluginMother\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePluginCategoriesTable extends Migration
{
    public function up()
    {
        Schema::create('keios_pluginmother_plugin_cat', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->text('description')->default('');
            $table->text('market_plugins');
            $table->text('metadata');
            $table->timestamps();
        });

        Schema::create('keios_pluginmother_pct_pivot', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('plugin_id')->index();
            $table->integer('category_id')->index();
        });
    }

    public function down()
    {
        Schema::dropIfExists('keios_pluginmother_plugin_cat');
        Schema::dropIfExists('keios_pluginmother_pct_pivot');
    }
}
