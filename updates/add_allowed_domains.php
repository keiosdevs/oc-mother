<?php namespace Keios\PluginMother\Updates;

use Illuminate\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

class AddAllowedDomains extends Migration
{

    public function up()
    {
        Schema::table(
            'keios_pluginmother_projects',
            function (Blueprint $table) {
                $table->text('allowed_domains')->after('lock_console')->nullable();
            }
        );
    }

    public function down()
    {
        Schema::table(
            'keios_pluginmother_projects',
            function (Blueprint $table) {
                $table->dropColumn('allowed_domains');
            }
        );
    }

}