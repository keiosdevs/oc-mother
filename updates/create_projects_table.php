<?php namespace Keios\PluginMother\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * Class CreateProjectsTable
 *
 * @package Keios\PluginMother\Updates
 */
class CreateProjectsTable extends Migration
{
    /**
     * Create table
     */
    public function up()
    {
        Schema::create(
            'keios_pluginmother_projects',
            function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->integer('user_id')->index();
                $table->integer('parent_id')->index()->nullable();
                $table->string('title');
                $table->string('slug');
                $table->text('disabled_plugins')->default('');
                $table->string('code');
                $table->text('plugin_overrides');
                $table->integer('theme_id')->index()->nullable();
                $table->text('theme_overrides');
                $table->text('pre_market_plugins')->default('');
                $table->text('post_market_plugins')->default('');
                $table->string('domain')->nullable();
                $table->boolean('enable_notifications')->default(true);
                $table->boolean('enable_autoupdate')->default(false);
                $table->boolean('is_enabled')->default(true);
                $table->boolean('is_paid')->default(true);
                $table->boolean('is_supported')->default(true);
                $table->string('last_update_status')->nullable();
                $table->dateTime('last_update_date')->nullable();
                $table->timestamps();
            }
        );
    }

    /**
     * Drop tables
     */
    public function down()
    {
        Schema::dropIfExists('keios_pluginmother_projects');
    }
}
