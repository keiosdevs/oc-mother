<?php namespace Keios\PluginMother\Updates;

use Illuminate\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

class AddIsexternal extends Migration
{

    public function up()
    {
        Schema::table(
            'keios_pluginmother_plugins',
            function (Blueprint $table) {
                $table->boolean('is_external')->after('is_visible')->default(false);
            }
        );
    }

    public function down()
    {
        Schema::table(
            'keios_pluginmother_plugins',
            function (Blueprint $table) {
                $table->dropColumn('is_external');
            }
        );
    }

}
