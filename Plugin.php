<?php namespace Keios\PluginMother;

use Backend;
use Illuminate\Foundation\AliasLoader;
use Keios\PluginMother\Classes\PluginMotherServiceProvider;
use Keios\PluginMother\Components\AccountHelper;
use Keios\PluginMother\Components\FinancialComponent;
use Keios\PluginMother\Components\PluginsComponent;
use Keios\PluginMother\Components\ProjectsComponent;
use Keios\PluginMother\Console\DiscoverVultr;
use Keios\PluginMother\Console\SyncPlugin;
use Keios\PluginMother\Console\SyncExternal;
use Keios\PluginMother\Console\SyncPlugins;
use Keios\PluginMother\Console\SyncTheme;
use Keios\PluginMother\Console\SyncThemes;
use Keios\PluginMother\Console\UpdateSpawns;
use Keios\PluginMother\Facades\MoneyFormatter;
use Keios\PluginMother\Models\Project;
use Keios\PluginMother\Models\Settings;
use RainLab\User\Models\User;
use System\Classes\PluginBase;

/**
 * PluginMother Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'PluginMother',
            'description' => 'Core for PluginManagement in OctoberExchange',
            'author'      => 'Keios Solutions & PxPx',
            'icon'        => 'icon-cogs',
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        $alias = AliasLoader::getInstance();
        $this->app->register(PluginMotherServiceProvider::class);
        $this->app->bind(
            'exchange:sync-themes',
            SyncThemes::class
        );
        $this->app->bind(
            'exchange:sync-theme',
            SyncTheme::class
        );

        $this->app->bind(
            'exchange:sync-plugin',
            SyncPlugin::class
        );
        $this->app->bind(
            'exchange:sync-plugins',
            SyncPlugins::class
        );
        $this->app->bind(
            'exchange:update-spawns',
            UpdateSpawns::class
        );
        $this->app->bind(
            'exchange:discover-vultr',
            DiscoverVultr::class
        );
        $this->app->bind(
            'exchange:sync-external',
            SyncExternal::class
        );
        $alias->alias('MoneyFormatter', MoneyFormatter::class);
        $this->commands(
            [
                'exchange:sync-external',
                'exchange:sync-plugin',
                'exchange:sync-plugins',
                'exchange:sync-themes',
                'exchange:sync-theme',
                'exchange:update-spawns',
                'exchange:discover-vultr',
            ]
        );

    }

    /**
     * @return array
     */
    public function registerMarkupTags()
    {
        return [
            'filters' => [
                'money' => function ($amount) {
                    return MoneyFormatter::format($amount, false);
                },
            ],
        ];
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        User::extend(
            function ($model) {
                $model->hasMany['projects'] = Project::class;
            }
        );
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            AccountHelper::class      => 'ke_helper',
            PluginsComponent::class   => 'ke_plugins',
            ProjectsComponent::class  => 'ke_projects',
            FinancialComponent::class => 'ke_financial',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'keios.pluginmother.plugins'          => [
                'tab'   => 'PluginMother',
                'label' => 'Plugins access',
            ],
            'keios.pluginmother.plugingroups'     => [
                'tab'   => 'PluginMother',
                'label' => 'PluginGroups access',
            ],
            'keios.pluginmother.plugincategories' => [
                'tab'   => 'PluginMother',
                'label' => 'PluginCategories access',
            ],
            'keios.pluginmother.projects'         => [
                'tab'   => 'PluginMother',
                'label' => 'Projects access',
            ],
            'keios.pluginmother.deploytypes'      => [
                'tab'   => 'PluginMother',
                'label' => 'Deploy Types access',
            ],
            'keios.pluginmother.license_types'    => [
                'tab'   => 'PluginMother',
                'label' => 'License Types access',
            ],
            'keios.pluginmother.licenses'         => [
                'tab'   => 'PluginMother',
                'label' => 'Licenses access',
            ],
            'keios.pluginmother.themes'           => [
                'tab'   => 'PluginMother',
                'label' => 'Themes access',
            ],
            'keios.pluginmother.license_orders'   => [
                'tab'   => 'PluginMother',
                'label' => 'License orders access',
            ],
        ];
    }

    /**
     * @param string $schedule
     */
    public function registerSchedule($schedule)
    {
        $settings = Settings::instance();
        if($settings->get('autoupdate_external')){
            $cronString = $settings->get('autoupdate_external_cron');
            if(!$cronString){
                $cronString = '0 6 * * *';
            }
            $schedule->command('exchange:sync-external')->cron($cronString);
        }
        $runAutoupdate = $settings->get('enable_autoupdate');
        if($settings->get('vultr_api_key')){
            $schedule->command('exchange:discover-vultr')->everyFiveMinutes();
        }
        if ($runAutoupdate) {
            $autoupdatePeriod = $settings->get('autoupdate_period');
            $autoupdateHour = $settings->get('autoupdate_hour');
            if ($autoupdatePeriod === 'weekly') {
                $autoupdateDays = $settings->get('autoupdate_day');
                $schedule->command('exchange:update-spawns')->weekly()->$autoupdateDays()->at($autoupdateHour);
            } else {
                $schedule->command('exchange:update-spawns')->daily()->at($autoupdateHour);
            }
        }
    }

    /**
     * @return array
     */
    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => \Lang::trans('keios.pluginmother::lang.nav.plugin_mother'),
                'description' => \Lang::trans('keios.pluginmother::lang.nav.settings_desc'),
                'category'    => \Lang::trans('keios.pluginmother::lang.nav.settings_group'),
                'icon'        => 'icon-cog',
                'class'       => 'Keios\PluginMother\Models\Settings',
                'order'       => 500,
                'permissions' => ['keios.pluginmother.settings'],
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'pluginmother' => [
                'label'       => \Lang::trans('keios.pluginmother::lang.nav.plugin_mother'),
                'url'         => Backend::url('keios/pluginmother/plugins'),
                'icon'        => 'icon-globe',
                'iconSvg'     => 'plugins/keios/pluginmother/assets/img/menu-icon.svg',
                'permissions' => ['keios.pluginmother.*'],
                'order'       => 30,

                'sideMenu' => [
                    'plugins'          => [
                        'label'       => \Lang::trans('keios.pluginmother::lang.nav.plugins'),
                        //'iconSvg'     => 'plugins/keios/pluginmother/assets/img/light-bulb.png',
                        'icon'        => 'icon-plug',
                        'url'         => Backend::url('keios/pluginmother/plugins'),
                        'permissions' => ['keios.pluginmother.plugins'],
                    ],
                    'themes'           => [
                        'label'       => \Lang::trans('keios.pluginmother::lang.nav.themes'),
                        'icon'        => 'icon-image',
                        //'iconSvg'     => 'plugins/keios/pluginmother/assets/img/themes-icon.svg',
                        'url'         => Backend::url('keios/pluginmother/themes'),
                        'permissions' => ['keios.pluginmother.themes'],
                    ],
                    'projects'         => [
                        'label'       => \Lang::trans('keios.pluginmother::lang.nav.projects'),
                        'icon'        => 'icon-briefcase',
                        //'iconSvg'     => 'plugins/keios/pluginmother/assets/img/projects-icon.svg',
                        'url'         => Backend::url('keios/pluginmother/projects'),
                        'permissions' => ['keios.pluginmother.projects'],
                    ],
                    'license_orders'   => [
                        'label'       => \Lang::trans('keios.pluginmother::lang.nav.license_orders'),
                        'icon'        => 'icon-money',
                        //'iconSvg'     => 'plugins/keios/pluginmother/assets/img/money-icon.svg',
                        'url'         => Backend::url('keios/pluginmother/licenseorders'),
                        'permissions' => ['keios.pluginmother.license_orders'],
                    ],
                    'cloudnodes'       => [
                        'label'       => \Lang::trans('keios.pluginmother::lang.nav.cloud_nodes'),
                        'icon'        => 'icon-cloud',
                        //'iconSvg'     => 'plugins/keios/pluginmother/assets/img/cloud-icon.svg',
                        'url'         => Backend::url('keios/pluginmother/cloudnodes'),
                        'permissions' => ['keios.pluginmother.cloudnodes'],
                    ],
                    'plugingroups'     => [
                        'label'       => \Lang::trans('keios.pluginmother::lang.nav.plugin_groups'),
                        'icon'        => 'icon-cubes',
                        //'iconSvg'     => 'plugins/keios/pluginmother/assets/img/groups-icon.svg',
                        'url'         => Backend::url('keios/pluginmother/plugingroups'),
                        'permissions' => ['keios.pluginmother.plugingroups'],
                    ],
                    'plugincategories' => [
                        'label'       => \Lang::trans('keios.pluginmother::lang.nav.plugin_categories'),
                        'icon'        => 'icon-ticket',
                        //'iconSvg'     => 'plugins/keios/pluginmother/assets/img/categories-icon.svg',
                        'url'         => Backend::url('keios/pluginmother/plugincategories'),
                        'permissions' => ['keios.pluginmother.plugincategories'],
                    ],
                    'deploytypes'      => [
                        'label'       => \Lang::trans('keios.pluginmother::lang.nav.deploy_types'),
                        'icon'        => 'icon-fighter-jet',
                        //'iconSvg'     => 'plugins/keios/pluginmother/assets/img/deploys-icon.svg',
                        'url'         => Backend::url('keios/pluginmother/deploytypes'),
                        'permissions' => ['keios.pluginmother.deploytypes'],
                    ],
                    'licensetypes'     => [
                        'label'       => \Lang::trans('keios.pluginmother::lang.nav.license_types'),
                        'icon'        => 'icon-file-pdf-o',
                        //'iconSvg'     => 'plugins/keios/pluginmother/assets/img/license-types-icon.svg',
                        'url'         => Backend::url('keios/pluginmother/licensetypes'),
                        'permissions' => ['keios.pluginmother.license_types'],
                    ],
                    'licenses'         => [
                        'label'       => \Lang::trans('keios.pluginmother::lang.nav.bought_licenses'),
                        'icon'        => 'icon-money',
                        //'iconSvg'     => 'plugins/keios/pluginmother/assets/img/licenses-icon.svg',
                        'url'         => Backend::url('keios/pluginmother/licenses'),
                        'permissions' => ['keios.pluginmother.licenses'],
                    ],
                ],
            ],
        ];
    }
}
