<?php namespace Keios\PluginMother\Models;

use Keios\MoneyRight\Money;
use Keios\PluginMother\Classes\CostCalculator;
use Keios\PluginMother\Repositories\ProjectRepository;
use Model;
use RainLab\User\Models\User;

/**
 * Project Model
 */
class Project extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'keios_pluginmother_projects';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array
     */
    protected $jsonable = [
        'plugin_overrides',
        'disabled_plugins',
        'pre_market_plugins',
        'post_market_plugins',
        'theme_overrides',
        'allowed_domains'
    ];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array
     */
    public $hasMany = [
        'subprojects' => [Project::class, 'key' => 'parent_id'],
        'license_orders' => [LicenseOrder::class]
    ];
    /**
     * @var array
     */
    public $belongsTo = [
        'user'   => User::class,
        'parent' => [Project::class, 'key' => 'parent_id'],
        'theme'  => Theme::class,
    ];
    /**
     * @var array
     */
    public $belongsToMany = [
        'plugins'    => [Plugin::class, 'table' => 'keios_pluginmother_proj_plug'],
        'themes'     => [Theme::class, 'table' => 'keios_pluginmother_proj_themes'],
        'cloudnodes' => [
            CloudNode::class,
            'table'    => 'keios_pluginmother_nodes_projects',
            'key'      => 'cloudnode_id',
            'otherKey' => 'project_id',
        ],
    ];

    /**
     * @return bool
     */
    public function veryPaid(){
        foreach($this->license_orders as $licenseOrder){
            if(!$licenseOrder->is_paid){
                return false;
            }
        }
        return true;
    }

    /**
     * @return array
     */
    public function getVultrNode()
    {
        foreach ($this->cloudnodes as $cloudnode) {
            if ($cloudnode->type === 'vultr') {
                return $cloudnode;
            }
        }

        return [];
    }

    /**
     * @return mixed
     */
    public function getPluginSlugOptions()
    {
        return Plugin::lists('name', 'slug');

    }

    /**
     * @return mixed
     */
    public function getThemeSlugOptions()
    {
        return Theme::lists('name', 'slug');

    }

    /**
     * Configure slug and code automatically
     */
    public function beforeCreate()
    {
        if (!$this->slug) {
            $this->slug = $this->generateSlug($this->title);
        }
        if (!$this->code) {
            $this->code = $this->generateCode(42);
        }
    }

    /**
     * @throws \ApplicationException
     */
    public function beforeSave()
    {
        $overrides = $this->plugin_overrides;
        if (!$overrides) {
            $overrides = [];
        }
        $slugs = [];
        foreach ($overrides as $override) {
            if (in_array($override['plugin_slug'], $slugs, true)) {
                throw new \ApplicationException(
                    \Lang::trans('keios.pluginmother::lang.errors.retarded_doubled_override')
                );
            }
            $slugs[] = $override['plugin_slug'];
        }
    }

    /**
     * @return Money
     */
    public function getOwedCost()
    {
        $costCalculator = new CostCalculator();

        return $costCalculator->getOwedAmount($this);
    }

    /**
     * @return bool
     */
    public function hasSubprojects()
    {
        return count($this->subprojects) > 0;
    }

    /**
     * @param string $string
     *
     * @return string
     */
    private function generateSlug($string)
    {
        $repo = new ProjectRepository();
        $slug = strtolower(str_replace([' ', '_'], ['', '-'], $string));
        $no = 1;
        while ($repo->getBySlug($slug)) {
            $slug = strtolower(str_replace([' ', '_'], ['', '-'], $string)).$no;
            ++$no;
        }

        return $slug;
    }

    /**
     * Helper function
     *
     * @param int $length
     *
     * @return string
     */
    private function generateCode($length = 20)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[random_int(0, $charactersLength - 1)];
        }

        return $randomString;
    }

}
