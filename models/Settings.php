<?php namespace Keios\PluginMother\Models;

use Keios\PluginMother\Classes\Connectors\VultrConnector;
use Keios\PluginMother\ValueObjects\VultrNodeConfig;
use Model;

/**
 * Class Settings
 *
 * @package Keios\PluginMother\Models
 */
class Settings extends Model
{
    /**
     * @var array
     */
    public $implement = ['System.Behaviors.SettingsModel'];

    /**
     * @var string
     */
    public $settingsCode = 'pluginmother_settings';
    /**
     * @var string
     */
    public $settingsFields = 'fields.yaml';

    /**
     * @var \Vultr\VultrClient
     */
    public $vultrClient;

    public function createVultrConfig()
    {
        $config = new VultrNodeConfig();
        $config->DCID = $this->vultr_region_id;
        $config->VPSPLANID = $this->vultr_plan_id;
        $config->OSID = $this->vultr_os_id;
        if($config->OSID == 193){
            $config->SCRIPTID = $this->vultr_script_id;
        }
        if($config->OSID == 164) {
            $config->SNAPSHOTID = $this->vultr_snapshot_id;
        }
        $config->SSHKEYID = $this->vultr_sshkey_id;

        return $config;
    }

    /**
     * @return array
     */
    public function getVultrRegionIdOptions()
    {
        $result = [];
        $vultrConnector = new VultrConnector();
        if (!$this->vultr_api_key) {
            return $result;
        }
        $this->vultrClient = $vultrConnector->authorize($this->vultr_api_key);

        if ($this->vultrClient) {
            $response = $this->vultrClient->region()->getList();
            if(!$response){$response = [];};
            foreach ($response as $dcid => $details) {
                $result[$dcid] = $details['name'].', '.$details['country'];
            }
        }

        return $result;
    }


    /**
     * @return array
     */
    public function getVultrScriptIdOptions()
    {
        $result = [];
        $vultrConnector = new VultrConnector();
        if (!$this->vultr_api_key) {
            return $result;
        }
        $this->vultrClient = $vultrConnector->authorize($this->vultr_api_key);
        if ($this->vultrClient) {
            $response = $this->vultrClient->startupScript()->getList();
            if(!$response){$response = [];};
            foreach ($response as $scriptId => $details) {
                $result[$scriptId] = $details['name'];
            }
        }

        return $result;
    }


    /**
     * @return array
     */
    public function getVultrPlanIdOptions()
    {
        $result = [];
        $vultrConnector = new VultrConnector();
        if (!$this->vultr_api_key) {
            return $result;
        }
        $this->vultrClient = $vultrConnector->authorize($this->vultr_api_key);
        if($this->vultrClient){
            $response = $this->vultrClient->metaData()->getPlansList();
            if(!$response){$response = [];};
            foreach($response as $planId => $details){
                $result[$planId] = $details['name'];
            }
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getVultrOsIdOptions()
    {
        $result = [];
        $vultrConnector = new VultrConnector();
        if (!$this->vultr_api_key) {
            return $result;
        }
        $this->vultrClient = $vultrConnector->authorize($this->vultr_api_key);
        if($this->vultrClient){
            $response = $this->vultrClient->metaData()->getOsList();
            if(!$response){$response = [];};
            foreach($response as $planId => $details){
                $result[$planId] = $details['name'];
            }
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getVultrSnapshotIdOptions()
    {
        $result = ['' => 'none'];
        $vultrConnector = new VultrConnector();
        if (!$this->vultr_api_key) {
            return $result;
        }
        $this->vultrClient = $vultrConnector->authorize($this->vultr_api_key);
        if($this->vultrClient){
            $response = $this->vultrClient->snapshot()->getList();
            if(!$response){$response = [];};
            foreach($response as $snapshotId => $details){
                $result[$snapshotId] = $details['description'];
            }
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getVultrSshkeyIdOptions()
    {
        $result = [];
        $vultrConnector = new VultrConnector();
        if (!$this->vultr_api_key) {
            return $result;
        }
        $this->vultrClient = $vultrConnector->authorize($this->vultr_api_key);
        if($this->vultrClient){
            $response = $this->vultrClient->sshKey()->getList();
            if(!$response){$response = [];};
            foreach($response as $planId => $details){
                $result[$planId] = $details['name'];
            }
        }

        return $result;
    }
}