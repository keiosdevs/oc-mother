<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2/24/17
 * Time: 7:50 PM
 */

namespace Keios\PluginMother\Models;

use Backend\Models\ExportModel;

class PluginExport extends ExportModel
{
    public function exportData($columns, $sessionKey = null)
    {
        $plugins = Plugin::all();
        $plugins->each(
            function ($plugin) use ($columns) {
                $plugin->addVisible($columns);
            }
        );

        return $plugins->toArray();
    }
}