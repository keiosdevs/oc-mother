<?php namespace Keios\PluginMother\Models;

use Model;

/**
 * DeployType Model
 */
class DeployType extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'keios_pluginmother_deploy_types';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function beforeSave()
    {
        if ($this->is_default == true) {
            DeployType::where('is_default', true)->update(
                [
                    'is_default' => false,
                ]
            );
        }
    }
}
