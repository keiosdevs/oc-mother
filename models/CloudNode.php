<?php namespace Keios\PluginMother\Models;

use Keios\PluginMother\Classes\CloudManager;
use Keios\PluginMother\Classes\Connectors\VultrConnector;
use Model;

/**
 * CloudNode Model
 */
class CloudNode extends Model
{

    use \October\Rain\Database\Traits\SoftDelete;
    /**
     * @var string The database table used by the model.
     */
    public $table = 'keios_pluginmother_cloud_nodes';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array
     */
    protected $jsonable = ['details'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array
     */
    public $belongsToMany = [
        'projects' => [Project::class, 'table' => 'keios_pluginmother_nodes_projects', 'key' => 'project_id', 'otherKey' => 'cloudnode_id'],
    ];

    /**
     * @param string|int $remoteId
     */
    public function setRemoteId($remoteId)
    {
        $this->remote_id = $remoteId;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @param array $details
     */
    public function setDetails(array $details)
    {
        $this->details = json_encode($details);
    }

    /**
     * @return string|int
     */
    public function getRemoteId()
    {
        return $this->remote_id;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param array $node
     */
    public function setupWithArray(array $node)
    {
        $cloudManager = new CloudManager();
        $meta = [];
        foreach($cloudManager->getNodeArrayMap() as $entry => $data){
            if(!array_key_exists($entry, $node)){
                continue;
            }
            $dbField = $data['db_field'];
            if($dbField === 'meta'){
                $meta[$entry] = $node[$entry];
            } else {
                $this->$dbField = $node[$entry];
            }
            if($entry === 'power_status' && $node[$entry] === 'running'){
                $this->online = 1;
            }
            if($entry === 'power_status' && $node[$entry] !== 'running'){
                $this->online = 1;
            }
        }
        $this->setDetails($meta);
    }

    /**
     * @return array
     */
    public function getDetails()
    {
        return json_decode($this->details, true);
    }

    /**
     *
     */
    public function beforeDelete()
    {
        $settings = Settings::instance();
        $remoteId = $this->remote_id;
        $manager = new VultrConnector();
        $client = $manager->authorize($settings->get('vultr_api_key'));
        $client->server()->destroy($remoteId);
    }


}
