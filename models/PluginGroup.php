<?php namespace Keios\PluginMother\Models;

use Model;

/**
 * PluginGroup Model
 */
class PluginGroup extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'keios_pluginmother_plugin_groups';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
        'plugins' => [Plugin::class, 'table' => 'keios_pluginmother_pg_pivot', 'key' => 'category_id', 'otherKey' => 'plugin_id']
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
