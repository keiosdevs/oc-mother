<?php namespace Keios\PluginMother\Models;

use Model;

/**
 * PluginsRecommended Model
 */
class PluginsRecommended extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'keios_pluginmother_plugins_recommendeds';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
