<?php namespace Keios\PluginMother\Models;

use Model;

/**
 * License Model
 */
class License extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'keios_pluginmother_licenses';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [

        'license_type' => ['Keios\PluginMother\Models\LicenseType'],
    ];
    public $belongsToMany = [
        'plugins' => [
            Plugin::class,
            'table'    => 'keios_pluginmother_lic_plug',
            'key'      => 'license_id',
            'otherKey' => 'plugin_id',
        ],
        'project' => ['Keios\PluginMother\Models\Project', 'table' => 'keios_pluginmother_lic_proj'],
        'user'    => ['RainLab\User\Models\User', 'table' => 'keios_pluginmother_lic_users'],
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}
