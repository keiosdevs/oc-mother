<?php namespace Keios\PluginMother\Models;

use Keios\PluginMother\Classes\CacheManager;
use Keios\PluginMother\Classes\ThemePoller;
use Keios\ThemeMother\Repositories\ThemeRepository;
use Model;
use System\Models\File;

/**
 * Theme Model
 */
class Theme extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'keios_pluginmother_themes';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Jsonable fields
     */
    protected $jsonable = ['git_branches'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'name',
        'slug',
        'namespace',
        'directory',
        'excerpt',
        'description',
        'git_ssh_path',
        'git_branch',
        'version'
    ];
    /**
     * @var array
     */
    public $hasMany = [
     //   'licenses' => ['Keios\PluginMother\Models\License'],
    ];
    /**
     * @var array
     */
    public $belongsToMany = [
        'projects' => ['Keios\PluginMother\Models\Project', 'table' => 'keios_pluginmother_proj_themes'],
    ];
    /**
     * @var array
     */
    public $attachMany = [
        'screenshots' => [File::class],
    ];

    public function beforeCreate()
    {
        if (!$this->slug) {
            $this->slug = $this->generateSlug($this->name);
        }
    }

    /**
     * @throws \Exception
     */
    public function afterCreate()
    {
        CacheManager::clearThemeList();
        $poller = new ThemePoller();
        $poller->pollTheme($this);
    }

    /**
     * @throws \Exception
     */
    public function afterUpdate()
    {
        CacheManager::clearThemeList();
        $poller = new ThemePoller();
        $poller->pollTheme($this);
    }

    /**
     * @return mixed
     */
    public function getDeployTypeOptions()
    {
        return DeployType::where('is_available', true)->lists('label', 'code');
    }

    /**
     * @param $string
     *
     * @return string
     */
    private function generateSlug($string)
    {
        $repo = new ThemeRepository();
        $slug = strtolower(str_replace([' ', '_'], ['', '-'], $string));
        $no = 1;
        while ($repo->getBySlug($slug)) {
            $slug = strtolower(str_replace([' ', '_'], ['', '-'], $string)).$no;
            ++$no;
        }

        return $slug;
    }
}
