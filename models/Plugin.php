<?php namespace Keios\PluginMother\Models;

use Keios\MoneyRight\Money;
use Keios\PluginMother\Classes\CostCalculator;
use Keios\PluginMother\Classes\CacheManager;
use Keios\PluginMother\Classes\PluginPoller;
use Keios\PluginMother\Facades\MoneyFormatter;
use Keios\PluginMother\Repositories\PluginRepository;
use Model;
use Cms\Classes\Page as CmsPage;
use Url;
use Cms\Classes\Theme;
use System\Models\File;

/**
 * Plugin Model
 */
class Plugin extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'keios_pluginmother_plugins';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Jsonable fields
     */
    protected $jsonable = ['git_branches', 'metadata'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'name',
        'slug',
        'namespace',
        'directory',
        'excerpt',
        'description',
        'git_ssh_path',
        'git_branch',
        'version',
    ];

    /**
     * @var array
     */
    public $hasMany = [

    ];
    /**
     * @var array
     */
    public $belongsToMany = [
        'projects'    => ['Keios\PluginMother\Models\Project', 'table' => 'keios_pluginmother_proj_plug'],
        'recommended' => [
            Plugin::class,
            'table'    => 'keios_pluginmother_plugins_recommendeds',
            'key'      => 'plugin_id',
            'otherKey' => 'recommended_id',
        ],
        'licenses'    => [
            License::class,
            'table'    => 'keios_pluginmother_lic_plug',
            'key'      => 'plugin_id',
            'otherKey' => 'license_id',
        ],
        'groups'      => [
            PluginGroup::class,
            'table'    => 'keios_pluginmother_pg_pivot',
            'key'      => 'plugin_id',
            'otherKey' => 'category_id',
        ],
        'categories'  => [
            PluginCategory::class,
            'table'    => 'keios_pluginmother_pct_pivot',
            'key'      => 'plugin_id',
            'otherKey' => 'category_id',
        ],
    ];
    /**
     * @var array
     */
    public $attachMany = [
        'screenshots' => [File::class],
    ];

    /**
     * @var array
     */
    public $attachOne = [
        'cover' => [File::class],
    ];

    /**
     * @return string
     */
    public function getCoverPath()
    {
        $cover = $this->cover;
        if ($cover) {
            return $this->cover->path;
        }

        return '';
    }

    /**
     *
     */
    public function beforeCreate()
    {
        if (!$this->slug) {
            $this->slug = $this->generateSlug($this->name);
        }
    }

    /**
     * @param string $branch
     * @return string
     */
    public function getBranchPath($branch)
    {
        return base_path('storage/mother_plugins/').strtolower($this->namespace).'/'.strtolower(
                $this->directory
            ).'/'.$branch;
    }

    /**
     * @throws \Exception
     */
    public function afterCreate()
    {
        CacheManager::clearPluginData();
        $poller = new PluginPoller();
        $poller->pollPlugin($this);
    }

    /**
     * @throws \Exception
     */
    public function afterUpdate()
    {
        CacheManager::clearPluginData($this);
        $poller = new PluginPoller();
        $poller->pollPlugin($this);
    }

    /**
     * @return mixed
     */
    public function getDeployTypeOptions()
    {
        return DeployType::where('is_available', true)->lists('label', 'code');
    }

    /**
     * @return mixed
     */
    public function priceStartsAt()
    {
        $costCalculator = new CostCalculator();
        $currency = $costCalculator->getCurrency();
        $result = [];
        foreach ($this->licenses as $license) {
            $result[$license->price] = new Money($license->price, $currency);
        }
        if (count($result) > 0) {
            $key = min(array_keys($result));

            return MoneyFormatter::format($result[$key]);
        }

        $emptyMoney = new Money(0, $currency);

        return MoneyFormatter::format($emptyMoney);
    }

    /**
     * @param $string
     *
     * @return string
     */
    private function generateSlug($string)
    {
        $repo = new PluginRepository();
        $slug = strtolower(str_replace([' ', '_'], ['', '-'], $string));
        $no = 1;
        while ($repo->getBySlug($slug)) {
            $slug = strtolower(str_replace([' ', '_'], ['', '-'], $string)).$no;
            ++$no;
        }

        return $slug;
    }

    /**
     * Handler for the pages.menuitem.getTypeInfo event.
     * Returns a menu item type information. The type information is returned as array
     * with the following elements:
     * - references - a list of the item type reference options. The options are returned in the
     *   ["key"] => "title" format for options that don't have sub-options, and in the format
     *   ["key"] => ["title"=>"Option title", "items"=>[...]] for options that have sub-options. Optional,
     *   required only if the menu item type requires references.
     * - nesting - Boolean value indicating whether the item type supports nested items. Optional,
     *   false if omitted.
     * - dynamicItems - Boolean value indicating whether the item type could generate new menu items.
     *   Optional, false if omitted.
     * - cmsPages - a list of CMS pages (objects of the Cms\Classes\Page class), if the item type requires a CMS page reference to
     *   resolve the item URL.
     * @param string $type Specifies the menu item type
     * @return array Returns an array
     */
    public static function getMenuTypeInfo($type)
    {
        $result = [];

        if ($type == 'plugin') {

            $references = [];
            $plugins = self::orderBy('name')->get();
            foreach ($plugins as $plugin) {
                $references[$plugin->id] = $plugin->name;
            }

            $result = [
                'references'   => $references,
                'nesting'      => false,
                'dynamicItems' => false,
            ];
        }

        if ($type == 'all-plugins') {
            $result = [
                'dynamicItems' => true,
            ];
        }

        if ($result) {
            $theme = Theme::getActiveTheme();

            $pages = CmsPage::listInTheme($theme, true);
            $cmsPages = [];

            foreach ($pages as $page) {
                if (!$page->hasComponent('kom_plugin')) {
                    continue;
                }

                /*
                 * Component must use a categoryPage filter with a routing parameter and post slug
                 * eg: categoryPage = "{{ :somevalue }}", slug = "{{ :somevalue }}"
                 */
                $properties = $page->getComponentProperties('kom_plugin');
                if (!preg_match('/{{\s*:/', $properties['slug'])) {
                    continue;
                }

                $cmsPages[] = $page;
            }

            $result['cmsPages'] = $cmsPages;
        }

        return $result;
    }

    /**
     * Handler for the pages.menuitem.resolveItem event.
     * Returns information about a menu item. The result is an array
     * with the following keys:
     * - url - the menu item URL. Not required for menu item types that return all available records.
     *   The URL should be returned relative to the website root and include the subdirectory, if any.
     *   Use the Url::to() helper to generate the URLs.
     * - isActive - determines whether the menu item is active. Not required for menu item types that
     *   return all available records.
     * - items - an array of arrays with the same keys (url, isActive, items) + the title key.
     *   The items array should be added only if the $item's $nesting property value is TRUE.
     * @param \RainLab\Pages\Classes\MenuItem $item  Specifies the menu item.
     * @param \Cms\Classes\Theme              $theme Specifies the current theme.
     * @param string                          $url   Specifies the current page URL, normalized, in lower case
     *                                               The URL is specified relative to the website root, it includes the subdirectory name, if any.
     * @return mixed Returns an array. Returns null if the item cannot be resolved.
     */
    public static function resolveMenuItem($item, $url, $theme)
    {
        $result = null;
        if ($item->type == 'plugin') {
            if (!$item->reference || !$item->cmsPage) {
                return null;
            }

            $category = self::find($item->reference);
            if (!$category) {
                return null;
            }

            $pageUrl = self::getPluginPageUrl($item->cmsPage, $category, $theme);
            if (!$pageUrl) {
                return null;
            }

            $pageUrl = Url::to($pageUrl);

            $result = [];
            $result['url'] = $pageUrl;
            $result['isActive'] = $pageUrl == $url;
            $result['mtime'] = $category->updated_at;
        } elseif ($item->type == 'all-plugins') {
            $result = [
                'items' => [],
            ];
            $pluginRepo = new PluginRepository();
            $plugins = $pluginRepo->getVisiblePlugins();
            foreach ($plugins as $plugin) {
                $postItem = [
                    'title' => $plugin->name,
                    'url'   => self::getPluginPageUrl($item->cmsPage, $plugin, $theme),
                    'mtime' => $plugin->updated_at,
                ];

                $postItem['isActive'] = $postItem['url'] == $url;

                $result['items'][] = $postItem;
            }
        }

        return $result;
    }

    /**
     * Returns URL of a post page.
     * @param $pageCode
     * @param $category
     * @param $theme
     * @return null|string
     */
    protected static function getPluginPageUrl($pageCode, $category, $theme)
    {
        $page = CmsPage::loadCached($theme, $pageCode);
        if (!$page) {
            return null;
        }

        $properties = $page->getComponentProperties('kom_plugin');
        if (!isset($properties['slug'])) {
            return null;
        }

        /*
         * Extract the routing parameter name from the category filter
         * eg: {{ :someRouteParam }}
         */
        if (!preg_match('/^\{\{([^\}]+)\}\}$/', $properties['slug'], $matches)) {
            return null;
        }

        $paramName = substr(trim($matches[1]), 1);

        return CmsPage::url($page->getBaseFileName(), [$paramName => $category->slug]);
    }
}
