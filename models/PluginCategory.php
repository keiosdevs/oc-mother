<?php namespace Keios\PluginMother\Models;

use Keios\PluginMother\Classes\CacheManager;
use Model;
use Cms\Classes\Page as CmsPage;
use Url;
use Cms\Classes\Theme;
use System\Models\File;

/**
 * PluginCategory Model
 */
class PluginCategory extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'keios_pluginmother_plugin_cat';

    public $jsonable = ['market_plugins', 'metadata'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $belongsToMany = [
        'plugins' => [
            Plugin::class,
            'table'    => 'keios_pluginmother_pct_pivot',
            'key'      => 'category_id',
            'otherKey' => 'plugin_id',
        ],
    ];
    public $attachMany = [
        'screenshots' => [File::class],
    ];

    public $attachOne = [
        'cover' => [File::class],
    ];
    public function afterSave(){
        CacheManager::clearCategoryData($this);
    }

    /**
     * Handler for the pages.menuitem.getTypeInfo event.
     * Returns a menu item type information. The type information is returned as array
     * with the following elements:
     * - references - a list of the item type reference options. The options are returned in the
     *   ["key"] => "title" format for options that don't have sub-options, and in the format
     *   ["key"] => ["title"=>"Option title", "items"=>[...]] for options that have sub-options. Optional,
     *   required only if the menu item type requires references.
     * - nesting - Boolean value indicating whether the item type supports nested items. Optional,
     *   false if omitted.
     * - dynamicItems - Boolean value indicating whether the item type could generate new menu items.
     *   Optional, false if omitted.
     * - cmsPages - a list of CMS pages (objects of the Cms\Classes\Page class), if the item type requires a CMS page reference to
     *   resolve the item URL.
     * @param string $type Specifies the menu item type
     * @return array Returns an array
     */
    public static function getMenuTypeInfo($type)
    {
        $result = [];

        if ($type == 'package') {

            $references = [];
            $packages = self::orderBy('name')->get();
            foreach ($packages as $package) {
                $references[$package->id] = $package->name;
            }

            $result = [
                'references'   => $references,
                'nesting'      => false,
                'dynamicItems' => false,
            ];
        }

        if ($type == 'all-packages') {
            $result = [
                'dynamicItems' => true,
            ];
        }

        if ($result) {
            $theme = Theme::getActiveTheme();

            $pages = CmsPage::listInTheme($theme, true);
            $cmsPages = [];

            foreach ($pages as $page) {
                if (!$page->hasComponent('kom_category')) {
                    continue;
                }

                /*
                 * Component must use a categoryPage filter with a routing parameter and post slug
                 * eg: categoryPage = "{{ :somevalue }}", slug = "{{ :somevalue }}"
                 */
                $properties = $page->getComponentProperties('kom_category');
                if (!preg_match('/{{\s*:/', $properties['slug'])) {
                    continue;
                }

                $cmsPages[] = $page;
            }

            $result['cmsPages'] = $cmsPages;
        }

        return $result;
    }

    /**
     * Handler for the pages.menuitem.resolveItem event.
     * Returns information about a menu item. The result is an array
     * with the following keys:
     * - url - the menu item URL. Not required for menu item types that return all available records.
     *   The URL should be returned relative to the website root and include the subdirectory, if any.
     *   Use the Url::to() helper to generate the URLs.
     * - isActive - determines whether the menu item is active. Not required for menu item types that
     *   return all available records.
     * - items - an array of arrays with the same keys (url, isActive, items) + the title key.
     *   The items array should be added only if the $item's $nesting property value is TRUE.
     * @param \RainLab\Pages\Classes\MenuItem $item  Specifies the menu item.
     * @param \Cms\Classes\Theme              $theme Specifies the current theme.
     * @param string                          $url   Specifies the current page URL, normalized, in lower case
     *                                               The URL is specified relative to the website root, it includes the subdirectory name, if any.
     * @return mixed Returns an array. Returns null if the item cannot be resolved.
     */
    public static function resolveMenuItem($item, $url, $theme)
    {
        $result = null;

        if ($item->type == 'package') {
            if (!$item->reference || !$item->cmsPage) {
                return null;
            }

            $category = self::find($item->reference);
            if (!$category) {
                return null;
            }

            $pageUrl = self::getPackagePageUrl($item->cmsPage, $category, $theme);
            if (!$pageUrl) {
                return null;
            }

            $pageUrl = Url::to($pageUrl);

            $result = [];
            $result['url'] = $pageUrl;
            $result['isActive'] = $pageUrl == $url;
            $result['mtime'] = $category->updated_at;
        } elseif ($item->type == 'all-packages') {
            $result = [
                'items' => [],
            ];

            $packages = self::orderBy('name')->where('is_visible', 1)->where('on_sale', 1)->get();
            foreach ($packages as $package) {
                $postItem = [
                    'title' => $package->name,
                    'url'   => self::getPackagePageUrl($item->cmsPage, $package, $theme),
                    'mtime' => $package->updated_at,
                ];

                $postItem['isActive'] = $postItem['url'] == $url;

                $result['items'][] = $postItem;
            }
        }

        return $result;
    }

    /**
     * Returns URL of a post page.
     * @param $pageCode
     * @param $category
     * @param $theme
     * @return null|string
     */
    protected static function getPackagePageUrl($pageCode, $category, $theme)
    {
        $page = CmsPage::loadCached($theme, $pageCode);
        if (!$page) {
            return null;
        }

        $properties = $page->getComponentProperties('kom_category');
        if (!isset($properties['slug'])) {
            return null;
        }

        /*
         * Extract the routing parameter name from the category filter
         * eg: {{ :someRouteParam }}
         */
        if (!preg_match('/^\{\{([^\}]+)\}\}$/', $properties['slug'], $matches)) {
            return null;
        }

        $paramName = substr(trim($matches[1]), 1);

        return CmsPage::url($page->getBaseFileName(), [$paramName => $category->slug]);
    }
}
