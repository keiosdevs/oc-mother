<?php

namespace Keios\PluginMother\Models;

use Backend\Models\ImportModel;

class PluginImport extends ImportModel
{
    /**
     * @var array The rules to be applied to the data.
     */
    public $rules = [];

    public function importData($results, $sessionKey = null)
    {
        foreach ($results as $row => $data) {

            try {
                $pluginModel = Plugin::where('slug', $data['slug'])->first();
                if(!$pluginModel) {
                    $pluginModel = new Plugin;
                }
                // TODO: replace fill with something with dignity and human reason.
                $pluginModel->fill($data);
                $pluginModel->save();

                $this->logCreated();
            } catch (\Exception $ex) {
                $this->logError($row, $ex->getMessage());
            }

        }
    }
}
