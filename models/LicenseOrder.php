<?php namespace Keios\PluginMother\Models;

use Model;
use Keios\PluginMother\Models\Plugin as PluginModel;

/**
 * Promotion Model
 */
class LicenseOrder extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'keios_pluginmother_financial';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array
     */
    public $belongsTo = [
        'plugin'  => [PluginModel::class],
        'project' => [Project::class],
        'license' => [License::class],
    ];

}
