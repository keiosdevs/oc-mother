<?php namespace Keios\PluginMother\Models;

use Model;

/**
 * ProjectOrder Model
 */
class ProjectOrder extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'keios_pluginmother_project_orders';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'project' => Project::class,

    ];
}
