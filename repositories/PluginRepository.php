<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2/17/17
 * Time: 2:02 AM
 */

namespace Keios\PluginMother\Repositories;

use Keios\PluginMother\Models\Plugin;
use Keios\PluginMother\Models\PluginCategory;
use Keios\PluginMother\Models\PluginGroup;

/**
 * Class PluginRepository
 *
 * @package Keios\MotherPlugin\Repositories
 */
class PluginRepository
{
    /**
     * @param string $slug
     *
     * @return Plugin|null
     */
    public function getBySlug($slug)
    {
        return $this->getPluginBySlug($slug);
    }

    /**
     * @param $aliases
     * @return mixed
     */
    public function getPluginsByAlias($aliases){
        $namespaces = [];
        $dirs = [];
        foreach($aliases as $alias){
            $exploded = explode('.', $alias);
            $namespaces[] = strtolower($exploded[0]);
try {
            $dirs[] = strtolower($exploded[1]);
} catch(\Exception $e){
throw new \ApplicationException('Problem at namespace of plugin ' . $alias);
}
        }

        return Plugin::whereIn('namespace', $namespaces)->whereIn('directory', $dirs)->get();

    }
    /**
     * @param int $perPage
     *
     * @return Plugin
     */
    public function paginateAll($perPage = 20)
    {
        return Plugin
            ::with('categories')
            ->with('groups')
            ->with('cover')
            ->with('screenshots')
            ->paginate($perPage);
    }

    /**
     * @return Plugin
     */
    public function getExternal()
    {
        return Plugin::where('is_external', true)->where('is_depreciated', false)->get();
    }

    /**
     * @return Plugin
     */
    public function getAll()
    {
        return Plugin::with('categories')
            ->with('groups')
            ->with('cover')
            ->with('screenshots')
            ->rememberForever('all_plugins')
            ->get();
    }

    /**
     * @param      $slug
     * @param bool $visibleOnly
     * @return Plugin
     */
    public function getPluginBySlug($slug, $visibleOnly = false)
    {
        $result = Plugin::where('slug', $slug)
            ->with('categories')
            ->with('groups')
            ->with('cover')
            ->with('screenshots');

        if ($visibleOnly) {
            $result = $result->where('is_visible', true);
        }

        return $result->rememberForever('plugin_'.$slug)->first();

    }

    /**
     * @return Plugin[]
     */
    public function getFeaturedPlugins()
    {
        return Plugin::where('is_featured', 1)
            ->with('categories')
            ->with('groups')
            ->where('is_visible', true)
            ->with('cover')
            ->with('screenshots')
            ->rememberForever('featured_plugins')
            ->get();
    }

    /**
     * @param PluginCategory $category
     * @return mixed
     */
    public function getPluginsFromCategory($category){
        return $category->plugins()->where('is_visible', 1)
            ->with('categories')
            ->with('groups')
            ->with('cover')
            ->with('screenshots')
            ->where('is_depreciated', '!=', 1)
            ->orderBy('is_featured', 'DESC')
            ->orderBy('is_library', 'ASC')
            ->orderBy('name', 'ASC')
            ->rememberForever($category->slug.'_plugins')
            ->get();
    }

    /**
     * @return Plugin[]
     */
    public function getVisiblePlugins()
    {
        return Plugin::where('is_visible', 1)
            ->with('categories')
            ->with('groups')
            ->with('cover')
            ->with('screenshots')
            ->where('is_depreciated', '!=', 1)
            ->orderBy('is_featured', 'DESC')
            ->orderBy('is_library', 'ASC')
            ->orderBy('name', 'ASC')
            ->rememberForever('visible_plugins')
            ->get();
    }

    /**
     * @param string $slug
     * @return PluginGroup
     */
    public function getPluginCategory($slug)
    {
        return PluginGroup::where('slug', $slug)->with('plugins')->first();
    }

}
