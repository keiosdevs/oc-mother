<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 4/22/17
 * Time: 2:39 AM
 */

namespace Keios\PluginMother\Repositories;

use Keios\PluginMother\Models\License;
use Keios\PluginMother\Models\LicenseOrder;
use Keios\PluginMother\Models\Plugin;
use Keios\PluginMother\Models\Project;

/**
 * Class LicenseRepository
 * @package Keios\PluginMother\Repositories
 */
class LicenseRepository
{
    /**
     * @var ProjectRepository
     */
    private $projectRepository;

    /**
     * LicenseRepository constructor.
     * @param ProjectRepository|null $projectRepository
     */
    public function __construct(ProjectRepository $projectRepository = null)
    {
        $this->projectRepository = $projectRepository;
        if (!$projectRepository) {
            $this->projectRepository = new ProjectRepository();
        }

    }

    /**
     * @param int $id
     * @return License
     */
    public function getById($id)
    {
        return License::where('id', $id)->first();
    }

    /**
     * @param int $id
     * @return LicenseOrder
     */
    public function getLicenseEntryById($id)
    {
        return LicenseOrder::where('id', $id)->first();
    }

    /**
     * @param $user
     * @return array
     */
    public function getUserLicenseOrders($user)
    {
        $result = [];
        $projects = $this->projectRepository->getByUser($user);
        foreach ($projects as $project) {
            $projectArray = [];
            $projectPluginArray = [];
            foreach ($project->license_orders as $licenseOrder) {
                $projectArray[$licenseOrder->license->id] = $licenseOrder;
                $projectPluginArray[$licenseOrder->license->id][] = $licenseOrder->plugin->name;
            }
            $result[$project->title] = ['licenses' => $projectArray, 'plugins' => $projectPluginArray];
        }

        return $result;
    }

    /**
     * @param Project $project
     * @param Plugin  $plugin
     * @return LicenseOrder
     */
    public function getLicenseEntry(Project $project, Plugin $plugin)
    {
        return LicenseOrder::where('project_id', $project->id)->where('plugin_id', $plugin->id)->first();
    }

    /**
     * @param Project $project
     * @return LicenseOrder[]
     */
    public function getLicenseEntries(Project $project)
    {
        return LicenseOrder::where('project_id', $project->id)->get();
    }
}