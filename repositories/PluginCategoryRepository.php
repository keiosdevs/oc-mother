<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 3/20/17
 * Time: 7:51 PM
 */

namespace Keios\PluginMother\Repositories;

use Keios\PluginMother\Models\PluginCategory;

/**
 * Class PluginCategoryRepository
 * @package Keios\PluginMother\Repositories
 */
class PluginCategoryRepository
{
    /**
     * @return mixed
     */
    public function getAll()
    {
        return PluginCategory::with(
            [
                'plugins' => function ($query) {
                    $query->where('is_visible', 1)->with('cover')->with('screenshots');
                },
            ]
        )->rememberForever('all_categories')->with('cover')->with('screenshots')->get();
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function getBySlug($slug)
    {
        return PluginCategory::with(
            [
                'plugins' => function ($query) {
                    $query->where('is_visible', 1)->with('cover')->with('screenshots');
                },
            ]
        )->where('slug', $slug)->with('cover')->with('screenshots')->first();
    }

    /**
     * @param $name
     * @return mixed
     */
    public function getByName($name)
    {
        return PluginCategory::with(
            [
                'plugins' => function ($query) {
                    $query->where('is_visible', 1)->with('cover')->with('screenshots');
                },
            ]
        )->where('name', $name)->with('cover')->with('screenshots')->first();
    }
}