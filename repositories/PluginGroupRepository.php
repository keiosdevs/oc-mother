<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 3/20/17
 * Time: 7:51 PM
 */

namespace Keios\PluginMother\Repositories;

use Keios\PluginMother\Models\PluginGroup;

/**
 * Class PluginGroupRepository
 * @package Keios\PluginMother\Repositories
 */
class PluginGroupRepository
{
    /**
     * @return mixed
     */
    public function getAll()
    {
        return PluginGroup::with('plugins')->rememberForever('all_plugin_groups')->get();
    }
}