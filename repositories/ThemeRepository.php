<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 3/26/17
 * Time: 9:28 PM
 */

namespace Keios\PluginMother\Repositories;

use Keios\PluginMother\Models\Theme;

class ThemeRepository
{
    /**
     * @param string $slug
     *
     * @return Theme|null
     */
    public function getBySlug($slug)
    {
        return Theme::where('slug', $slug)->first();
    }

    /**
     * @param int $perPage
     *
     * @return Theme
     */
    public function paginateAll($perPage = 20)
    {
        return Theme::paginate($perPage);
    }

    /**
     * @return Theme
     */
    public function getAll()
    {
        return Theme::all();
    }
}
