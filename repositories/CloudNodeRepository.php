<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 3/14/17
 * Time: 8:08 PM
 */

namespace Keios\PluginMother\Repositories;

use Keios\PluginMother\Models\CloudNode;

/**
 * Class CloudNodeRepository
 * @package Keios\PluginMother\Repositories
 */
class CloudNodeRepository
{
    /**
     * @param integer|string $remoteId
     * @return CloudNode|null
     */
    public function getByRemoteId($remoteId){
        return CloudNode::where('remote_id', $remoteId)->first();
    }

    /**
     * @return CloudNode[]
     */
    public function getAll(){
        return CloudNode::all();
    }
}