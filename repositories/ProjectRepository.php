<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2/17/17
 * Time: 2:02 AM
 */

namespace Keios\PluginMother\Repositories;

use Keios\PluginMother\Models\Project;
use RainLab\User\Models\User;

/**
 * Class ProjectRepository
 *
 * @package Keios\MotherPlugin\Repositories
 */
class ProjectRepository
{
    /**
     * @param string $slug
     *
     * @return Project|null
     */
    public function getBySlug($slug)
    {
        return Project::where('slug', $slug)->first();
    }

    /**
     * @param User $user
     * @return Project[]
     */
    public function getByUser($user){
        return Project::where('user_id', $user->id)->with('license_orders')->get();
    }
    /**
     * @param int $id
     *
     * @return Project|null
     */
    public function getById($id)
    {
        return Project::where('id', $id)->first();
    }

    /**
     * @param string $code
     *
     * @return Project|null
     */
    public function getByCode($code)
    {
        return Project::where('code', $code)->with('theme')->with('plugins')->first();
    }

    /**
     * @return Project[]
     */
    public function getAutoUpdatable()
    {
        return Project::where('enable_autoupdate', true)->where('domain', '!=', null)->get();
    }
}