# PluginMother

**This is a huuuge project under heavy development**

OctoberExchange is a plugin set that allows you to create a private plugins market for OctoberCMS.

You can create projects here, assign them your private plugins and connect end-client's October instance to keep your private plugins updated.


### Features

##### Plugins

Create plugin entry, assign it with git repository link and branches configuration for various deployments types.

After creating a plugin, you will be given a webhook URL that you should configure in your repository to automatically update zip artifact provided to end clients.

##### Projects

Create a project, assign it plugins, overwrite branches configuration if required and get a project key to share to your end client.

##### Deploy Types

Create for example "production" and "edge" types. Assign proper git branches to these in your plugin settings. Set up deploy_type to "edge" in end-client's October config to keep it synced with proper type of deployment.

##### Licenses

Work in Progress

### Configuration

For PluginMother:

- Install the plugin
- Review and change Deploy Types
- Create plugins

### Commands

```
php artisan exchange:sync-plugins
```

Fetches latest repositories and creates zip artifacts. Updates all plugins, may take a while. Recommended method to update artifacts is by using webhook.

```
php artisan exchange:update-spawns
```

Sends update requests to projects with auto-update enabled. Requires proper domain to be set in Project settings.

```
php artisan exchange:discover-vultr
```

Downloads list of nodes from Vultr and syncs it with the database.

```
php artisan exchange:sync-external
```
Works like exchange:sync-plugins but only for plugins marked as external. If option enabled in settings, this will trigger as scheduled (default: 6 AM every day) as long as artisan schedule:run is set in cron.

### Schedule

Projects with properly set domain and autoupdate option enabled can be scheduled to run automatically. Visit Settings page to configure the schedule.

### Categories

Categories are the most important entities e-commerce wise. They allow to create set of plugins and present them on the frontend.

Categories include metadata, which you can use to store your own values.

### Projects

Important feature to note with project is include_git_repo option.

To make it working, you should have add to your env `ENABLE_GITLESS=1` and resync mother.
