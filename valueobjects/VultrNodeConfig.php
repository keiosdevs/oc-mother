<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2/28/17
 * Time: 3:54 PM
 */

namespace Keios\PluginMother\ValueObjects;

class VultrNodeConfig
{
    
    
    /**
     * Location to create this virtual machine in.  See v1/regions/list
     *
     * @var integer
     */
    public $DCID;
    
    /**
     * Plan to use when creating this virtual machine.  See v1/plans/list
     *
     * @var integer
     */
    public $VPSPLANID;
    
    /**
     * Operating system to use.  See v1/os/list
     *
     * @var integer
     */
    public $OSID;
    
    /**
     * (optional) If you've selected the 'custom' operating system, this can be set to chainload the specified URL on bootup, via iPXE
     *
     * @var string
     */
    public $ipxe_chain_url;
    
    /**
     * (optional)  If you've selected the 'custom' operating system, this is the ID of a specific ISO to mount during the deployment
     *
     * @var string
     */
    public $ISOID;
    
    /**
     * (optional) If you've not selected a 'custom' operating system, this can be the SCRIPTID of a startup script to execute on boot.  See v1/startupscript/list
     *
     * @var integer
     */
    public $SCRIPTID;
    
    /**
     * (optional) If you've selected the 'snapshot' operating system, this should be the SNAPSHOTID (see v1/snapshot/list) to restore for the initial installation
     *
     * @var string
     */
    public $SNAPSHOTID;
    
    /**
     * (optional) 'yes' or 'no'.  If yes, an IPv6 subnet will be assigned to the machine (where available)
     *
     * @var string
     */
    public $enable_ipv6;
    
    /**
     * (optional) 'yes' or 'no'. If yes, private networking support will be added to the new server.
     *
     * @var string
     */
    public $enable_private_network;
    
    /**
     * (optional) This is a text label that will be shown in the control panel
     *
     * @var string
     */
    public $label;
    
    /**
     * (optional) List of SSH keys to apply to this server on install (only valid for Linux/FreeBSD).  See v1/sshkey/list.  Seperate keys with commas
     *
     * @var string
     */
    public $SSHKEYID;
    
    /**
     * (optional) 'yes' or 'no'.  If yes, automatic backups will be enabled for this server (these have an extra charge associated with them)
     *
     * @var string
     */
    public $auto_backups;
    
    /**
     * (optional) If launching an application (OSID 186), this is the APPID to launch. See v1/app/list.
     *
     * @var integer
     */
    public $APPID;
    
    /**
     * (optional) Base64 encoded cloud-init user-data
     *
     * @var string
     */
    public $userdata;
    
    /**
     * (optional, default 'yes') 'yes' or 'no'. If yes, an activation email will be sent when the server is ready.
     *
     * @var string
     */
    public $notify_activate;
    
    /**
     * optional, default 'no') 'yes' or 'no'.  If yes, DDOS protection will be enabled on the subscription (there is an additional charge for this)
     *
     * @var (
     */
    public $ddos_protection;
    
    /**
     * (optional) IP address of the floating IP to use as the main IP of this server
     *
     * @var string
     */
    public $reserved_ip_v4;
    
    /**
     * (optional) The hostname to assign to this server.
     *
     * @var string
     */
    public $hostname;
    
    /**
     * (optional) The tag to assign to this server.
     *
     * @var string
     */
    public $tag;
    
    /**
     * (optional) The firewall group to assign to this server. See /v1/firewall/group_list.
     *
     * @var string
     */
    public $FIREWALLGROUPID;

}