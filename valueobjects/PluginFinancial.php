<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 4/22/17
 * Time: 5:34 PM
 */

namespace Keios\PluginMother\ValueObjects;

use Keios\MoneyRight\Currency;
use Keios\MoneyRight\Money;
use Keios\PluginMother\Models\License;
use Keios\PluginMother\Models\LicenseOrder;
use Keios\PluginMother\Models\Plugin;
use Keios\PluginMother\Models\Project;

/**
 * Class ProjectFinancial
 * @package Keios\PluginMother\ValueObjects
 */
class PluginFinancial
{
    /**
     * @var integer
     */
    public $key;
    /**
     * @var LicenseOrder
     */
    public $licenseOrder;

    /**
     * @var License
     */
    public $license;

    /**
     * @var Project
     */
    public $project;

    /**
     * @var Plugin
     */
    public $plugin;

    /**
     * @var Money
     */
    public $value;

    /**
     * @var float|int
     */
    public $amount;

    /**
     * @var string
     */
    public $currency;

    /**
     * @var bool
     */
    public $isPaid;

    /**
     * Days amount
     *
     * @var integer
     */
    public $validFor;

    /**
     * ProjectFinancial constructor.
     * @param LicenseOrder|null $licenseOrder
     * @throws \Keios\MoneyRight\Exceptions\UnknownCurrencyException
     */
    public function __construct(LicenseOrder $licenseOrder = null)
    {
        if ($licenseOrder) {
            $this->licenseOrder = $licenseOrder;
            $this->key = $licenseOrder->id;
            $this->license = $licenseOrder->license;
            $this->project = $licenseOrder->project;
            $this->plugin = $licenseOrder->plugin;
            $this->amount = $licenseOrder->amount;
            $this->currency = $licenseOrder->currency;
            $this->isPaid = $licenseOrder->is_paid;
            $this->value = new Money($this->amount, new Currency($this->currency));
        }
    }
}