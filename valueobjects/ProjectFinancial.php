<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 4/22/17
 * Time: 5:34 PM
 */

namespace Keios\PluginMother\ValueObjects;

use Keios\MoneyRight\Money;
use Keios\PluginMother\Models\Project;

/**
 * Class ProjectFinancial
 * @package Keios\PluginMother\ValueObjects
 */
class ProjectFinancial
{
    /**
     * @var PluginFinancial[]
     */
    public $pluginFinancials = [];

    /**
     * @var Project
     */
    public $project;
    /**
     * Currency keyed totals
     *
     * @var Money[]
     */
    public $totals;

    /**
     * @var Money
     */
    public $total;

    /**
     * @var Money
     */
    public $paid;

    /**
     * @var Money
     */
    public $unpaid;

    /**
     * @var Money
     */
    public $isPaid;

    /**
     * @var int
     */
    public $tax = 0;

    /**
     * ProjectFinancial constructor.
     * @param Project $project
     */
    public function __construct(Project $project)
    {
        $this->project = $project;
    }

}