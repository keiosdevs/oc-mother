<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2/28/17
 * Time: 3:51 PM
 */

namespace Keios\PluginMother\Exceptions;


use Keios\PluginMother\Contracts\MotherException;
use October\Rain\Exception\ValidationException;
use Validator;

/**
 * Class AuthorisationException
 *
 * @package Keios\PluginMother\Exceptions
 */
class InvalidRequestException extends ValidationException implements MotherException
{

    public $requestMessages;

    public function __construct($validation = null)
    {
        parent::__construct($validation);
        if ($validation instanceof Validator) {
            $this->requestMessages = $validation->messages();
        }
    }

    /**
     * @return string
     */
    public function getResponseMessage()
    {
        return json_encode($this->errors);
    }

    /**
     * @return int
     */
    public function getResponseCode()
    {
        return 400;
    }
}