<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 3/4/17
 * Time: 7:54 PM
 */

namespace Keios\PluginMother\Exceptions;


use ApplicationException;
use Keios\PluginMother\Contracts\MotherException;

class CloudConnectionException extends ApplicationException implements MotherException
{

    public function getResponseCode()
    {
        return 500;
    }

    public function getResponseMessage()
    {
        return 'Cannot connect to cloud!';
    }
}