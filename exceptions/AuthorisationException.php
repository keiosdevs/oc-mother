<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2/28/17
 * Time: 3:51 PM
 */

namespace Keios\PluginMother\Exceptions;


use Keios\PluginMother\Contracts\MotherException;
use October\Rain\Exception\ApplicationException;

/**
 * Class AuthorisationException
 *
 * @package Keios\PluginMother\Exceptions
 */
class AuthorisationException extends ApplicationException implements MotherException
{
    /**
     * @return string
     */
    public function getResponseMessage()
    {
        return \Lang::trans('keios.pluginmother::lang.errors.not_authorized');
    }

    /**
     * @return int
     */
    public function getResponseCode()
    {
        return 401;
    }
}