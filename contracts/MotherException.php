<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2/28/17
 * Time: 3:54 PM
 */

namespace Keios\PluginMother\Contracts;


/**
 * Interface MotherException
 *
 * @package Keios\PluginMother\Contracts
 */
interface MotherException
{
    /**
     * @return string
     */
    public function getResponseMessage();

    /**
     * @return int
     */
    public function getResponseCode();
}