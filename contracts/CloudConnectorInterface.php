<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 3/4/17
 * Time: 7:34 PM
 */

namespace Keios\PluginMother\Contracts;


interface CloudConnectorInterface
{

    public function authorize($apiKey = null);

    public function getAccountInfo();

    public function createNode($configInterface);

    public function snapshotNode();

    public function destroyNode();

    public function rebootNode();

    public function shutdownNode();

    public function addRam();

    public function addCpu();

    public function addSshKey();

    public function getRegions();

}