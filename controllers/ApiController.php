<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2/17/17
 * Time: 1:33 AM
 */

namespace Keios\PluginMother\Controllers;

use Carbon\Carbon;
use Crypt;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Input;
use Keios\PluginMother\Classes\DeployTools;
use Keios\PluginMother\Classes\NotificationSender;
use Keios\PluginMother\Classes\PluginPoller;
use Keios\PluginMother\Classes\ThemePoller;
use Keios\PluginMother\Contracts\MotherException;
use Keios\PluginMother\Exceptions\AuthorisationException;
use Keios\PluginMother\Exceptions\InvalidRequestException;
use Keios\PluginMother\Exceptions\ProjectSupportException;
use Keios\PluginMother\Models\Plugin;
use Keios\PluginMother\Models\Project;
use Keios\PluginMother\Models\Settings;
use Keios\PluginMother\Models\Theme;
use Keios\PluginMother\Repositories\PluginRepository;
use Keios\PluginMother\Repositories\ProjectRepository;
use Keios\PluginMother\Repositories\ThemeRepository;

/**
 * Class ApiController
 *
 * Responsible for online list management
 *
 * @package Pixelpixel\Motogamers\Controllers
 */
class ApiController extends Controller
{
    /**
     * @var array
     */
    private $postData;

    /**
     * @var ProjectRepository
     */
    private $projectRepo;

    /**
     * @var PluginRepository
     */
    private $pluginRepo;

    /**
     * @var DeployTools
     */
    private $deployTools;

    /**
     * @var ThemeRepository
     */
    private $themeRepo;

    /**
     * ApiController constructor.
     */
    public function __construct()
    {
        $this->projectRepo = new ProjectRepository();
        $this->pluginRepo = new PluginRepository();
        $this->themeRepo = new ThemeRepository();
        $this->deployTools = new DeployTools();
        $this->postData = Input::all();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Keios\PluginMother\Exceptions\ProjectSupportException
     * @throws \ValidationException
     */
    public function authorize()
    {
        $this->validateAuthorizeRequest();
        try {
            $project = $this->authorizeProject();
        } catch (MotherException $e) {
            return \Response::json(['message' => $e->getMessage()], $e->getResponseCode());
        }
        $pluginModels = $project->plugins;
        $deployType = $this->postData['deploy_type'];
        $overrides = $project->plugin_overrides;
        if (!$overrides) {
            $overrides = [];
        }
        $response = $this->preparePluginsArray($pluginModels, $deployType, $overrides, $project);
        $pre = $project->pre_market_plugins;
        if (!$pre) {
            $pre = [];
        }
        $pre = array_flatten($pre);
        $themeSlug = '';
        if ($project->theme) {
            $themeSlug = $project->theme->slug;
        }
        $themes = [];
        foreach ($project->themes as $theme) {
            $themes[] = $theme->slug;
        }
        $responseData = [
            'project' => $project->title,
            'pre'     => $pre,
            'plugins' => [],
            'theme'   => '',
            'themes'  => [],
        ];
        $headers = [];

        if ($project->lock_console) {
            $headers = [
                'Access-Control-Allow-Origin' => $project->domain,
            ];
            if (array_key_exists('running_mode', $this->postData)
                && $this->postData['running_mode'] !== 'cli'
                && $this->matchDomain($project)) {
                $responseData['plugins'] = $response;
                $responseData['theme'] = $themeSlug;
                $responseData['themes'] = $themes;
            } else {
                \Log::error(
                    'UNAUTHORIZED SYNC ATTEMPT FOR PROJECT ID'.$project->id
                    .' assigned to domain '.$project->domain.PHP_EOL
                    .' Here are details from $_SERVER var: '.print_r($_SERVER, true).PHP_EOL.PHP_EOL
                    .' Here is Post Request sent: '.print_r($this->postData, true)
                );
            }
        } else {
            $responseData['plugins'] = $response;
            $responseData['theme'] = $themeSlug;
            $responseData['themes'] = $themes;
        }

        return \Response::json(
            $responseData,
            200,
            $headers
        );
    }

    /**
     * @return Project|null
     * @throws \ValidationException
     * @throws \Keios\PluginMother\Exceptions\AuthorisationException
     * @throws AuthorisationException
     * @throws ProjectSupportException
     */
    private function authorizeProject()
    {
        $this->validateAuthorizeRequest();
        $project = $this->projectRepo->getByCode($this->postData['project_key']);
        $settings = Settings::instance();
        if ($settings->get('reject_disabled')) {
            if (!$project->is_enabled) {
                throw new ProjectSupportException('Project disabled. Enable it in your management panel.');
            }
        }
        if ($settings->get('reject_unpaid')) {
            if (!$project->is_paid) {
                throw new ProjectSupportException('Project unpaid. Contact us.');
            }
        }
        if ($settings->get('reject_unsupported')) {
            if (!$project->is_supported) {
                throw new ProjectSupportException('Project out of support. Contact us.');
            }
        }
        if (!$project) {
            throw new AuthorisationException('Unauthorized');
        }

        return $project;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Keios\PluginMother\Exceptions\ProjectSupportException
     * @throws \Keios\PluginMother\Exceptions\InvalidRequestException
     * @throws \ValidationException
     */
    public function receiveUpdate()
    {
        try {
            $project = $this->authorizeProject();
        } catch (MotherException $e) {
            return \Response::json(['message' => $e->getResponseMessage()], $e->getResponseCode());
        }
        $this->validateUpdateRequest();
        $status = json_decode($this->postData['status'], true);
        if (!$status) {
            $status = [];
        }
        if (array_key_exists('updated_count', $status) && $status['updated_count'] > 0) {
            $project->last_update_status = $this->postData['status'];
            $project->last_update_date = Carbon::now();
            $project->save();
        }
        $notifSender = new NotificationSender();
        $notifSender->send($this->postData['message']);

        return \Response::json(['message' => 'OK'], 200);
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Keios\PluginMother\Exceptions\ProjectSupportException
     */
    public function checkTheme()
    {
        // TODO: check if request comes from domain authorized in projects
        $data = $this->postData;
        try {
            $project = $this->authorizeProject();
        } catch (MotherException $e) {
            return \Response::json(['message' => $e->getResponseMessage()], $e->getResponseCode());
        }
        $this->validateCheckTheme($data);
        $theme = $this->themeRepo->getBySlug($data['slug']);
        $overrides = $this->deployTools->getOverrides($project);
        $branchCode = $this->deployTools->findThemeBranch($theme, $data['deploy_type'], $overrides, true);
        $zipPath = storage_path().'/mother_themes/'.$branchCode.'/'.$data['slug'].'.zip';
        if (!is_file($zipPath)) {
            return \Response(['message' => 'No slug provided'], 500);
        }

        return \Response(['size' => filesize($zipPath)], 200);
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Keios\PluginMother\Exceptions\ProjectSupportException
     */
    public function getThemeVersion()
    {
        try {
            $project = $this->authorizeProject();
        } catch (MotherException $e) {
            return \Response::json(['message' => $e->getResponseMessage()], $e->getResponseCode());
        }
        $deployType = $this->postData['deploy_type'];

        $theme = Theme::where('slug', $this->postData['alias'])->first();
        if (!$theme) {
            return \Response(['message' => 'Not found'], 404);
        }
        $branchCode = $this->deployTools->findThemeBranch(
            $theme,
            $deployType,
            $this->deployTools->getOverrides($project),
            true
        );
        $versionFile = storage_path(
            ).'/mother_themes/'.$this->postData['alias'].'/'.$branchCode.'/updates/version.yaml';
        if (!is_file($versionFile)) {
            return \Response(['message' => 'Version not found'], 404);
        }
        $json = file_get_contents($versionFile);
        $array = \Yaml::parse($json);
        $keys = array_keys($array);

        return \Response(['version' => max($keys)], 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse|mixed
     * @throws \Keios\PluginMother\Exceptions\ProjectSupportException
     */
    public function getCurrentThemesState()
    {
        try {
            $project = $this->authorizeProject();
        } catch (MotherException $e) {
            return \Response::json(['message' => $e->getResponseMessage()], $e->getResponseCode());
        }
        $deployType = $this->postData['deploy_type'];
        $cachedState = \Cache::rememberForever(
            $deployType.'_themes_list',
            function () use ($deployType, $project) {
                $result = [];
                $themes = Theme::all();
                foreach ($themes as $theme) {
                    $branchCode = $this->deployTools->findThemeBranch(
                        $theme,
                        $deployType,
                        $this->deployTools->getOverrides($project),
                        true
                    );
                    $zipPath = storage_path().'/mother_themes/'.$branchCode.'/'.$theme->slug.'.zip';
                    if (file_exists($zipPath)) {
                        $result[$theme->slug] = ['size' => filesize($zipPath), 'branch_code' => $branchCode];
                    }
                }

                return $result;
            }
        );

        return $cachedState;
    }

    /**
     * @param string $type
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Keios\PluginMother\Exceptions\ProjectSupportException
     * @throws \Keios\PluginMother\Exceptions\InvalidRequestException
     * @throws \ValidationException
     */
    public function getZip($type = 'plugin')
    {
        try {
            $project = $this->authorizeProject();
        } catch (MotherException $e) {
            return \Response::json(['message' => $e->getResponseMessage()], $e->getResponseCode());
        }
        $this->validateGetZipRequest();
        $slug = $this->postData['slug'];
        if ($type === 'theme') {
            $path = $this->getThemeZipPath($project, $slug);
        } else {
            $path = $this->getPluginZipPath($project, $slug);
        }
        if (!$path) {
            return \Response(['message' => 'Not found'], 404);
        }

        return \Response::download($path);
    }

    /**
     * @param Project $project
     * @param string  $slug
     * @return null|string
     */
    private function getPluginZipPath($project, $slug)
    {
        $plugin = $this->pluginRepo->getBySlug($slug);
        if (!$plugin) {
            return null;
        }
        $deployType = $this->deployTools->findDeployType($this->postData);
        $overrides = $project->plugin_overrides;
        if (!$overrides) {
            $overrides = [];
        }
        $branchCode = $this->deployTools->findPluginBranch($plugin, $deployType, $overrides, true);
        $path = storage_path().'/mother_plugins/'.$branchCode.'/'.$slug.'.zip';
        if(env('ENABLE_GITLESS') && !$project->include_git_repo){
          $path = storage_path().'/mother_plugins/'.$branchCode.'/'.$slug.'_gitless.zip';
        }

        return $path;
    }

    /**
     * @param Project $project
     * @param string  $slug
     * @return null|string
     */
    private function getThemeZipPath($project, $slug)
    {
        $theme = $this->themeRepo->getBySlug($slug);
        if (!$theme) {
            return null;
        }
        $deployType = $this->deployTools->findDeployType($this->postData);
        $overrides = $project->theme_overrides;
        if (!$overrides) {
            $overrides = [];
        }
        $branchCode = $this->deployTools->findThemeBranch($theme, $deployType, $overrides, true);

        $path = storage_path().'/mother_themes/'.$branchCode.'/'.$slug.'.zip';
        if(env('ENABLE_GITLESS') && !$project->include_git_repo){
          $path = storage_path().'/mother_themes/'.$branchCode.'/'.$slug.'_gitless.zip';
        }
        return $path;
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response|\Symfony\Component\HttpFoundation\Response
     * @throws \Keios\PluginMother\Exceptions\ProjectSupportException
     * @throws \ApplicationException
     */
    public function getPluginChangelog()
    {
        try {
            $project = $this->authorizeProject();
        } catch (MotherException $e) {
            return \Response::json(['message' => $e->getResponseMessage()], $e->getResponseCode());
        }
        $deployType = $this->postData['deploy_type'];
        $alias = $this->getPluginArray($this->postData['alias']);
        $changelog = $this->getChangelog($alias, $deployType, $project);

        return \Response(['changelog' => $changelog], 200);
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response|\Symfony\Component\HttpFoundation\Response
     * @throws \Keios\PluginMother\Exceptions\ProjectSupportException
     * @throws \ApplicationException
     */
    public function getPluginVersion()
    {
        try {
            $project = $this->authorizeProject();
        } catch (MotherException $e) {
            return \Response::json(['message' => $e->getResponseMessage()], $e->getResponseCode());
        }
        $deployType = $this->postData['deploy_type'];
        $alias = $this->getPluginArray($this->postData['alias']);
        $version = $this->findVersion($alias, $deployType, $project);

        return \Response(['version' => $version], 200);
    }

    /**
     * @param array   $aliasArray
     * @param string  $deployType
     * @param Project $project
     * @return \Illuminate\Contracts\Routing\ResponseFactory|mixed|\Symfony\Component\HttpFoundation\Response
     */
    private function getChangelog($aliasArray, $deployType, $project)
    {
        $plugin = Plugin::where('namespace', $aliasArray['namespace'])->where('directory', $aliasArray['name'])->first(
        );
        if (!$plugin) {
            return \Response(['message' => 'Not found'], 404);
        }
        $branchCode = $this->deployTools->findPluginBranch(
            $plugin,
            $deployType,
            $this->deployTools->getOverrides($project),
            true
        );
        $dir = strtolower($aliasArray['namespace']).'/'.strtolower($aliasArray['name']);
        $versionFile = storage_path().'/mother_plugins/'.$dir.'/'.$branchCode.'/updates/version.yaml';
        if (!is_file($versionFile)) {
            return \Response(['message' => 'Version not found'], 404);
        }
        $json = file_get_contents($versionFile);

        return \Yaml::parse($json);
    }

    /**
     * @param array   $aliasArray
     * @param string  $deployType
     * @param Project $project
     * @return \Illuminate\Contracts\Routing\ResponseFactory|mixed|\Symfony\Component\HttpFoundation\Response
     */
    private function findVersion($aliasArray, $deployType, $project)
    {

        $plugin = Plugin::where('namespace', $aliasArray['namespace'])->where('directory', $aliasArray['name'])->first(
        );
        if (!$plugin) {
            return \Response(['message' => 'Not found'], 404);
        }
        $branchCode = $this->deployTools->findPluginBranch(
            $plugin,
            $deployType,
            $this->deployTools->getOverrides($project),
            true
        );
        $dir = strtolower($aliasArray['namespace']).'/'.strtolower($aliasArray['name']);
        $versionFile = storage_path().'/mother_plugins/'.$dir.'/'.$branchCode.'/updates/version.yaml';
        if (!is_file($versionFile)) {
            return \Response(['message' => 'Version not found'], 404);
        }
        $json = file_get_contents($versionFile);
        $array = \Yaml::parse($json);
        $keys = array_keys($array);

        return max($keys);
    }

    /**
     * @param string $alias
     * @return array
     * @throws \ApplicationException
     */
    private function getPluginArray($alias)
    {
        $array = explode('.', $alias);
        if (count($array) === 2) {
            $result = [
                'namespace' => $array[0],
                'name'      => $array[1],
            ];

            return $result;
        }
        throw new \ApplicationException('Invalid alias');
    }

    /**
     * @return array|\Illuminate\Http\JsonResponse
     * @throws \Keios\PluginMother\Exceptions\ProjectSupportException
     * @throws \ValidationException
     */
    public function getCurrentState()
    {
        try {
            $project = $this->authorizeProject();
        } catch (MotherException $e) {
            return \Response::json(['message' => $e->getResponseMessage()], $e->getResponseCode());
        }
        $deployType = $this->postData['deploy_type'];
        $cachedState = \Cache::rememberForever(
            $deployType.'_plugins_list',
            function () use ($deployType, $project) {
                $result = [];
                $plugins = Plugin::all();
                foreach ($plugins as $plugin) {
                    $branchCode = $this->deployTools->findPluginBranch(
                        $plugin,
                        $deployType,
                        $this->deployTools->getOverrides($project),
                        true
                    );
                    $zipPath = storage_path().'/mother_plugins/'.$branchCode.'/'.$plugin->slug.'.zip';
                    if (file_exists($zipPath)) {
                        $result[$plugin->slug] = ['size' => filesize($zipPath), 'branch_code' => $branchCode];
                    }
                }

                return $result;
            }
        );

        return $cachedState;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Keios\PluginMother\Exceptions\ProjectSupportException
     * @throws \ValidationException
     */
    public function getProjectDetails()
    {
        $data = Input::all();
        try {
            $project = $this->authorizeProject();
        } catch (MotherException $e) {
            return \Response::json(['message' => $e->getResponseMessage()], $e->getResponseCode());
        }
        $result = [];
        $plugins = $project->plugins;
        foreach ($plugins as $plugin) {
            $branches = $plugin->git_branches;
            $result[$plugin->slug] = $plugin->git_branch;
            if (!$branches) {
                $branches = [];
            }
            foreach ($branches as $branch) {
                if ($branch['deploy_type'] === $data['deploy_type']) {
                    $result[$plugin->slug] = $branch['deploy_branch'];
                }
            }
        }
        $overrides = $project->plugin_overrides;
        foreach ($overrides as $override) {
            $slug = $override['plugin_slug'];
            if (array_key_exists($slug, $result)) {
                $result[$slug] = $override['plugin_branch'];
            }
        }

        return \Response::json($result, 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Keios\PluginMother\Exceptions\ProjectSupportException
     * @throws \ValidationException
     */
    public function getProjectTheme()
    {
        $data = Input::all();
        try {
            $project = $this->authorizeProject();
        } catch (MotherException $e) {
            return \Response::json(['message' => $e->getResponseMessage()], $e->getResponseCode());
        }
        $result = [];
        $theme = null;
        if (array_key_exists('slug', $data)) {
            $theme = $this->themeRepo->getBySlug($data['slug']);
        }

        if (!$theme) {
            $theme = $project->theme;
        }
        if (!$theme) {
            return \Response::json([], 404);
        }

        $branches = $theme->git_branches;
        $result[$theme->slug] = $theme->git_branch;
        foreach ($branches as $branch) {
            if ($branch['deploy_type'] === $data['deploy_type']) {
                $result[$theme->slug] = $branch['deploy_branch'];
            }
        }

        $overrides = $project->theme_verrides;
        if (!$overrides) {
            $overrides = [];
        }
        $result['name'] = $theme->name;
        $result['slug'] = $theme->slug;
        foreach ($overrides as $override) {
            $slug = $override['theme_slug'];
            if (array_key_exists($slug, $result)) {
                $result[$slug] = $override['theme_branch'];
            }
        }

        return \Response::json($result, 200);
    }

    /**
     * @param string $encodedSlug
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Contracts\Encryption\DecryptException
     * @throws \ApplicationException
     * @throws \Exception
     *
     */
    public function receiveHook($encodedSlug)
    {
        // $data = \Input::all(); // todo: some validation maybe.
        $slug = Crypt::decrypt($encodedSlug);
        $plugin = $this->pluginRepo->getBySlug($slug);
        $poller = new PluginPoller();
        $poller->pollPlugin($plugin);
        \Log::info($slug.' '.\Lang::trans('keios.pluginmother::lang.messages.has_been_synced_with_upstream'));
        if (!$plugin) {
            return \Response::json(['message' => 'Not found'], 404);
        }

        return \Response::json(['message' => 'OK'], 200);
    }

    /**
     * @param string $encodedSlug
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Contracts\Encryption\DecryptException
     * @throws \ApplicationException
     * @throws \Exception
     *
     */
    public function receiveThemeHook($encodedSlug)
    {
        // $data = \Input::all(); // todo: some validation maybe.
        $slug = Crypt::decrypt($encodedSlug);
        $theme = $this->themeRepo->getBySlug($slug);
        $poller = new ThemePoller();
        $poller->pollTheme($theme);
        \Log::info($slug.' '.\Lang::trans('keios.pluginmother::lang.messages.has_been_synced_with_upstream'));
        if (!$theme) {
            return \Response::json(['message' => 'Not found'], 404);
        }

        return \Response::json(['message' => 'OK'], 200);
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Keios\PluginMother\Exceptions\ProjectSupportException
     * @throws \ValidationException
     * @throws \Keios\PluginMother\Exceptions\InvalidRequestException
     */
    public function checkPlugin()
    {
        // TODO: check if request comes from domain authorized in projects
        $data = $this->postData;
        try {
            $project = $this->authorizeProject();
        } catch (MotherException $e) {
            return \Response::json(['message' => $e->getResponseMessage()], $e->getResponseCode());
        }
        $this->validateCheckPlugin($data);
        $plugin = $this->pluginRepo->getBySlug($data['slug']);
        $overrides = $this->deployTools->getOverrides($project);
        $branchCode = $this->deployTools->findPluginBranch($plugin, $data['deploy_type'], $overrides, true);
        $zipPath = storage_path().'/mother_plugins/'.$branchCode.'/'.$data['slug'].'.zip';
        if (!is_file($zipPath)) {
            return \Response(['message' => 'No slug provided'], 500);
        }

        return \Response(['size' => filesize($zipPath)], 200);
    }

    /**
     * @var array
     */
    private $deps = [];

    /**
     * @param Plugin[] $pluginModels
     * @param string   $deployType
     * @param array    $overrides
     * @param          $project
     * @return array
     */
    public function preparePluginsArray($pluginModels, $deployType, $overrides, $project)
    {
        $response = [];
        /** @var Plugin $pluginModel */
        $this->deps = [];
        foreach ($pluginModels as $pluginModel) {
            $response[$pluginModel->slug] = $this->getPluginResponseArray(
                $pluginModel,
                $deployType,
                $overrides,
                $project
            );
        }

        $settings = Settings::instance();
        if ($settings->get('enable_dependencies')) {
            $dependencies = $this->pluginRepo->getPluginsByAlias($this->deps);

            /** @var Plugin $dependency */
            foreach ($dependencies as $dependency) {
                $response[$dependency->slug] = $this->getPluginResponseArray(
                    $dependency,
                    $deployType,
                    $overrides,
                    $project
                );
            }
        }

        return $response;
    }

    /**
     * @param Plugin  $pluginModel
     * @param string  $deployType
     * @param array   $overrides
     * @param Project $project
     * @return array
     * @throws \ApplicationException
     */
    public function getPluginResponseArray($pluginModel, $deployType, $overrides, $project)
    {
        $zipPath = storage_path().'/mother_plugins/'.$deployType.'/'.$pluginModel->slug.'.zip';
        if (!is_file($zipPath)) {
            //TODO: pull and zip this plugin!
        }
        $branch = $this->deployTools->findPluginBranch($pluginModel, $deployType, $overrides, false);
        if (array_key_exists('alias', $this->postData)) {
            $alias = $this->postData['alias'];
        } else {
            $alias = $pluginModel->namespace.'.'.$pluginModel->directory;
        }
        $alias = $this->getPluginArray($alias);

        $this->loadDependencies($pluginModel, $branch);
        $result = [
            'name'         => $pluginModel->name,
            'git_ssh_path' => $pluginModel->git_ssh_path,
            'git_branch'   => $branch,
            'namespace'    => $pluginModel->namespace,
            'directory'    => $pluginModel->directory,
            'slug'         => $pluginModel->slug,
            'version'      => $this->findVersion($alias, $deployType, $project),
        ];

        return $result;
    }

    /**
     * @param Plugin $pluginModel
     * @param string $branch
     *
     * @return array
     */
    public function loadDependencies($pluginModel, $branch)
    {
        $repoPath = $pluginModel->getBranchPath($branch);
        $pluginFile = file_get_contents($repoPath.'/Plugin.php');
        $result = [];
        preg_match_all('|require = \[[\s\S]*?];|', $pluginFile, $mainMatch);

        if (count($mainMatch) > 0) {
            $match = $mainMatch[0];
            if (count($match)) {
                $requiresString = $match[0];
                $requires = explode(PHP_EOL, $requiresString);
                if (count($requires) > 2) {
                    unset($requires[0], $requires[max(array_keys($requires))]);
                }
                foreach ($requires as $dep) {
                    if (strpos($dep, '//') !== false || strpos($dep, '/*') !== false) {
                       continue;
                    }
                    $dep = str_replace([' ', ',', '\''], '', $dep);
                    $this->deps[] = $dep;
                    $result[] = $dep;
                }
            }
        }

        return $result;
    }

    /**
     * @param array $data
     *
     * @throws InvalidRequestException
     * @throws \Keios\PluginMother\Exceptions\InvalidRequestException
     */
    private function validateCheckPlugin(array $data)
    {
        $rules = [
            'slug'        => 'required',
            'deploy_type' => 'required',
            'project_key' => 'required',
        ];
        $v = \Validator::make($data, $rules);
        if ($v->fails()) {
            throw new InvalidRequestException($v);
        }
    }

    /**
     * @param array $data
     *
     * @throws InvalidRequestException
     * @throws \Keios\PluginMother\Exceptions\InvalidRequestException
     */
    private function validateCheckTheme(array $data)
    {
        $rules = [
            'slug'        => 'required',
            'deploy_type' => 'required',
            'project_key' => 'required',
        ];
        $v = \Validator::make($data, $rules);
        if ($v->fails()) {
            throw new InvalidRequestException($v);
        }
    }

    /**
     *
     * @throws \ValidationException
     * @throws \Keios\PluginMother\Exceptions\InvalidRequestException
     */
    private function validateUpdateRequest()
    {
        $rules = [
            'project_key' => 'required',
            'message'     => 'required',
            'status'      => 'required',
        ];
        $v = \Validator::make($this->postData, $rules);
        if ($v->fails()) {
            throw new InvalidRequestException($v);
        }
    }

    /**
     * @throws \ValidationException
     * @internal param $data
     *
     * @throws \Keios\PluginMother\Exceptions\InvalidRequestException
     */
    private function validateAuthorizeRequest()
    {
        $rules = [
            'project_key' => 'required',
            'deploy_type' => 'required',
        ];
        $v = \Validator::make($this->postData, $rules);
        if ($v->fails()) {
            throw new InvalidRequestException($v);
        }
    }

    /**
     * @throws \ValidationException
     * @internal param $data
     *
     * @throws \Keios\PluginMother\Exceptions\InvalidRequestException
     */
    private function validateGetZipRequest()
    {
        $rules = [
            'project_key' => 'required',
            'slug'        => 'required',
        ];
        $v = \Validator::make($this->postData, $rules);
        if ($v->fails()) {
            throw new InvalidRequestException($v);
        }
    }

    /**
     * I know it's ugly but it's repeater that requires all this stuff
     *
     * @param $project
     * @return bool
     */
    private function matchDomain($project)
    {
        if (!array_key_exists('domain', $this->postData)) {
            return false;
        }
        $sentDomain = $this->postData['domain'];
        $matchMain = $sentDomain === $project->domain;
        if ($matchMain) {
            return true;
        }
        $allowed = $project->allowed_domains;
        if (!is_array($allowed)) {
            $allowed = json_decode($allowed, true);
        }
        foreach ($allowed as $domainArray) {
            $domain = null;
            if(array_key_exists('domain', $domainArray)){
                $domain = $domainArray['domain'];
            }
            if ($domain === $sentDomain) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param array $postData
     */
    public function setPostData($postData)
    {
        $this->postData = $postData;
    }
}
