<?php namespace Keios\PluginMother\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Lang;
use Keios\PluginMother\Models\PluginGroup;

/**
 * Plugin Groups Back-end Controller
 */
class PluginGroups extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
    ];
    public $relationConfig = 'config_relations.yaml';
    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Keios.PluginMother', 'pluginmother', 'plugingroups');
    }

    /**
     * Deleted checked plugingroups.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $plugingroupId) {
                if (!$plugingroup = PluginGroup::find($plugingroupId)) {
                    continue;
                }
                $plugingroup->delete();
            }

            Flash::success(Lang::get('keios.pluginmother::lang.plugingroups.delete_selected_success'));
        } else {
            Flash::error(Lang::get('keios.pluginmother::lang.plugingroups.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}
