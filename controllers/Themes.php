<?php namespace Keios\PluginMother\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Carbon\Carbon;
use Flash;
use Lang;
use Keios\PluginMother\Models\Theme;

/**
 * Themes Back-end Controller
 */
class Themes extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Keios.PluginMother', 'pluginmother', 'themes');
    }
    public function getFileData(Theme $theme)
    {
        $result = [];
        $namespace = strtolower($theme->namespace);
        $directory = strtolower($theme->directory);
        $branches = [];
        $path = storage_path("mother_themes/$namespace/$directory");
        $content = [];
        try {
            $content = scandir($path);
        } catch (\Exception $e){
            $result['Error'] = 'File not found';
        }
        if (count($content) >= 0) {
            unset($content[0], $content[1]);
            foreach ($content as $branch) {
                $branches[] = $branch;
            }
        }

        foreach ($branches as $branch) {
            $branchArray = [];
            $branchVersionPath = $path.'/'.$branch.'/theme.yaml';
            $slug = $theme->slug;
            $zip = storage_path("mother_themes/$branch/$slug.zip");
            try {
                $yaml = \Symfony\Component\Yaml\Yaml::parse(file_get_contents($branchVersionPath));
                $code = 'Unknown';
                if(array_key_exists('code', $yaml)){
                    $code = $yaml['code'];
                }
                $branchArray['Code'] = $code;
            } catch (\Exception $e){
                $branchArray['Code'] = 'Unknown (Invalid Yaml)';
            }

            try {
                $size = (float)(filesize($zip) / 1024);
                $lastUpdate = Carbon::createFromTimestamp(filectime($zip));
                $branchArray['Last update'] = $lastUpdate->toDateTimeString();
                $branchArray['Zip Size'] = $size.' kB';
            } catch(\Exception $e){
                $branchArray['Last update'] = 'Unknown ('.$e->getMessage().')';
                $branchArray['Zip Size'] = 'Unknown ('.$e->getMessage().')';
            }

            $branchArray['Zip Path'] = $zip;
            $branchArray['Internal Repo Path'] = $path.'/'.$branch .'/';
            $result[$branch] = $branchArray;
        }

        return $result;
    }

    /**
     * @param Theme $theme
     *
     * @return string
     * @throws \Illuminate\Contracts\Encryption\EncryptException
     */
    public function getHookEndpoint(Theme $theme)
    {
        $rootUrl = \Request::root();

        return $rootUrl.'/api/v1/theme-hook/'.\Crypt::encrypt($theme->slug);
    }

    /**
     * @return mixed
     */
    public function getThemes()
    {
        return Theme::all();
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     * @throws \ApplicationException
     * @throws \Exception
     */
    public function onCopyTheme()
    {
        \DB::beginTransaction();
        try {
            $data = post();
            $this->validateCopyTheme($data);
            /** @var Plugin $source */
            $source = Plugin::where('id', $data['source_id'])->first();
            if (!$source) {
                throw new \ApplicationException(\Lang::trans('keios.pluginmother::lang.errors.source_theme_does_not_exist'));
            }
            //TODO: copy licenses when introduced
            //$related = \DB::table('keios_pluginmother_plug_lic')->where('project_id', $data['source_id'])->get();
            $targetTheme = new Plugin();
            $targetTheme->name = $data['plugin_name'];
            $targetTheme->excerpt = $source->excerpt;
            $targetTheme->description = $source->description;
            $targetTheme->git_ssh_path = $source->git_ssh_path;
            $targetTheme->git_branch = $source->git_branch;
            $targetTheme->git_branches = $source->git_branches;
            $targetTheme->namespace = $source->namespace;
            $targetTheme->directory = $source->directory;
            $targetTheme->version = $source->version;
            $targetTheme->save();
            /*
            $toInsert = [];
            foreach ($related as $ids) {
                $toInsert[] = ['project_id' => $targetTheme->id, 'plugin_id' => $ids->plugin_id];
            }
            \DB::table('keios_pluginmother_proj_plug')->insert($toInsert);
            */

            \DB::commit();

            return \Redirect::to('/backend/keios/pluginmother/themes/update/'.$targetTheme->id);
        } catch (\Exception $e) {
            \DB::rollBack();
            throw $e;
        }
    }

    /**
     * @param $data
     *
     * @throws \ValidationException
     */
    private function validateCopyTheme($data)
    {
        $rules = [
            'source_id'   => 'required',
            'theme_name' => 'required',
        ];
        $v = \Validator::make($data, $rules);
        if ($v->fails()) {
            throw new \ValidationException($v);
        }
    }

    /**
     * Deleted checked themes.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $themeId) {
                if (!$theme = Theme::find($themeId)) continue;
                $theme->delete();
            }

            Flash::success(Lang::get('keios.pluginmother::lang.themes.delete_selected_success'));
        }
        else {
            Flash::error(Lang::get('keios.pluginmother::lang.themes.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}
