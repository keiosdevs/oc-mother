<?php namespace Keios\PluginMother\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * License Types Back-end Controller
 */
class LicenseTypes extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Keios.PluginMother', 'pluginmother', 'licensetypes');
    }
}
