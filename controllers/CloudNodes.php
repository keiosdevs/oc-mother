<?php namespace Keios\PluginMother\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Lang;
use Keios\PluginMother\Models\CloudNode;

/**
 * Cloud Nodes Back-end Controller
 */
class CloudNodes extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Keios.PluginMother', 'pluginmother', 'cloudnodes');
    }

    /**
     * Deleted checked cloudnodes.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $cloudnodeId) {
                if (!$cloudnode = CloudNode::find($cloudnodeId)) continue;
                $cloudnode->delete();
            }

            Flash::success(Lang::get('keios.pluginmother::lang.cloudnodes.delete_selected_success'));
        }
        else {
            Flash::error(Lang::get('keios.pluginmother::lang.cloudnodes.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}
