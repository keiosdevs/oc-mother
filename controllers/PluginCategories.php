<?php namespace Keios\PluginMother\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Keios\MoneyRight\Money;
use Keios\PluginMother\Classes\CostCalculator;
use Lang;
use Keios\PluginMother\Models\PluginCategory;
use RainLab\User\Models\User;

/**
 * Plugin Categories Back-end Controller
 */
class PluginCategories extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
    ];
    public $relationConfig = 'config_relations.yaml';
    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Keios.PluginMother', 'pluginmother', 'plugincategories');
    }

    public function getPluginsWorth(){
        $user = \Auth::getUser();
        if(!$user){
            $user = User::where('id', 1)->first();
        }
        $costCalculator = new CostCalculator();
        $category = $this->widget->form->model;
        $plugins = $category->plugins;
        $minCost = new Money(0, $costCalculator->getCurrency());
        $maxCost = new Money(0, $costCalculator->getCurrency());
        foreach($plugins as $plugin) {
            $pluginCosts = $costCalculator->calculatePluginCosts($plugin, $user);
            $costsIndexes = [];
            foreach($pluginCosts as $pluginCost){
                if(array_key_exists('cost', $pluginCost)){
                    $costsIndexes[$pluginCost['cost']->getAmount()] = $pluginCost['cost'];
                }
            }
            $maxKey = max(array_keys($costsIndexes));
            $minKey = min(array_keys($costsIndexes));
            $minCost = $minCost->add($costsIndexes[$minKey]);
            $maxCost = $maxCost->add($costsIndexes[$maxKey]);
        }
        return [
            'min' => $minCost,
            'max' => $maxCost
        ];
    }
    /**
     * Deleted checked plugincategories.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $plugincategoryId) {
                if (!$plugincategory = PluginCategory::find($plugincategoryId)) continue;
                $plugincategory->delete();
            }

            Flash::success(Lang::get('keios.pluginmother::lang.plugincategories.delete_selected_success'));
        }
        else {
            Flash::error(Lang::get('keios.pluginmother::lang.plugincategories.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}
