<?php namespace Keios\PluginMother\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Keios\PluginMother\Models\License;
use Lang;
use Keios\PluginMother\Models\LicenseOrder;

/**
 * License Orders Back-end Controller
 */
class LicenseOrders extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Keios.PluginMother', 'pluginmother', 'licenseorders');
    }

    /**
     * Deleted checked licenseorders.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $licenseorderId) {
                if (!$licenseorder = LicenseOrder::find($licenseorderId)) {
                    continue;
                }
                $licenseorder->delete();
            }

            Flash::success(Lang::get('keios.pluginmother::lang.licenseorders.delete_selected_success'));
        } else {
            Flash::error(Lang::get('keios.pluginmother::lang.licenseorders.delete_selected_empty'));
        }

        return $this->listRefresh();
    }

    public function getLicenses()
    {
        return License::all();

    }

    /**
     * Deleted checked licenseorders.
     */
    public function index_onReassign()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $licenseorderId) {
                if (!$licenseorder = LicenseOrder::find($licenseorderId)) {
                    continue;
                }
                $licenseorder->delete();
            }

            Flash::success(Lang::get('keios.pluginmother::lang.licenseorders.delete_selected_success'));
        } else {
            Flash::error(Lang::get('keios.pluginmother::lang.licenseorders.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}
