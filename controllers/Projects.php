<?php namespace Keios\PluginMother\Controllers;

use Backend\Facades\Backend;
use BackendMenu;
use Backend\Classes\Controller;
use Keios\PluginMother\Classes\AutoUpdater;
use Keios\PluginMother\Classes\CloudManager;
use Keios\PluginMother\Classes\Connectors\VultrConnector;
use Keios\PluginMother\Exceptions\CloudConnectionException;
use Keios\PluginMother\Models\CloudNode;
use Keios\PluginMother\Models\PluginCategory;
use Keios\PluginMother\Models\PluginGroup;
use Keios\PluginMother\Models\Project;
use Keios\PluginMother\Models\Settings;
use Keios\PluginMother\Repositories\PluginRepository;
use Keios\PluginMother\Repositories\ProjectRepository;
use Keios\PluginMother\Classes\RequestSender;
use Flash;
use Lang;

/**
 * Projects Back-end Controller
 */
class Projects extends Controller
{
    /**
     * @var array
     */
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
    ];

    /**
     * @var string
     */
    public $formConfig = 'config_form.yaml';
    /**
     * @var string
     */
    public $listConfig = 'config_list.yaml';

    /**
     * @var string
     */
    public $relationConfig = 'config_relations.yaml';

    /**
     * @var ProjectRepository
     */
    private $projectRepo;

    /**
     * @var RequestSender
     */
    private $requestSender;

    /**
     * Projects constructor.
     */
    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Keios.PluginMother', 'pluginmother', 'projects');
        $this->projectRepo = new ProjectRepository();
        $this->requestSender = new RequestSender();
        $this->vars['groups'] = PluginGroup::all();
        $this->vars['categories'] = PluginCategory::all();
    }

    public function getProjectWorth(){
      // TODO
    }

    /**
     * @return mixed
     */
    public function getProjects()
    {
        return Project::all();
    }

    /**
     * @param $query
     */
    public function listExtendQuery($query)
    {
        $query->where('parent_id', 0)->orWhere('parent_id', null);
    }

    /**
     * @throws CloudConnectionException
     * @throws \ValidationException
     * @throws \Exception
     * @throws \ApplicationException
     * @throws \Vultr\Exception\ApiException
     */
    public function onCreateVultrForProject()
    {
        if (array_key_exists(0, $this->params)) {
            try {
                $projectId = $this->params[0];
            } catch (\Exception $e) {
                throw new \ValidationException(
                    ['message' => \Lang::trans('keios.pluginmother::lang.errors.invalid_project')]
                );
            }
            $project = $this->projectRepo->getById($projectId);
            $this->createVultrForProject($project->slug);

            return \Redirect::to(\Backend::url().'/keios/pluginmother/projects/update/'.$projectId);
        }
        \Log::error('IT DID HAPPEN!!!!! onCreateVultrForProject in controllers/Projects.php - there was no key 0 in $this->params. Go figure.');
        throw new \ApplicationException('Internal error. Please try again later');
    }

    /**
     * @param string $projectSlug
     *
     * @return CloudNode
     * @throws \Keios\PluginMother\Exceptions\CloudConnectionException
     * @throws \Vultr\Exception\ApiException
     * @throws CloudConnectionException
     * @throws \Exception
     */
    public function createVultrForProject($projectSlug)
    {
        /** @var Settings $settings */
        $settings = Settings::instance();
        $vultrConnector = new VultrConnector();
        $client = $vultrConnector->authorize($settings->get('vultr_api_key'));

        $config = $settings->createVultrConfig();
        $config->label = $projectSlug;
        $config->hostname = strtolower(str_replace(' ', '-', $projectSlug));

        $vultrId = $vultrConnector->createNode($config);
        if (!$vultrId) {
            throw new CloudConnectionException('Problem when connecting to Vultr');
        }
        $project = $this->projectRepo->getBySlug($projectSlug);
        $serverList = $client->server()->getList();

        $localModel = new CloudNode();
        $localModel->setProjectId($project->id);
        $localModel->setRemoteId($vultrId);
        $localModel->setType('vultr');
        $localModel->setDetails($serverList[$vultrId]);
        $localModel->save();

        return $localModel;
    }

    /**
     * @return Project|null
     * @throws \ValidationException
     */
    public function getProject()
    {
        if (array_key_exists(0, $this->params)) {
            try {
                $projectId = $this->params[0];

                return $this->projectRepo->getById($projectId);
            } catch (\Exception $e) {
                throw new \ValidationException(
                    ['message' => \Lang::trans('keios.pluginmother::lang.errors.invalid_project')]
                );
            }
        }

        return null;
    }

    /**
     * Deleted checked testtts.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            /** @var array $checkedIds */
            foreach ($checkedIds as $projectId) {
                if (!$project = Project::find($projectId)) {
                    continue;
                }
                $project->delete();
            }

            Flash::success(Lang::get('keios.pluginmother::lang.project.delete_selected_success'));
        } else {
            Flash::error(Lang::get('keios.pluginmother::lang.project.delete_selected_empty'));
        }

        return $this->listRefresh();
    }

    /**
     * Deleted checked testtts.
     * @throws \ApplicationException
     * @throws \ValidationException
     */
    public function onAttachPlugins()
    {

        $project = $this->getProject();

        $data = post();
        $pluginRepo = new PluginRepository();
        if (!array_key_exists('groups', $data)) {
            throw new \ApplicationException('Select a group first!');
        }
        $groups = $data['groups'];
        foreach ($groups as $group => $on) {
            $category = $pluginRepo->getPluginCategory($group);
            if ($category && $category->plugins) {
                foreach ($category->plugins as $plugin) {
                    $project->plugins()->attach($plugin);
                }
            }
        }

        $project->save();

        Flash::success(Lang::get('keios.pluginmother::lang.project.attached_ok'));

        return \Redirect::to(\Backend::url().'/keios/pluginmother/projects/update/'.$project->id);

    }

    /**
     * Deleted checked testtts.
     * @throws \ApplicationException
     * @throws \ValidationException
     */
    public function onUnattachPlugins()
    {
        $project = $this->getProject();
        $data = post();
        $pluginRepo = new PluginRepository();
        if (!array_key_exists('groups', $data)) {
            throw new \ApplicationException('Select a group first!');
        }
        $groups = $data['groups'];
        foreach ($groups as $group => $on) {
            $category = $pluginRepo->getPluginCategory($group);
            if ($category && $category->plugins) {
                foreach ($category->plugins as $plugin) {
                    $project->plugins()->detach($plugin);
                }
            }
        }

        $project->save();

        Flash::success(Lang::get('keios.pluginmother::lang.project.attached_ok'));

        return \Redirect::to(\Backend::url().'/keios/pluginmother/projects/update/'.$project->id);

    }

    /**
     * @return array
     * @throws \ValidationException
     */
    public function getProjectPlugins()
    {
        $project = $this->getProject();
        $disabledPlugins = $project->disabled_plugins;
        $plugins = $project->plugins;
        $result = [];
        foreach ($plugins as $plugin) {
            $alias = $plugin->namespace.'.'.$plugin->directory;
            if (!$disabledPlugins) {
                $disabledPlugins = [];
            }
            if (!in_array($alias, $disabledPlugins, true)) {
                $result[] = $alias;
            }
        }

        return $result;

    }

    /**
     * @return array
     * @throws \ValidationException
     */
    public function pingProject()
    {
        $settings = Settings::instance();
        $timeout = $settings->get('curl_timeout', 10);
        $project = $this->getProject();
        $domain = $project->domain;
        if (!$domain) {
            return [];
        }
        $url = $domain.'/api/v1/ping';

        try {
            $response = $this->requestSender->sendPostRequest([], $url, $timeout);
        } catch (\Exception $e) {
            return [$project->domain => 0];
        }
        if (array_key_exists(200, $response)) {
            return [$project->domain => 1];
        }
    }

    /**
     * @return array
     * @throws \ValidationException
     */
    public function pingSubprojects()
    {
        $settings = Settings::instance();
        $timeout = $settings->get('curl_timeout', 10);
        $project = $this->getProject();
        $subprojects = $project->subprojects;
        $domains = [];
        foreach ($subprojects as $subproject) {
            $domains[] = $subproject->domain;
        }
        if($project->domain) {
            $domains[] = $project->domain;
        }
        $responses = [];
        foreach ($domains as $key => $domain) {
            try {
                $url = $domain.'/api/v1/ping';
                $response = $this->requestSender->sendPostRequest([], $url, $timeout);
            } catch (\Exception $e) {
                $responses[$domain] = 'Offline';
                continue;
            }
            if (array_key_exists(200, $response)) {
                $responses[$domain] = 'Online';
            } else {
                if(array_key_exists(0, array_keys($response))) {
                    $responses[$domain] = array_keys($response)[0];
                } else {
                    $responses[$domain] = 'Error';
                }
            }
        }

        return $responses;
    }

    /**
     * @throws \ValidationException
     * @throws \ApplicationException
     */
    public function onDisablePlugin()
    {
        $data = post();
        $pluginCode = $data['plugin'];

        $project = $this->getProject();
        $url = $project->domain.'/api/v1/plugin/disable';
        $response = $this->requestSender->sendPostRequest(
            [
                'project_key' => $project->code,
                'plugin'      => $pluginCode,
            ],
            $url
        );
        if (array_key_exists(200, $response)) {
            $disabledPlugins = $project->disabled_plugins;
            if (!$disabledPlugins) {
                $disabledPlugins = [];
            }
            $disabledPlugins[] = $pluginCode;
            $project->disabled_plugins = $disabledPlugins;
            $project->save();
        } else {
            \Log::error('Error while disabling plugin '.$pluginCode.': '.print_r($response, true));
            throw new \ApplicationException(
                \Lang::trans('keios.pluginmother::lang.errors.operation_on_spawn_failed')
            );
        }

    }

    /**
     * @throws \ValidationException
     * @throws \ApplicationException
     */
    public function onEnablePlugin()
    {
        $data = post();
        $pluginCode = $data['plugin'];
        $project = $this->getProject();
        $url = $project->domain.'/api/v1/plugin/enable';
        $response = $this->requestSender->sendPostRequest(
            [
                'project_key' => $project->code,
                'plugin'      => $pluginCode,
            ],
            $url
        );
        if (array_key_exists(200, $response)) {
            $disabledPlugins = $project->disabled_plugins;
            if (!$disabledPlugins) {
                $disabledPlugins = [];
            }
            if (in_array($pluginCode, $disabledPlugins, true)) {
                $disabledPlugins = array_diff($disabledPlugins, [$pluginCode]);
                $project->disabled_plugins = $disabledPlugins;
                $project->save();
            }
        } else {
            \Log::error('Error while enabling plugin '.$pluginCode.': '.print_r($response, true));
            throw new \ApplicationException(
                \Lang::trans('keios.pluginmother::lang.errors.operation_on_spawn_failed')
            );
        }
    }

    /**
     *
     * @throws \ValidationException
     * @throws \ApplicationException
     */
    public function onUpdateRemoteProject()
    {
        $project = $this->getProject();
        if (!$project) {
            throw new \ValidationException(
                ['message' => \Lang::trans('keios.pluginmother::lang.errors.invalid_project')]
            );
        }
        $updater = new AutoUpdater();
        $response = $updater->update($project);

        if (array_key_exists(200, $response)) {
            \Flash::success(\Lang::trans('keios.pluginmother::lang.messages.update_request_sent'));

            return \Redirect::to(Backend::url().'/keios/pluginmother/projects/update/'.$project->id);
        }

        $response = array_flatten($response);
        if (count($response) === 2) {
            $exceptionMessage = $response[1];
            $msgArray = json_decode($exceptionMessage, true);
            throw new \ApplicationException($msgArray['message']);
        }
        throw new \ApplicationException(\Lang::trans('keios.pluginmother::lang.errors.unknown_update_error'));

    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     * @throws \ApplicationException
     * @throws \Exception
     */
    public function onCopyProject()
    {
        \DB::beginTransaction();
        try {
            $data = post();
            $this->validateCopyProject($data);
            /** @var Project $source */
            $source = Project::where('id', $data['source_id'])->first();
            if (!$source) {
                throw new \ApplicationException(
                    \Lang::trans('keios.pluginmother::lang.errors.source_project_doesnot_exist')
                );
            }
            $related = \DB::table('keios_pluginmother_proj_plug')->where('project_id', $data['source_id'])->get();
            $targetProject = new Project();
            $targetProject->title = $data['project_name'];
            $targetProject->user_id = $source->user_id;
            $targetProject->domain = $source->domain;
            $targetProject->enable_notifications = $source->enable_notifications;
            $targetProject->enable_autoupdate = $source->enable_autoupdate;
            $targetProject->parent_id = $source->id;
            $toInsert = [];
            $targetProject->save();
            foreach ($related as $ids) {
                $toInsert[] = ['project_id' => $targetProject->id, 'plugin_id' => $ids->plugin_id];
            }

            \DB::table('keios_pluginmother_proj_plug')->insert($toInsert);
            \DB::commit();

            return \Redirect::to(Backend::url().'/keios/pluginmother/projects/update/'.$targetProject->id);
        } catch (\Exception $e) {
            \DB::rollBack();
            throw $e;
        }
    }

    public function onInstallOctober()
    {
        $project = $this->getProject();
        $cloudNodes = $project->cloudnodes;
        $manager = new CloudManager();
        $settings = Settings::instance();
        $sshKey = $settings->get('ssh_key_path');
        foreach ($cloudNodes as $cloudNode) {
            $nodeDetails = $cloudNode->details;
            $ip = $this->findIpForNode($nodeDetails, $cloudNode->type);
            $port = 22;
            $manager->installSpawnOnVultrNode($project, $ip, $port, $sshKey);
        }
    }

    public function findIpForNode($nodeDetails, $type)
    {
        $ip = null;
        if ($type === 'vultr') {
            if (array_key_exists('main_ip', $nodeDetails)) {
                $ip = $nodeDetails['main_ip'];
            }
        }
        if (!$ip) {
            throw new \ApplicationException('No IP in Node details. Refresh the page and try again.');
        }

        return $ip;
    }

    /**
     * @param $data
     *
     * @throws \ValidationException
     */
    private function validateCopyProject($data)
    {
        $rules = [
            'source_id'    => 'required',
            'project_name' => 'required',
        ];
        $v = \Validator::make($data, $rules);
        if ($v->fails()) {
            throw new \ValidationException($v);
        }
    }
}
