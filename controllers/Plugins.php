<?php namespace Keios\PluginMother\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Carbon\Carbon;
use Keios\MoneyRight\Money;
use Keios\PluginMother\Classes\CostCalculator;
use Keios\PluginMother\Facades\MoneyFormatter;
use Keios\PluginMother\Models\Plugin;
use Flash;
use Lang;
use RainLab\User\Models\User;

/**
 * Plugins Back-end Controller
 */
class Plugins extends Controller
{
    /**
     * @var array
     */
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
        'Backend.Behaviors.ImportExportController',
    ];

    /**
     * @var string
     */
    public $formConfig = 'config_form.yaml';
    /**
     * @var string
     */
    public $listConfig = 'config_list.yaml';
    /**
     * @var string
     */
    public $relationConfig = 'config_relations.yaml';
    /**
     * @var string
     */
    public $importExportConfig = 'config_import_export.yaml';

    /**
     * Plugins constructor.
     */
    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Keios.PluginMother', 'pluginmother', 'plugins');
    }

    public function getPluginValue($plugin)
    {
        $user = \Auth::getUser();
        if (!$user) {
            $user = User::where('id', 1)->first();
        }
        $result = [];
        $costCalculator = new CostCalculator();
        $pluginCosts = $costCalculator->calculatePluginCosts($plugin, $user);
        /** @var MoneyFormatter $formatter */


        foreach ($pluginCosts as $pluginCost) {
            if (array_key_exists('cost', $pluginCost)) {
                /** @var Money $cost */
                $cost = $pluginCost['cost'];
                if($pluginCost['license'] === null){
                    $result[] = '-- no license --';
                } else {
                    $result[] = MoneyFormatter::format($cost);
                }
            }
        }

        return implode(', ', $result);
    }

    /**
     * @param Plugin $plugin
     *
     * @return string
     * @throws \Illuminate\Contracts\Encryption\EncryptException
     */
    public function getHookEndpoint(Plugin $plugin)
    {
        $rootUrl = \Request::root();

        return $rootUrl.'/api/v1/hook/'.\Crypt::encrypt($plugin->slug);
    }

    /**
     * @return mixed
     */
    public function getPlugins()
    {
        return Plugin::all();
    }

    /**
     * Deleted checked plugins.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            /** @var array $checkedIds */
            foreach ($checkedIds as $pluginId) {
                if (!$plugin = Plugin::find($pluginId)) {
                    continue;
                }
                $plugin->delete();
            }

            Flash::success(Lang::get('keios.pluginmother::lang.plugin.delete_selected_success'));
        } else {
            Flash::error(Lang::get('keios.pluginmother::lang.plugin.delete_selected_empty'));
        }

        return $this->listRefresh();
    }

    public function getFileData(Plugin $plugin)
    {
        $result = [];
        $namespace = strtolower($plugin->namespace);
        $directory = strtolower($plugin->directory);
        $branches = [];
        $path = storage_path("mother_plugins/$namespace/$directory");
        $content = [];
        try {
            $content = scandir($path);
        } catch (\Exception $e) {
            $result['Error'] = 'File not found';
        }

        if (count($content) >= 0) {
            unset($content[0], $content[1]);
            foreach ($content as $branch) {
                $branches[] = $branch;
            }
        }

        foreach ($branches as $branch) {
            $branchArray = [];
            $branchVersionPath = $path.'/'.$branch.'/updates/version.yaml';
            $slug = $plugin->slug;
            $zip = storage_path("mother_plugins/$branch/$slug.zip");
            try {
                $yaml = \Symfony\Component\Yaml\Yaml::parse(file_get_contents($branchVersionPath));
                end($yaml);
                $version = key($yaml);
                $branchArray['Version'] = $version;
            } catch (\Exception $e) {
                $branchArray['Version'] = 'Unknown (Invalid Yaml)';
            }

            try {
                $size = (float)(filesize($zip) / 1024);
                $lastUpdate = Carbon::createFromTimestamp(filectime($zip));
                $branchArray['Last update'] = $lastUpdate->toDateTimeString();
                $branchArray['Zip Size'] = $size.' kB';
            } catch (\Exception $e) {
                $branchArray['Last update'] = 'Unknown ('.$e->getMessage().')';
                $branchArray['Zip Size'] = 'Unknown ('.$e->getMessage().')';
            }

            $branchArray['Zip Path'] = $zip;
            $branchArray['Internal Repo Path'] = $path.'/'.$branch.'/';
            $result[$branch] = $branchArray;
        }

        return $result;
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     * @throws \ApplicationException
     * @throws \Exception
     */
    public function onCopyPlugin()
    {
        \DB::beginTransaction();
        try {
            $data = post();
            $this->validateCopyPlugin($data);
            /** @var Plugin $source */
            $source = Plugin::where('id', $data['source_id'])->first();
            if (!$source) {
                throw new \ApplicationException(
                    \Lang::trans('keios.pluginmother::lang.errors.source_project_does_not_exist')
                );
            }
            //TODO: copy licenses when introduced
            //$related = \DB::table('keios_pluginmother_plug_lic')->where('project_id', $data['source_id'])->get();
            $targetPlugin = new Plugin();
            $targetPlugin->name = $data['plugin_name'];
            $targetPlugin->excerpt = $source->excerpt;
            $targetPlugin->description = $source->description;
            $targetPlugin->git_ssh_path = $source->git_ssh_path;
            $targetPlugin->git_branch = $source->git_branch;
            $targetPlugin->git_branches = $source->git_branches;
            $targetPlugin->namespace = $source->namespace;
            $targetPlugin->directory = $source->directory;
            $targetPlugin->version = $source->version;
            $targetPlugin->save();
            /*
            $toInsert = [];
            foreach ($related as $ids) {
                $toInsert[] = ['project_id' => $targetPlugin->id, 'plugin_id' => $ids->plugin_id];
            }
            \DB::table('keios_pluginmother_proj_plug')->insert($toInsert);
            */

            \DB::commit();

            return \Redirect::to('/backend/keios/pluginmother/plugins/update/'.$targetPlugin->id);
        } catch (\Exception $e) {
            \DB::rollBack();
            throw $e;
        }
    }

    /**
     * @param $data
     *
     * @throws \ValidationException
     */
    private function validateCopyPlugin($data)
    {
        $rules = [
            'source_id'   => 'required',
            'plugin_name' => 'required',
        ];
        $v = \Validator::make($data, $rules);
        if ($v->fails()) {
            throw new \ValidationException($v);
        }
    }
}
