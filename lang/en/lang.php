<?php

return [
    'errors' => [
        'must_be_a_directory' => 'must be a directory',
        'cannot_create_directory' => 'Cannot create directory',
        'invalid_project' => 'Invalid Project',
        'operation_on_spawn_failed' => 'Operation on spawn has failed.',
        'unknown_update_error' => 'Update failed with unknown error.',
        'source_project_doesnot_exist' => 'Source project does not exist!',
        'source_project_does_not_exist' => 'Source project does not exist!',
        'invalid_project_or_plugin' => 'Invalid project or plugin',
        'plugin_already_attached' => 'Plugin already attached!',
        'project_not_found' => 'Project not found',
        'retarded_doubled_override' => 'You cannot add two overrides for the same plugin!!!',
        'source_theme_does_not_exist' => 'Source theme does not exist',
    ],
    'project' => [
        'delete_selected_success' => 'Deleted successfully',
        'delete_selected_empty' => 'Select project to delete',
        'attached_ok' => 'Plugins attached properly',
        'unattached_ok' => 'Plugins unattached properly',
    ],
    'projects' => [
        'menu_label' => 'Projects',
        'delete_confirm' => 'Are you sure you want to delete?',
        'return_to_list' => 'Return to projects list',
        'delete_selected_confirm' => 'Are you sure you want to delete selected projects?',
    ],
    'plugin' => [
        'delete_selected_success' => 'Deleted successfully',
        'delete_selected_empty' => 'Select plugin to delete',
    ],
    'messages' => [
        'no_autoupdatable_projects' => 'No projects marked for autoupdate',
        'updating' => 'Updating',
        'updated' => 'Updated',
        'not_updated_check_if' => 'Not updated. Check if',
        'responds_and_if_spawn_plugin_is_configured' => 'responds and if spawn plugin is properly configured',
        'finished' => 'Finished',
        'update_request_sent' => 'Update request sent!',
        'project_offline' => 'Project Offline',
        'project_online' => 'Project Online',
        'has_been_synced_with_upstream' => 'has been synced with upstream',
    ],
    'strings' => [
        'new_deploy_type' => 'New Deploy Type',
        'deploy_type' => 'Deploy Type',
        'create_deploy_type' => 'Create deploy type',
        'update_deploy_type' => 'Update deploy type',
        'preview_deploy_type' => 'Preview deploy type',
        'manage_deploy_types' => 'Manage deploy types',
        'deploy_types' => 'Deploy Types',
        'return_to_deploytypes_list' => 'Return to deploy types list',
        'creating_deploytype' => 'Creating deploy type...',
        'create' => 'Create',
        'create_and_close' => 'Create and close',
        'or' => 'or',
        'cancel' => 'Cancel',
        'saving_deploytype' => 'Saving deploy type...',
        'save' => 'Save',
        'save_and_close' => 'Save and close',
        'deleting_deploytype' => 'Deleting deploy type...',
        'confirm_delete_deploytype' => 'Are you sure you want to delete this deploy type?',
        'import_plugins' => 'Import plugins',
        'export_plugins' => 'Export plugins',
        'new_plugin' => 'New Plugin',
        'export' => 'Export',
        'import' => 'Import',
        'copy_plugin' => 'Copy plugin',
        'source_plugin' => 'Source plugin',
        'new_plugin_name' => 'New plugin name',
        'copy' => 'Copy',
        'import_records' => 'Import records',
        'license' => 'License',
        'project' => 'Project',
        'plugin' => 'Plugin',
        'create_plugin' => 'Create plugin',
        'edit_plugin' => 'Update plugin',
        'preview_plugin' => 'Preview plugin',
        'export_records' => 'Export records',
        'manage_plugins' => 'Manage plugins',
        'plugins' => 'Plugins',
        'return_to_plugins_list' => 'Return to plugins list',
        'creating_plugin' => 'Creating plugin...',
        'saving_plugin' => 'Saving plugin...',
        'deleting_plugin' => 'Deleting plugin...',
        'confirm_plugin_delete' => 'Are you sure you want to delete this plugin?',
        'none' => 'none',
        'new_project' => 'New project',
        'copy_project' => 'Copy project',
        'source_project' => 'Source project',
        'new_project_name' => 'New project name',
        'update_remote_instance' => 'Update remote instance',
        'disable_plugin' => 'Disable plugin',
        'subprojects' => 'Subprojects',
        'create_project' => 'Create project',
        'update_project' => 'Update project',
        'preview_project' => 'Preview project',
        'manage_projects' => 'Manage projects',
        'projects' => 'Projects',
        'return_to_projects_list' => 'Return to projects list',
        'creating_project' => 'Creating project...',
        'attach_plugin_group' => 'Attach plugins groups',
        'attach_plugins' => 'Attach plugins groups',
        'attach_plugins_desc' => 'Plugins from selected plugin groups will be attached to this project',
        'unattach_plugin_group' => 'Unattach plugins groups',
        'new_theme' => 'New Theme',
        'copy_theme' => 'Copy Theme',
        'new_theme_name' => 'New theme name',
        'themes' => 'Themes',
        'recommended' => 'Recommended',
        'pulling_plugin' => 'Pulling plugin...',
        'save_pull' => 'Save & Pull',
        'save_pull_close' => 'Save, Pull & Close',
        'create_pull' => 'Create & Clone',
        'create_pull_close' => 'Create, Clone & Close',
        'value' => 'Licenses worth',
    ],
    'models' => [
        'allowed_domains' => 'Allowed domains',
        'allowed_domains_desc' => 'List of domains, which, aside main domain, can access mother if Lock console is enabled. ',
        'allowed_domain' => 'Allowed Domain',
        'file_date' => 'File data',
        'name' => 'Name',
        'description' => 'Description',
        'price' => 'Price',
        'type' => 'Type',
        'valid_to' => 'Valid to',
        'id' => 'ID',
        'license_type' => 'License type',
        'title' => 'Title',
        'content' => 'Content',
        'is_forever' => 'Valid forever',
        'plugin_name' => 'Plugin name',
        'plugin_slug' => 'Plugin slug',
        'hook_endpoint' => 'Hook endpoint',
        'excerpt' => 'Excerpt',
        'git_path' => 'Git SSH path',
        'git_default_branch' => 'Default git branch',
        'branches' => 'Git branches',
        'deploy_type' => 'Deploy type',
        'deploy_branch' => 'Deploy branch',
        'plugin_namespace' => 'Plugin namespace',
        'licenses' => 'Licenses',
        'screenshots' => 'Screenshots',
        'plugin_internal_alias' => 'Plugin Alias',
        'git_ssh_path' => 'Git SSH path',
        'git_branch' => 'Default git branch',
        'version' => 'Version',
        'user' => 'User',
        'parent' => 'Parent',
        'slug' => 'Slug',
        'domain' => 'Domain',
        'fully_qualified_url' => 'Fully qualified URL',
        'enable_notifications' => 'Enable notifications',
        'enable_autoupdate' => 'Enable autoupdate',
        'attached_plugins' => 'Attached plugins',
        'branch_overrides' => 'Branch overrides',
        'plugin' => 'Plugin',
        'branch' => 'Branch',
        'project_key' => 'Project key',
        'subprojects' => 'Subprojects',
        'last_update_date' => 'Last update date',
        'last_update_status' => 'Last update status',
        'deploy_code' => 'Code',
        'deploy_label' => 'Label',
        'is_available' => 'Is available',
        'is_default' => 'Is default',
        'plugin_info_tab' => 'Plugin Information',
        'git_branches_tab' => 'Git Branches Overrides',
        'licenses_tab' => 'Licenses',
        'cloudnodes' => 'Cloud Nodes',
        'categories' => 'Categories',
        'categories_tab' => 'Categories',
        'theme' => 'Theme',
        'theme_name' => 'Theme name',
        'theme_info_tab' => 'Theme information',
        'theme_slug' => 'Theme slug',
        'theme_namespace' => 'Theme namespace',
        'mother_internal_alias' => 'Mother internal alias',
        'mother_namespace' => 'Mother namespace',
        'mother_name' => 'Mother name',
        'mother_slug' => 'Mother slug',
        'is_visible' => 'Is visible',
        'is_featured' => 'Is featured',
        'groups' => 'Groups',
        'pre_market_plugins' => 'Pre-installed plugins',
        'pre_market_plugins_desc' => 'Plugins from official Market',
        'market_plugins' => 'Market Plugins',
        'cover' => 'Cover',
        'main_theme' => 'Main Theme',
        'additional_themes' => 'Additional Themes',
        'plugin_branch_overrides' => 'Branch overrides for Plugins',
        'theme_branch_overrides' => 'Branch overrides for Themes',
        'valid_for_days' => 'Valid for (days)',
        'category_worth' => 'Category Worth',
        'recommended' => 'Recommended',
        'recommended_plugins' => 'Recommended plugins',
        'plugin_overview' => 'Plugin overview',
        'technical_details' => 'Technical details',
        'is_library' => 'Is library',
        'is_depreciated' => 'Is depreciated',
        'on_sale' => 'On sale',
        'additional_data' => 'Additional data',
        'groups_and_categories' => 'Groups and Categories',
        'theme_overview' => 'Theme overview',
        'tech_overview' => 'Technical details',
        'additional_info' => 'Additional data',
        'license' => 'License',
        'project' => 'Project',
        'amount' => 'Amount',
        'currency' => 'Currency',
        'created_at' => 'Created at',
        'is_paid' => 'Is paid',
        'user_desc' => 'Main owner of the project',
        'parent_desc' => 'Source project. Can be unbound in parent settings.',
        'title_desc' => 'Project alias or domain',
        'slug_desc' => 'Urlized form of title',
        'main_theme_desc' => 'Main Theme. Will be activated as default.',
        'main_themes_desc' => 'Additional themes that should be downloaded.',
        'main_plugins_desc' => 'All plugins attached to this project',
        'main_plugin_branch_overrides_desc' => 'You can define an override for given deploy type. For example switch some plugin to develop temporarily.',
        'subprojects_desc' => 'All projects created from this project.',
        'main_theme_branch_overridesdesc' => 'You can define an override for given deploy type. For example switch theme to develop temporarily.',
        'cloudnodes_desc' => 'Droplets connected to this project',
        'name_desc' => 'Name under which plugin will be visible in the the spawns and on the frontend',
        'excerpt_desc' => 'Short description or link to repository',
        'description_desc' => 'Long description. You can use formatting.',
        'hook_endpoint_desc' => 'This webhook URL should be setup in repository webhooks.',
        'plugin_namespace_desc' => 'Plugin namespace as defined in Plugin.php',
        'plugin_name_desc' => 'Plugin name as defined in Plugin.php',
        'git_path_desc' => 'You can use http or SSH if webserver is authorized with your repo provider',
        'git_default_branch_desc' => 'Branch that will be used if no overrides or rules',
        'recommended_pluginsc_desc' => 'Recommended plugins for frontend purposes',
        'groups_and_categories_desc' => '',
        'branches_desc' => 'Branches configuration for various deploy types',
        'licenses_desc' => 'Licenses will be assigned to plugin and project when adding to plugin to project.',
        'is_public' => 'Is public',
        'is_forver' => 'Is forever',
        'is_external' => 'Is external',
    ],
    'settings' => [
        'main_currency' => 'Default currency ISO code (eg. GBP)',
        'autoupdate_settings' => 'Autoupdate settings',
        'enable_autoupdate' => 'Enable autoupdate',
        'send_request' => 'Send request...',
        'daily' => 'daily',
        'weekly' => 'weekly',
        'on' => 'on',
        'mondays' => 'Mondays',
        'tuesdays' => 'Tuesdays',
        'wednesdays' => 'Wednesdays',
        'thursdays' => 'Thursdays',
        'fridays' => 'Fridays',
        'saturdays' => 'Saturdays',
        'sundays' => 'Sundays',
        'at' => 'at...',
        'connection_settings' => 'Connection settings',
        'spawn_timeout' => 'Spawn timeout',
        'spawn_timeout_desc' => 'Amount of seconds after which system will treat spawn as offline. Default and recommended is 10.',
        'autoupdate_tab' => 'AutoUpdate',
        'connection_tab' => 'Connections',
        'vultr_settings' => 'Vultr Settings',
        'cloud_tab' => 'Cloud',
        'vultr_api_key' => 'Vultr API Key',
        'vultr_region_id' => 'Vultr Region',
        'vultr_plan_id' => 'Vultr Plan',
        'vultr_os_id' => 'Vultr Operating System',
        'vultr_snapshot_id' => 'Vultr Snapshot',
        'vultr_sshkey_id' => 'Vultr SSH Key',
        'core_settings' => 'Core Settings',
        'core_tab' => 'Core Settings',
        'git_path' => 'Git binary path (eg /usr/bin/git)',
        'vultr_script_id' => 'Vultr Script ID',
        'enable_dependencies' => 'Enable automatic dependencies download',
        'reject_undisabled' => 'Reject disabled projects requests',
        'reject_undisabled_desc' => 'Will reject any auth request from any project that is not enabled',
        'reject_unpaid' => 'Reject unpaid projects requests',
        'reject_unpaid_desc' => 'Will reject any auth request from any project that is not paid for',
        'reject_unsupported' => 'Reject unsupported projects requests',
        'reject_unsupported_desc' => 'Will reject any auth request from any project that has no support',
        'autoupdate_external_plugins' => 'Autoupdate external plugins',
        'autoupdate_external_cron' => 'External plugins autoupdate cron string. Leave empty for daily, 6 AM',
    ],
    'nav' => [
        'plugin_mother' => 'Plugin Mother',
        'settings_desc' => 'Configure upstream settings',
        'settings_group' => 'OctoberExchange',
        'plugins' => 'Plugins',
        'projects' => 'Projects',
        'deploy_types' => 'Deploy Types',
        'license_types' => 'License Types',
        'bought_licenses' => 'Licenses',
        'cloud_nodes' => 'Cloud Nodes',
        'plugin_groups' => 'Plugin Groups',
        'themes' => 'Themes',
        'plugin_categories' => 'Plugin Categories',
        'license_orders' => 'License Orders',
    ],
    'cloudnode' => [
        'new' => 'New Cloud Node',
        'label' => 'Cloud Node',
        'create_title' => 'Create Cloud Node',
        'update_title' => 'Edit Cloud Node',
        'preview_title' => 'Preview Cloud Node',
        'list_title' => 'Manage Cloud Nodes',
    ],
    'cloudnodes' => [
        'delete_selected_confirm' => 'Delete the selected Cloud Nodes?',
        'menu_label' => 'Cloud Nodes',
        'return_to_list' => 'Return to Cloud Nodes',
        'delete_confirm' => 'Do you really want to delete this Cloud Node?',
        'delete_selected_success' => 'Successfully deleted the selected Cloud Nodes.',
        'delete_selected_empty' => 'There are no selected Cloud Nodes to delete.',
    ],
    'plugingroup' => [
        'new' => 'New Plugin Group',
        'label' => 'Plugin Group',
        'create_title' => 'Create Plugin Group',
        'update_title' => 'Edit Plugin Group',
        'preview_title' => 'Preview Plugin Group',
        'list_title' => 'Manage Plugin Groups',
    ],
    'plugingroups' => [
        'delete_selected_confirm' => 'Delete the selected Plugin Groups?',
        'menu_label' => 'Plugin Groups',
        'return_to_list' => 'Return to Plugin Groups',
        'delete_confirm' => 'Do you really want to delete this Plugin Group?',
        'delete_selected_success' => 'Successfully deleted the selected Plugin Groups.',
        'delete_selected_empty' => 'There are no selected Plugin Groups to delete.',
    ],
    'testtts' => [
        'delete_selected_confirm' => 'Are you sure you want to delete selected?',
    ],
    'theme' => [
        'new' => 'New Theme',
        'label' => 'Theme',
        'create_title' => 'Create Theme',
        'update_title' => 'Edit Theme',
        'preview_title' => 'Preview Theme',
        'list_title' => 'Manage Themes',
        'delete_selected_confirm' => 'Are you sure you want to delete selected theme?',
    ],
    'themes' => [
        'delete_selected_confirm' => 'Delete the selected Themes?',
        'menu_label' => 'Themes',
        'return_to_list' => 'Return to Themes',
        'delete_confirm' => 'Do you really want to delete this Theme?',
        'delete_selected_success' => 'Successfully deleted the selected Themes.',
        'delete_selected_empty' => 'There are no selected Themes to delete.',
    ],
    'plugincategory' => [
        'new' => 'New Plugin Category',
        'label' => 'Plugin Category',
        'create_title' => 'Create Plugin Category',
        'update_title' => 'Edit Plugin Category',
        'preview_title' => 'Preview Plugin Category',
        'list_title' => 'Manage Plugin Categories',
    ],
    'plugincategories' => [
        'delete_selected_confirm' => 'Delete the selected Plugin Categories?',
        'menu_label' => 'Plugin Categories',
        'return_to_list' => 'Return to Plugin Categories',
        'delete_confirm' => 'Do you really want to delete this Plugin Category?',
        'delete_selected_success' => 'Successfully deleted the selected Plugin Categories.',
        'delete_selected_empty' => 'There are no selected Plugin Categories to delete.',
    ],
    'licenseorder' => [
        'new' => 'New License Order',
        'label' => 'License Order',
        'create_title' => 'Create License Order',
        'update_title' => 'Edit License Order',
        'preview_title' => 'Preview License Order',
        'list_title' => 'Manage License Orders',
    ],
    'licenseorders' => [
        'delete_selected_confirm' => 'Delete the selected License Orders?',
        'menu_label' => 'License Orders',
        'return_to_list' => 'Return to License Orders',
        'delete_confirm' => 'Do you really want to delete this License Order?',
        'delete_selected_success' => 'Successfully deleted the selected License Orders.',
        'delete_selected_empty' => 'There are no selected License Orders to delete.',
    ],
    'components' => [
        'financial' => [
            'name' => 'Financial Component',
            'description' => 'No description provided yet...',
        ],
    ],
];
