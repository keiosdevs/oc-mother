<?php namespace Keios\PluginMother\Facades;

use October\Rain\Support\Facade;

/**
 * Class MoneyFormatter
 * @package Keios\PaymentGateway\Facades
 */
class MoneyFormatter extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'pluginmother.moneyformatter';
    }
}