<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2/20/17
 * Time: 5:56 PM
 */

namespace Keios\PluginMother\Classes;


use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ZipArchive;

/**
 * Class Zipper
 *
 * @package Keios\PluginMother\Classes
 */
class Zipper
{
    /**
     * @param $source
     * @param $destination
     *
     * @return bool
     */
    public function zip($source, $destination, $skipGit = false)
    {
        if (!extension_loaded('zip') || !file_exists($source)) {
            return false;
        }

        $zip = new ZipArchive();
        if (!$zip->open($destination, ZipArchive::CREATE)) {
            return false;
        }

        $source = str_replace('\\', '/', realpath($source));

        if (is_dir($source) === true) {
            $files = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($source),
                RecursiveIteratorIterator::SELF_FIRST
            );

            foreach ($files as $file) {
                if($skipGit && strpos( $file->getPathname(), '.git') !== false){
                    continue;
                }
                $file = str_replace('\\', '/', $file);

                // Ignore "." and ".." folders
                if (in_array(substr($file, strrpos($file, '/') + 1), ['.', '..'], false)) {
                    continue;
                }

                $file = realpath($file);

                if (is_dir($file) === true) {
                    $zip->addEmptyDir(str_replace($source.'/', '', $file.'/'));
                } else {
                    if (is_file($file) === true) {
                        $zip->addFromString(str_replace($source.'/', '', $file), file_get_contents($file));
                    }
                }



            }
        }
    }
}
