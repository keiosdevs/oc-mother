<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2/20/17
 * Time: 7:48 PM
 */

namespace Keios\PluginMother\Classes;

use Keios\PluginMother\Classes\Git\Git;
use Keios\PluginMother\Models\Plugin;
use Keios\PluginMother\Models\Project;
use Keios\PluginMother\Models\Settings;

/**
 * Class PluginPoller
 *
 * @package Keios\PluginMother\Classes
 */
class PluginPoller
{
    /**
     * @var FileTools
     */
    private $fileTools;

    /**
     * @var DeployTools
     */
    private $deployTools;

    /**
     * PluginPoller constructor.
     */
    public function __construct()
    {
        $this->fileTools = new FileTools();
        $this->deployTools = new DeployTools();
    }

    /**
     * @param Plugin $plugin
     *
     * @return array
     * @throws \ApplicationException
     * @throws \Exception
     */
    public function pollPlugin(Plugin $plugin)
    {
        $logs = [];
        $motherPluginsPath = storage_path().'/mother_plugins';
        $namespace = strtolower($plugin->namespace);
        $directory = strtolower($plugin->directory);
        $pluginPath = $motherPluginsPath.'/'.$namespace.'/'.$directory;
        /** @var array $gitBranches */
        $gitBranches = $plugin->git_branches;
        if (!$gitBranches) {
            $gitBranches = [
                ['deploy_type' => 'production', 'deploy_branch' => $plugin->git_branch],
            ];
        }
        $settings = Settings::instance();
        $git = new Git();
        if ($settings->get('git_path')) {
            $git->setBin($settings->get('git_path'));
        }

        $gitBranches = $this->attachOverrideBranches($plugin, $gitBranches);
        foreach ($gitBranches as $branch) {
            $branchCode = str_replace('/', '-', $branch['deploy_branch']);
            $deployBranch = $branch['deploy_branch'];
            $deployPath = $pluginPath.'/'.$branchCode;

            if (is_dir($pluginPath) && file_exists($deployPath.'/Plugin.php')) {
                $repo = $git->open($deployPath);
                $repo->checkout($deployBranch);
                $repo->run('reset --hard');
                $repo->pull();
                $logs[] = $plugin->name.' has been pulled from '.$deployBranch;
            } else {
                $this->makePluginsPath($motherPluginsPath, $namespace, $directory, $branchCode);
                $git->cloneRemote($deployPath, $plugin->git_ssh_path, null, $deployBranch);
                $logs[] = $plugin->name.' has been created from '.$deployBranch;
            }
            $zipper = new Zipper();
            $this->checkZipDirectory($motherPluginsPath, $branchCode);
            $zipPath = $motherPluginsPath.'/'.$branchCode.'/'.$plugin->slug.'.zip';
            if (is_file($zipPath)) {
                unlink($zipPath);
            }
            $zipper->zip($pluginPath, $zipPath);
            if(env('ENABLE_GITLESS')){
                //rename($pluginPath.'/'.$branchCode.'/.git', $pluginPath.'/.git');
                $zipPath = $motherPluginsPath.'/'.$branchCode.'/'.$plugin->slug.'_gitless.zip';
                $zipper->zip($pluginPath, $zipPath, true);
                //rename($pluginPath.'/.git', $pluginPath.'/'.$branchCode.'/.git');
            }

        }
        CacheManager::clearPluginList();

        return $logs;
    }

    /**
     * I hate myself
     *
     * @param Plugin $plugin
     * @param array  $branches
     *
     * @return array
     */
    private function attachOverrideBranches($plugin, $branches)
    {
        $projects = Project::all();
        $allOverrides = [];
        $existingBranches = [];
        foreach ($branches as $branch) {
            $existingBranches[] = $branch['deploy_branch'];
        }
        foreach ($projects as $project) {
            if ($project->plugin_overrides) {
                $allOverrides[] = $project->plugin_overrides;
            }
        }
        foreach ($allOverrides as $projectOverride) {
            foreach ($projectOverride as $override) {
                if ($plugin->slug === $override['plugin_slug']) {
                    $branchArray = [
                        'deploy_type'   => 'custom',
                        'deploy_branch' => $override['plugin_branch'],

                    ];
                    if (!in_array($override['plugin_branch'], $existingBranches, true)) {
                        $branches[] = $branchArray;
                    }
                }
            }
        }

        return $branches;
    }

    /**
     * @param string $motherPluginsPath
     * @param string $branchCode
     *
     * @throws \ApplicationException
     */
    private function checkZipDirectory($motherPluginsPath, $branchCode)
    {
        $path = $motherPluginsPath.'/'.$branchCode;
        if (!is_dir($path)) {
            $this->fileTools->mkdir($path);
        }
    }

    /**
     * @param string      $mainPath
     * @param string      $namespace
     * @param string      $dir
     * @param null|string $branchCode
     * @throws \ApplicationException
     */
    private function makePluginsPath($mainPath, $namespace, $dir, $branchCode = null)
    {
        if (!is_dir($mainPath)) {
            $this->mkdir($mainPath);
        }
        if (!is_dir($mainPath.'/'.$namespace)) {
            $this->mkdir($mainPath.'/'.$namespace);
        }
        if (!is_dir($mainPath.'/'.$namespace.'/'.$dir)) {
            $this->mkdir($mainPath.'/'.$namespace.'/'.$dir);
        }
        if ($branchCode && !is_dir($mainPath.'/'.$namespace.'/'.$dir.'/'.$branchCode)) {
            $this->mkdir($mainPath.'/'.$namespace.'/'.$dir.'/'.$branchCode);
        }
    }

    /**
     * @param string $path
     *
     * @throws \ApplicationException
     */
    private function mkdir($path)
    {
        if (!@mkdir($path) && !is_dir($path)) {
            throw new \ApplicationException(\Lang::trans('keios.pluginmother::lang.errors.cannot_create_directory'));
        }
    }

}
