<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 1/16/17
 * Time: 1:23 PM
 */

namespace Keios\PluginMother\Classes;

use October\Rain\Exception\ApplicationException;

/**
 * Class RequestSender
 *
 * @package Keios\PluginSpawn\Classes
 */
class RequestSender
{
    /**
     * Headers array
     *
     * @var array
     */
    protected $headers = [];

    /**
     * RequestSender constructor.
     *
     * @param string      $contentType
     * @param string|null $bearerToken
     */
    public function __construct($bearerToken = null, $contentType = 'application/json')
    {
        $this->headers[] = 'Content-Type: '.$contentType;
        if ($bearerToken) {
            $this->headers[] = 'Authorization: Bearer '.$bearerToken;
        }
    }

    /**
     * Send post request to given url
     *
     * @param array  $data
     * @param string $url
     *
     * @param int    $connectTimeout
     *
     * @return array Http Code => ['detail']
     * @throws ApplicationException
     */
    public function sendPostRequest(array $data, $url, $connectTimeout = 0)
    {
        $response = [];
        $requestData = json_encode($data, JSON_UNESCAPED_SLASHES);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $requestData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $connectTimeout);
        $result = curl_exec($ch);

        if (false === $result) {
            $this->handleError($ch, $data, $url);
        }
        
        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response[$responseCode] = [
            'code'    => $responseCode,
            'details' => $result,
        ];

        curl_close($ch);

//        \Log::debug('Request: '.print_r($data, true).' to '.$url.PHP_EOL.'Post Response: '.print_r($result, true));

        return $response;
    }

    /**
     * Send put request to given url
     *
     * @param array  $data
     * @param string $url
     *
     * @return array Http Code
     */
    public function sendPutRequest(array $data, $url)
    {
        $response = [];
        $requestData = http_build_query($data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $requestData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        if (false === $result) {
            $this->handleError($ch, $data, $url);
        }
        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response[$responseCode] = [
            'code'    => $responseCode,
            'details' => $result,
        ];

        curl_close($ch);
        \Log::debug('Put Response: '.print_r($result, true));

        return $response;
    }

    /**
     * Send put request to given url
     *
     * @param array  $data
     * @param string $url
     *
     * @return array Http Code
     */
    public function sendGetRequest(array $data, $url)
    {
        $response = [];
        $requestData = http_build_query($data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $requestData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        if (false === $result) {
            $this->handleError($ch, $data, $url);
        }
        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response[$responseCode] = [
            'code'    => $responseCode,
            'details' => $result,
        ];

        curl_close($ch);
        \Log::debug('Get Response: '.print_r($result, true));

        return $response;
    }


    /**
     * Send delete request to given url
     *
     * @param array  $data
     * @param string $url
     *
     * @return array
     */
    public function sendDeleteRequest(array $data, $url)
    {
        $response = [];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        if (false === $result) {
            $this->handleError($ch, $data, $url);
        }
        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response[$responseCode] = [
            'code'    => $responseCode,
            'details' => $result,
        ];

        curl_close($ch);
        \Log::debug('Delete Response: '.print_r($result, true));

        return $response;
    }

    /**
     * Add custom header
     *
     * @param string $header
     */
    public function addHeader($header)
    {
        $this->headers[] = $header;
    }

    /**
     * Log error and throw exception
     *
     * @param resource $ch
     * @param array    $data
     * @param string   $url
     *
     * @throws ApplicationException
     * @throws \October\Rain\Exception\ApplicationException
     */
    protected function handleError($ch, $data, $url)
    {
        \Log::error(
            'Request error for url: '.$url.' and data: '
            .print_r($data, true)
            .': '.curl_error($ch)
            .' code:'.curl_error(
                $ch
            )
        );
        throw new ApplicationException(curl_error($ch), curl_errno($ch));
    }
}