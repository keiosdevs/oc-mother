<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 3/4/17
 * Time: 9:41 PM
 */

namespace Keios\PluginMother\Classes;

use Keios\PluginMother\Classes\Connectors\VultrConnector;
use Keios\PluginMother\Models\Project;
use Keios\PluginMother\Models\Settings;
use phpseclib\Net\SSH2;

/**
 * Class CloudManager
 *
 * @package Keios\PluginMother\Classes
 */
class CloudManager
{

    /**
     * @param Project $project
     * @param         $ip
     * @param         $sshPort
     * @param         $sshKeyPath
     *
     * @throws \ApplicationException
     */
    public function installSpawnOnVultrNode($project, $ip, $sshPort, $sshKeyPath)
    {
        $cleanDomain = str_replace(['http://', 'https://', '/'], '', $project->domain);
        $vars = [
            'project_domain' => $cleanDomain,
            'project_key'    => $project->code,
            'instance_name'  => $project->slug,
            'dbname'         => str_replace('-', '', $project->slug),
        ];
        $script = base_path().'/plugins/keios/pluginmother/resources/deploy_spawn.sh';
        $content = file_get_contents($script);
        $parsed = \Twig::parse($content, $vars);
        $node = $project->getVultrNode();
        $nodeDetails = $node->details;
        $password = null;
        if (array_key_exists('default_password', $nodeDetails)) {
            $password = $nodeDetails['default_password'];
        }
        if (!$password) {
            throw new \ApplicationException('No password provided');
        }
        $sshConnector = new SSH2($cleanDomain);
        $sshConnector->login('root', $password);
        $sshConnector->exec('echo '.$parsed.' > /root/install_spawn.sh');
        $sshConnector->exec('chmod +x /root/install_spawn.sh');
        $sshConnector->exec('bash /root/install_spawn.sh &> /tmp/spawn-install.log');

        // TODO send to ssh
    }

    /**
     * @param $remoteId
     * @param $type
     *
     * @return array
     */
    public function getNodeDetails($remoteId, $type)
    {
        $settings = Settings::instance();
        if ($type === 'vultr') {
            return $this->getVultrNodeDetails($remoteId, $settings);
        }

        return [];
    }

    public function getNodeArrayMap()
    {
        return [
            'SUBID'                => [
                'type'     => 'integer',
                'db_field' => 'remote_id',
            ],
            'os'                   => [
                'type'     => 'string',
                'db_field' => 'os',
            ],
            'ram'                  => [
                'type'     => 'string',
                'db_field' => 'meta',
            ],
            'disk'                 => [
                'type'     => 'string',
                'db_field' => 'meta',
            ],
            'main_ip'              => [
                'type'     => 'string',
                'db_field' => 'ip',
            ],
            'vcpu_count'           => [
                'type'     => 'integer',
                'db_field' => 'meta',
            ],
            'location'             => [
                'type'     => 'string',
                'db_field' => 'location',
            ],
            'DCID'                 => [
                'type'     => 'integer',
                'db_field' => 'meta',
            ],
            'default_password'     => [
                'type'     => 'string',
                'db_field' => 'meta',
            ],
            'date_created'         => [
                'type'     => 'datetime',
                'db_field' => 'meta',
            ],
            'pending_charges'      => [
                'type'     => 'float',
                'db_field' => 'meta',
            ],
            'status'               => [
                'type'     => 'string',
                'db_field' => 'meta',
            ],
            'cost_per_month'       => [
                'type'     => 'float',
                'db_field' => 'meta',
            ],
            'current_bandwidth_gb' => [
                'type'     => 'float',
                'db_field' => 'meta',
            ],
            'allowed_bandwidth_gb' => [
                'type'     => 'integer',
                'db_field' => 'meta',
            ],
            'netmask_v4'           => [
                'type'     => 'string',
                'db_field' => 'meta',
            ],
            'gateway_v4'           => [
                'type'     => 'string',
                'db_field' => 'meta',
            ],
            'power_status'         => [
                'type'     => 'string',
                'db_field' => 'meta',
            ],
            'server_state'         => [
                'type'     => 'string',
                'db_field' => 'meta',
            ],
            'VPSPLANID'            => [
                'type'     => 'integer',
                'db_field' => 'meta',
            ],
            'v6_main_ip'           => [
                'type'     => 'string',
                'db_field' => 'meta',
            ],
            'v6_network_size'      => [
                'type'     => 'string',
                'db_field' => 'meta',
            ],
            'v6_network'           => [
                'type'     => 'string',
                'db_field' => 'meta',
            ],
            'v6_networks'          => [
                'type'     => 'array',
                'db_field' => 'meta',
            ],
            'label'                => [
                'type'     => 'string',
                'db_field' => 'label',
            ],
            'internal_ip'          => [
                'type'     => 'string',
                'db_field' => 'meta',
            ],
            'kvm_url'              => [
                'type'     => 'string',
                'db_field' => 'meta',
            ],
            'auto_backups'         => [
                'type'     => 'string',
                'db_field' => 'meta',
            ],
            'tag'                  => [
                'type'     => 'string',
                'db_field' => 'meta',
            ],
            'OSID'                 => [
                'type'     => 'integer',
                'db_field' => 'meta',
            ],
            'APPID'                => [
                'type'     => 'integer',
                'db_field' => 'meta',
            ],
            'FIREWALLGROUPID'      => [
                'type'     => 'integer',
                'db_field' => 'meta',
            ],
        ];
    }

    /**
     * @param $remoteId
     * @param $settings
     *
     * @return array
     */
    private function getVultrNodeDetails($remoteId, $settings)
    {
        $conn = new VultrConnector();

        $key = $settings->get('vultr_api_key');
        $client = $conn->authorize($key);

        return $client->server()->getDetail($remoteId);
    }
}