<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 4/22/17
 * Time: 2:03 AM
 */

namespace Keios\PluginMother\Classes;

use Carbon\Carbon;
use Keios\MoneyRight\Currency;
use Keios\MoneyRight\Money;
use Keios\PluginMother\Models\License;
use Keios\PluginMother\Models\LicenseOrder;
use Keios\PluginMother\Models\Project;
use Keios\PluginMother\Models\ProjectOrder;
use Keios\PluginMother\Repositories\LicenseRepository;
use Keios\PluginMother\Repositories\ProjectRepository;
use Keios\PluginMother\ValueObjects\PluginFinancial;
use Keios\PluginMother\ValueObjects\ProjectFinancial;
use RainLab\User\Models\User;
use Keios\PluginMother\Models\Plugin as PluginModel;

/**
 * Class CostCalculator
 * @package Keios\PluginMother\Classes
 */
class CostCalculator
{
    /**
     *
     */
    const currency = 'USD';

    /**
     * @var Currency
     */
    public $currency;
    /**
     * @var LicenseRepository
     */
    public $licenseRepo;

    /**
     * @var ProjectRepository
     */
    public $projectRepo;
    /**
     * @var array
     */
    private $promotions = [];

    /**
     * @var
     */
    private $projectFinancial;

    /**
     * @var Project
     */
    private $project;

    /**
     * CostCalculator constructor.
     * @param array $promotions
     * @throws \Keios\MoneyRight\Exceptions\UnknownCurrencyException
     */
    public function __construct(array $promotions = [])
    {
        $this->licenseRepo = new LicenseRepository();
        $this->projectRepo = new ProjectRepository();
        $this->promotions = $promotions;
        $this->currency = new Currency(self::currency);
    }

    /**
     * @param $project
     * @return ProjectFinancial
     */
    public function generateProjectFinancial($project)
    {
        $this->create($project);

        return $this->getProjectFinancial();
    }

    /**
     * @param Project $project
     */
    public function create($project)
    {
        $this->projectFinancial = new ProjectFinancial($project);
        $this->project = $project;
        foreach ($project->plugins as $plugin) {
            $licOrder = $this->licenseRepo->getLicenseEntry($project, $plugin);
            if (!$licOrder) {
                $licOrder = $this->createLicOrder($plugin);
            }

            $pluginFinancial = new PluginFinancial($licOrder);
            $this->projectFinancial->pluginFinancials[] = $pluginFinancial;
            $this->projectFinancial->totals = $this->recalculateTotals();
            $this->projectFinancial->total = $this->recalculateTotal();
            $payments = $this->recalculatePayments($project);
            $this->projectFinancial->paid = $payments['paid'];
            $this->projectFinancial->unpaid = $payments['unpaid'];
            $this->projectFinancial->isPaid = $payments['is_paid'];
        }
    }

    /**
     * @param Project $project
     * @return array
     */
    public function recalculatePayments(Project $project)
    {
        /** @var Money $total */
        $total = $this->projectFinancial->total;
        $result = [
            'total'   => $total,
            'paid'    => new Money(0, $total->getCurrency()),
            'unpaid'  => new Money($total->getAmountString(), $total->getCurrency()),
            'is_paid' => false,
        ];
        $projectOrders = ProjectOrder::where('project_id', $project->id)->get();

        foreach ($projectOrders as $projectOrder) {
            /** @var Order $order */
            $order = $projectOrder->order;
            /** @var Money $amount */
            $amount = $order->amount;

            if ($order->successful_payment_id) {
                $result['paid'] = $result['paid']->add($amount);
                $result['unpaid'] = $result['unpaid']->subtract($amount);
            }

        }
        /** @var Money $unpaid */
        $unpaid = $result['unpaid'];
        if ($unpaid->getAmount() == 0) {
            $result['is_paid'] = true;
        }

        return $result;
    }

    /**
     * @return ProjectFinancial
     */
    public function getProjectFinancial()
    {
        return $this->projectFinancial;
    }

    /**
     * @return Money
     * @throws \Keios\MoneyRight\Exceptions\InvalidArgumentException
     */
    public function recalculateTotal()
    {
        $total = new Money(0, $this->currency);
        foreach ($this->projectFinancial->totals as $currency => $money) {
            if ($currency !== self::currency) {
                // TODO - converter from PG went out
                //$money = $this->converter->convertToNative($money);
                //$total->add($money);
            } else {
                $total = $total->add($money);
            }
        }
        $this->projectFinancial->total = $total;

        return $total;
    }

    /**
     * @return array
     * @throws \Keios\MoneyRight\Exceptions\UnknownCurrencyException
     * @throws \Keios\MoneyRight\Exceptions\InvalidArgumentException
     */
    public function recalculateTotals()
    {
        $currencySorted = [];
        foreach ($this->projectFinancial->pluginFinancials as $pluginFinancial) {
            if(is_string($pluginFinancial->currency)){
                $pluginFinancial->currency = new Currency($pluginFinancial->currency);
            }
            if ($pluginFinancial->currency == null) {
                $pluginFinancial->currency = $this->currency;
                $pluginFinancial->amount = 0;
                $pluginFinancial->isPaid = true;
                $pluginFinancial->value = new Money(0, $this->currency);
            }

            if (!array_key_exists($pluginFinancial->currency->getIsoCode(), $currencySorted)) {
                $currencySorted[$pluginFinancial->currency->getIsoCode()] = [];
            }
            $currencySorted[$pluginFinancial->currency->getIsoCode()][] = $pluginFinancial;
        }
        $totals = [];

        foreach ($currencySorted as $currencyIso => $pluginFinancials) {
            $currencyTotal = new Money(0, new Currency($currencyIso));
            foreach ($pluginFinancials as $pluginFinancial) {
                $currencyTotal = $currencyTotal->add($pluginFinancial->value);
            }
            $totals[$currencyIso] = $currencyTotal;
        }
        $this->projectFinancial->totals = $totals;

        return $totals;
    }

    /**
     * @param $plugin
     * @return LicenseOrder
     * @throws \Exception
     */
    public function createLicOrder($plugin)
    {

        $license = $this->getDefaultLicense();
        $licenseEntry = new LicenseOrder();
        $licenseEntry->license_id = $license->id;
        $licenseEntry->plugin_id = $plugin->id;
        $licenseEntry->project_id = $this->project->id;
        $licenseEntry->amount = $license->price;
        $licenseEntry->is_paid = 1;
        $licenseEntry->valid_to = Carbon::now()->addDays(3270);
        $licenseEntry->deadline = Carbon::now()->addDays(3270);
        $licenseEntry->currency = $this->currency->getIsoCode();
        $licenseEntry->save();

        return $licenseEntry;
    }

    /**
     * @return mixed
     * @throws \ApplicationException
     */
    private function getDefaultLicense()
    {
        $license = License::where('name', 'Default')->orWhere('name', 'Promotion')->first();
        if(!$license){
            $license = License::where('price', 0)->first();
        }
        if(!$license){
            throw new \ApplicationException('Create some free license that will work as default first!');
        }
        return $license;

    }


    /**
     * @param $plugin
     * @param $license
     * @param $project
     * @return bool
     * @throws \ApplicationException
     */
    public function createFinancialEntry($plugin, $license, $project)
    {
        if (!$plugin || !$license || !$project) {
            throw new \ApplicationException('Invalid data for financial operation');
        }

        $exists = $this->licenseRepo->getLicenseEntry($project, $plugin);
        if ($exists) {
            throw new \ApplicationException('This license is already attached to this project!');
        }
        $cost = $this->calculatePluginLicenseCost($license, $project->user);
        $toInsert = [
            'plugin_id'  => $plugin->id,
            'license_id' => $license->id,
            'project_id' => $project->id,
            'amount'     => $cost->getAmountString(),
            'currency'   => $this->defineUserCurrency($project->user),
            'is_paid'    => false,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
        \DB::table('keios_pluginmother_financial')->insert([$toInsert]);

        return true;
    }

    /**
     * @param $user
     * @return Currency|string
     */
    public function defineUserCurrency($user)
    {
        // TODO per user currencies
        return self::currency;
    }

    /**
     * @param PluginModel $plugin
     * @param User        $user
     * @return array
     */
    public function calculatePluginCosts($plugin, $user)
    {
        $results = [];
        if (count($plugin->licenses) > 0) {
            foreach ($plugin->licenses as $license) {
                $results[$license->name] = [
                    'cost'    => $this->calculatePluginLicenseCost($license, $user),
                    'license' => $license,
                ];
            }

            return $results;
        }
        $results[] = [
            'cost'    => new Money(0, $this->currency),
            'license' => null,
        ];

        return $results;
    }

    /**
     * @param Project $project
     * @return bool
     * @throws \ApplicationException
     */
    public function validateProjectPaymentStatus(Project $project)
    {
        if (!$project->is_paid) {
            $owed = $this->getOwedAmount($project);

            if ($owed->getAmount() > 0) {
                throw new \ApplicationException('Project is not paid. Due amount is: '.$owed->getAmountString());
            }
        }

        return true;
    }

    /**
     * @param $project
     * @return Money
     * @throws \Keios\MoneyRight\Exceptions\InvalidArgumentException
     */
    public function getOwedAmount($project)
    {
        $total = $this->getCostForProject($project);
        $orders = $project->orders;

        foreach ($orders as $order) {
            if ($order->successful_payment_id) {
                $total = $total->subtract($order->amount);
            }
        }

        return $total;
    }

    /**
     * @param Project $project
     * @return Money
     * @throws \Keios\MoneyRight\Exceptions\InvalidArgumentException
     */
    public function getCostForProject($project)
    {
        $total = new Money(0, $this->currency);
        $plugins = $project->plugins;
        if (!$plugins) {
            return $total;
        }
        foreach ($plugins as $plugin) {
            $order = $this->licenseRepo->getLicenseEntry($project, $plugin);
            if ($order) {
                $cost = $this->calculatePluginCost($order, $project->user);
                $total = $total->add($cost);
            } else {

            }
        }

        if ($total->getAmount() < 0) {
            $total = new Money(0, $total->getCurrency());
        }

        return $total;
    }

    /**
     * @param License $license
     * @param         $user
     * @return Money
     */
    public function calculatePluginLicenseCost(License $license, $user)
    {
        $price = $this->convertToMoney($license->price);
        if ($user) {
            if ($user->perks) {
                // TODO user perks
            }
            if ($this->promotions) {
                // TODO promotions
            }
        }

        return $price;
    }

    /**
     * @param LicenseOrder $licenseOrder
     * @param User         $user
     * @return Money
     */
    public function calculatePluginCost(LicenseOrder $licenseOrder, $user)
    {
        $price = $this->convertToMoney($licenseOrder->amount, $licenseOrder->currency);
        if ($user->perks) {
            // TODO user perks
        }
        if ($this->promotions) {
            // TODO promotions
        }

        return $price;
    }

    /**
     * @return Currency
     * @throws \Keios\MoneyRight\Exceptions\UnknownCurrencyException
     */
    public function getCurrency()
    {
        return new Currency(self::currency);
    }

    /**
     * @param float|string $value
     * @param string|null  $iso
     * @return Money
     * @throws \Keios\MoneyRight\Exceptions\UnknownCurrencyException
     */
    public function convertToMoney($value, $iso = null)
    {
        if ($iso) {
            $currency = new Currency($iso);
        } else {
            $currency = $this->getCurrency();
        }

        return new Money($value, $currency);
    }
}