<?php namespace Keios\PluginMother\Classes;

use Illuminate\Support\ServiceProvider;

class PluginMotherServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            'pluginmother.moneyformatter',
            function () {
                return new MoneyFormatter($this->app['config'], $this->app['request']);
            }
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            'pluginmother.moneyformatter',
        ];
    }

}
