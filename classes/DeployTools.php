<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 3/1/17
 * Time: 4:51 PM
 */

namespace Keios\PluginMother\Classes;


use Keios\PluginMother\Models\DeployType;
use Keios\PluginMother\Models\Plugin;
use Keios\PluginMother\Models\Project;
use Keios\PluginMother\Models\Theme;

class DeployTools
{
    /**
     * @param Project $project
     *
     * @return array
     */
    public function getOverrides(Project $project)
    {
        $overrides = $project->plugin_overwrites;
        if (!$overrides) {
            $overrides = [];
        }

        return $overrides;
    }


    /**
     * @param $postData
     *
     * @return string
     */
    public function findDeployType($postData)
    {
        $deployType = 'production';
        if (array_key_exists('deploy_type', $postData)) {
            $deployType = $postData['deploy_type'];
        } else {
            $deployTypeModel = DeployType::where('is_default', true)->first();
            if ($deployTypeModel) {
                $deployType = $deployTypeModel->code;
            }
        }

        return $deployType;
    }


    /**
     * Send true as last argument to generate code for directory name
     *
     * @param Plugin $plugin
     * @param string $deployType
     * @param array  $overrides
     * @param bool   $asCode
     *
     * @return mixed
     */
    public function findPluginBranch(Plugin $plugin, $deployType, array $overrides = [], $asCode = false)
    {
        $branch = $plugin->git_branch;
        $branches = [];
        if ($plugin->git_branches) {
            $branches = $plugin->git_branches;
        }
        foreach ($branches as $branchData) {
            if ($branchData['deploy_type'] === $deployType) {
                $branch = $branchData['deploy_branch'];
            }
        }
        foreach ($overrides as $override) {
            if ($override['plugin_slug'] === $plugin->slug) {
                $branch = $override['plugin_branch'];
            }
        }
        if ($asCode) {
            return str_replace('/', '-', $branch);
        }

        return $branch;
    }

    /**
     * Send true as last argument to generate code for directory name
     *
     * @param Theme  $theme
     * @param string $deployType
     * @param array  $overrides
     * @param bool   $asCode
     *
     * @return mixed
     */
    public function findThemeBranch(Theme $theme, $deployType, array $overrides = [], $asCode = false)
    {
        $branch = $theme->git_branch;
        $branches = [];
        if ($theme->git_branches) {
            $branches = $theme->git_branches;
        }
        foreach ($branches as $branchData) {
            if ($branchData['deploy_type'] === $deployType) {
                $branch = $branchData['deploy_branch'];
            }
        }
        foreach ($overrides as $override) {
            if ($override['theme_slug'] === $theme->slug) {
                $branch = $override['theme_branch'];
            }
        }
        if ($asCode) {
            return str_replace('/', '-', $branch);
        }

        return $branch;
    }
}
