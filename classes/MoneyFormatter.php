<?php namespace Keios\PluginMother\Classes;

use Symfony\Component\HttpFoundation\Request;
use October\Rain\Config\Repository;
use October\Rain\Router\Helper;
use Keios\MoneyRight\Money;
use Keios\MoneyRight\Math;

/**
 * Class MoneyFormatter
 * @package Keios\PluginMother\Classes
 */
class MoneyFormatter
{
    /**
     * @var Repository
     */
    protected $config;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var bool|mixed
     */
    protected $frontendPreferLocal = false;

    /**
     * @var bool|mixed
     */
    protected $backendPreferLocal = false;

    /**
     * MoneyFormatter constructor.
     * @param Repository $repository
     * @param Request    $request
     */
    public function __construct(Repository $repository, Request $request)
    {
        $this->config = $repository;
        $this->request = $request;
        $this->frontendPreferLocal = $this->config->get('keios.paymentgateway.money.frontendPreferLocal');
        $this->backendPreferLocal = $this->config->get('keios.paymentgateway.money.backendPreferLocal');
    }

    /**
     * @param Money     $money
     * @param bool|null $override
     * @return string
     * @throws \Keios\MoneyRight\Exceptions\InvalidArgumentException
     */
    public function format(Money $money, $override = false)
    {
        $condition = $override;
        if (!is_bool($override) || null === $override) {
            $condition = $this->isBackendRequest();
        }
        switch ($condition) {
            case true:
                if ($this->backendPreferLocal) {
                    return $this->formatLocal($money);
                }

                return $this->formatIso($money);
                break;
            case false:
                if ($this->frontendPreferLocal) {
                    return $this->formatLocal($money);
                }

                return $this->formatIso($money);
                break;
        }

        return '';
    }

    /**
     * @param Money $money
     * @return string
     * @throws \Keios\MoneyRight\Exceptions\InvalidArgumentException
     */
    public function formatLocal(Money $money)
    {
        $currency = $money->getCurrency();
        if ($currency->getSymbolFirst()) {
            return $currency->getSymbol().str_replace(
                    '.',
                    $currency->getDecimalMark(),
                    Math::bcround($money->getAmountString(), Money::BASIC_PRECISION)
                );
        }

        return str_replace(
                '.',
                $currency->getDecimalMark(),
                Math::bcround($money->getAmountString(), Money::BASIC_PRECISION)
            ).$currency->getSymbol();
    }

    /**
     * @param Money $money
     * @return string
     * @throws \Keios\MoneyRight\Exceptions\InvalidArgumentException
     */
    public function formatIso(Money $money)
    {
        $currency = $money->getCurrency();

        return Math::bcround($money->getAmountString(), Money::BASIC_PRECISION).$currency->getIsoCode();
    }

    /**
     * @return bool
     */
    protected function isBackendRequest()
    {
        $pathArray = Helper::segmentizeUrl(urldecode($this->request->getPathInfo()));

        return count($pathArray) && $pathArray[0] === trim($this->config->get('cms.backendUri'), '/');
    }
}