<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2/28/17
 * Time: 4:10 PM
 */

namespace Keios\PluginMother\Classes;

use Keios\PluginMother\Models\DeployType;

/**
 * Class CacheManager
 *
 * @package Keios\PluginMother\Classes
 */
class CacheManager
{

    /**
     *
     */
    public static function clearPluginList()
    {
        self::clearPluginsData();
    }

    /**
     * @param null $plugin
     */
    public static function clearPluginData($plugin = null)
    {
        if ($plugin) {
            \Cache::forget('plugin_'.$plugin->slug);
            foreach ($plugin->categories as $category) {
                \Cache::forget($category->slug.'_plugins');
            }
        }
        self::clearPluginsData();
    }

    /**
     * @param null $category
     */
    public static function clearCategoryData($category = null){
        if($category){
            \Cache::forget($category->slug.'_plugins');
        }
        \Cache::forget('all_categories');
    }
    /**
     *
     */
    public static function clearPluginsData()
    {
        $deployTypeModels = DeployType::where('is_available', true)->get();
        foreach ($deployTypeModels as $deployTypeModel) {
            \Cache::forget($deployTypeModel->code.'_plugin_list');
        }
        \Cache::forget('plugin_list');
        \Cache::forget('featured_plugins');
        \Cache::forget('visible_plugins');
        \Cache::forget('all_categories');
        \Cache::forget('all_plugin_groups');
        \Cache::forget('all_plugins');
    }

    /**
     * Clear project from cache
     *
     * @param int|string $idOrSlugOrCode
     */
    public static function clearProject($idOrSlugOrCode)
    {
        \Cache::forget('project_'.$idOrSlugOrCode);
    }

    /**
     * Clears themes list cache
     */
    public static function clearThemeList()
    {
        $deployTypeModels = DeployType::where('is_available', true)->get();
        foreach ($deployTypeModels as $deployTypeModel) {
            \Cache::forget($deployTypeModel->code.'_theme_list');
        }
        \Cache::forget('theme_list');
    }
}
