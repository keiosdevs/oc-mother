<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 3/26/17
 * Time: 9:26 PM
 */

namespace Keios\PluginMother\Classes;

use Keios\PluginMother\Classes\Git\Git;
use Keios\PluginMother\Models\Project;
use Keios\PluginMother\Models\Settings;
use Keios\PluginMother\Models\Theme;

class ThemePoller
{
    /**
     * @var FileTools
     */
    private $fileTools;

    /**
     * @var DeployTools
     */
    private $deployTools;

    /**
     * PluginPoller constructor.
     */
    public function __construct()
    {
        $this->fileTools = new FileTools();
        $this->deployTools = new DeployTools();
    }

    /**
     * @param Theme $theme
     *
     * @return array
     * @throws \ApplicationException
     * @throws \Exception
     */
    public function pollTheme(Theme $theme)
    {
        $logs = [];
        $motherThemesPath = storage_path().'/mother_themes';
        $namespace = strtolower($theme->namespace);
        $directory = strtolower($theme->directory);
        $themePath = $motherThemesPath.'/'.$namespace.'/'.$directory;
        /** @var array $gitBranches */
        $gitBranches = $theme->git_branches;
        if (!$gitBranches) {
            $gitBranches = [
                ['deploy_type' => 'production', 'deploy_branch' => $theme->git_branch],
            ];
        }
        $settings = Settings::instance();
        $git = new Git();
        if ($settings->get('git_path')) {
            $git->setBin($settings->get('git_path'));
        }

        $gitBranches = $this->attachOverrideBranches($theme, $gitBranches);

        foreach ($gitBranches as $branch) {
            $branchCode = str_replace('/', '-', $branch['deploy_branch']);
            $deployBranch = $branch['deploy_branch'];
            $deployPath = $themePath.'/'.$branchCode;

            if (is_dir($themePath) && file_exists($deployPath.'/theme.yaml')) {
                $repo = $git->open($deployPath);
                $repo->checkout($deployBranch);
                $repo->pull();
                $logs[] = $theme->name.' has been pulled from '.$deployBranch;
            } else {
                $this->makeThemePath($motherThemesPath, $namespace, $directory, $branchCode);
                $git->cloneRemote($deployPath, $theme->git_ssh_path, null, $deployBranch);
                $logs[] = $theme->name.' has been created from '.$deployBranch;
            }
            $zipper = new Zipper();
            $this->checkZipDirectory($motherThemesPath, $branchCode);
            $zipPath = $motherThemesPath.'/'.$branchCode.'/'.$theme->slug.'.zip';
            if (is_file($zipPath)) {
                unlink($zipPath);
            }
            $zipper->zip($themePath, $zipPath);
            if(env('ENABLE_GITLESS')){
              $zipPath = $motherThemesPath.'/'.$branchCode.'/'.$theme->slug.'_gitless.zip';
              $zipper->zip($themePath, $zipPath, true);
            }
        }
        CacheManager::clearPluginList();

        return $logs;
    }

    private function removeDirRecursive($dirPath){
        if (! is_dir($dirPath)) {
            return null;
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                $this->removeDirRecursive($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }

    /**
     * I hate myself
     *
     * @param Theme $theme
     * @param array  $branches
     *
     * @return array
     */
    private function attachOverrideBranches($theme, $branches)
    {
        $projects = Project::all();
        $allOverrides = [];
        $existingBranches = [];
        foreach ($branches as $branch) {
            $existingBranches[] = $branch['deploy_branch'];
        }
        foreach ($projects as $project) {
            if ($project->plugin_overrides) {
                $allOverrides[] = $project->plugin_overrides;
            }
        }
        foreach ($allOverrides as $projectOverride) {
            foreach ($projectOverride as $override) {
                if(!array_key_exists('theme_slug', $override)){
                    continue;
                }
                if ($theme->slug === $override['theme_slug']) {
                    $branchArray = [
                        'deploy_type'   => 'custom',
                        'deploy_branch' => $override['theme_branch'],

                    ];
                    if (!in_array($override['theme_branch'], $existingBranches, true)) {
                        $branches[] = $branchArray;
                    }
                }
            }
        }

        return $branches;
    }

    /**
     * @param string $motherThemesPath
     * @param string $branchCode
     *
     * @throws \ApplicationException
     */
    private function checkZipDirectory($motherThemesPath, $branchCode)
    {
        $path = $motherThemesPath.'/'.$branchCode;
        if (!is_dir($path)) {
            $this->fileTools->mkdir($path);
        }
    }

    /**
     * @param string      $mainPath
     * @param string      $namespace
     * @param string      $dir
     * @param null|string $branchCode
     * @throws \ApplicationException
     */
    private function makeThemePath($mainPath, $namespace, $dir, $branchCode = null)
    {
        if (!is_dir($mainPath)) {
            $this->mkdir($mainPath);
        }
        if (!is_dir($mainPath.'/'.$namespace)) {
            $this->mkdir($mainPath.'/'.$namespace);
        }
        if (!is_dir($mainPath.'/'.$namespace.'/'.$dir)) {
            $this->mkdir($mainPath.'/'.$namespace.'/'.$dir);
        }
        if ($branchCode && !is_dir($mainPath.'/'.$namespace.'/'.$dir.'/'.$branchCode)) {
            $this->mkdir($mainPath.'/'.$namespace.'/'.$dir.'/'.$branchCode);
        }
    }

    /**
     * @param string $path
     *
     * @throws \ApplicationException
     */
    private function mkdir($path)
    {
        if (!@mkdir($path) && !is_dir($path)) {
            throw new \ApplicationException(\Lang::trans('keios.pluginmother::lang.errors.cannot_create_directory'));
        }
    }
}
