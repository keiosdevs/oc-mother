<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 3/2/17
 * Time: 10:10 AM
 */

namespace Keios\PluginMother\Classes;


use Keios\PluginMother\Models\Project;
use Keios\PluginMother\Repositories\ProjectRepository;

/**
 * Class AutoUpdater
 *
 * @package Keios\PluginMother\Classes
 */
class AutoUpdater
{
    /**
     * @var ProjectRepository
     */
    private $projectRepo;

    /**
     * AutoUpdater constructor.
     */
    public function __construct()
    {
        $this->projectRepo = new ProjectRepository();
    }

    /**
     * @return array
     */
    public function run()
    {
        $projects = $this->projectRepo->getAutoUpdatable();
        $responses = [];
        foreach ($projects as $project) {
            $responses[$project->domain] = $this->update($project);
        }

        return $responses;
    }

    /**
     * @param Project $project
     *
     * @return array
     */
    public function update(Project $project)
    {
        $domain = $project->domain;
        $url = $domain.'/api/v1/update';
        $data['project_key'] = $project->code;
        $requestSender = new RequestSender();

        return $requestSender->sendPostRequest($data, $url);
    }
}