<?php
namespace Keios\PluginMother\Classes\Git;

use Keios\PluginMother\Models\Settings;

/*
 * Git.php
 *
 * A PHP git library
 *
 * @package    Git.php
 * @version    0.1.4
 * @author     James Brumond
 * @copyright  Copyright 2013 James Brumond
 * @repo       http://github.com/kbjr/Git.php
 */

if (__FILE__ === $_SERVER['SCRIPT_FILENAME']) {
    die('Bad load order');
}

// ------------------------------------------------------------------------

/**
 * Git Interface Class
 *
 * This class enables the creating, reading, and manipulation
 * of git repositories.
 *
 * @class  Git
 */
class Git
{

    /**
     * Git executable location
     *
     * @var string
     */
    protected $bin = '/usr/bin/git';

    /**
     * Git constructor.
     * @param null $gitPath
     */
    public function __construct($gitPath = null)
    {
        if (!$gitPath) {
            $this->setGitPathFromSettings();
        }
    }

    /**
     *
     */
    public function setGitPathFromSettings()
    {
        $settings = Settings::instance();
        if($gitpath = $settings->get('git_path')){
            $this->setBin($gitpath);
        }
    }

    /**
     * Sets git executable path
     *
     * @param string $path executable location
     */
    public function setBin($path)
    {
        $this->bin = $path;
    }

    /**
     * Gets git executable path
     */
    public function getBin()
    {
        return $this->bin;
    }

    /**
     * Sets up library for use in a default Windows environment
     */
    public function windows_mode()
    {
        $this->setBin('git');
    }

    /**
     * Create a new git repository
     *
     * Accepts a creation path, and, optionally, a source path
     *
     * @access  public
     *
     * @param   string $repoPath repository path
     * @param   string $source    directory to source
     *
     * @return  GitRepo
     * @throws \Exception
     */
    public function &create($repoPath, $source = null)
    {
        $this->setGitPathFromSettings();
        $repo = new GitRepo();
        return $repo->createNew($repoPath, $source);
    }

    /**
     * Open an existing git repository
     *
     * Accepts a repository path
     *
     * @access  public
     *
     * @param   string $repoPath repository path
     *
     * @return  GitRepo
     */
    public function open($repoPath)
    {
        $this->setGitPathFromSettings();
        return new GitRepo($repoPath);
    }

    /**
     * Clones a remote repo into a directory and then returns a GitRepo object
     * for the newly created local repo
     *
     * Accepts a creation path and a remote to clone from
     *
     * @access  public
     *
     * @param   string $repoPath repository path
     * @param   string $remote    remote source
     * @param   string $reference reference path
     *
     * @param null     $branch
     *
     * @return GitRepo
     */
    public function &cloneRemote($repoPath, $remote, $reference = null, $branch = null)
    {
        $this->setGitPathFromSettings();
        $repo = new GitRepo();
        //Changed the below boolean from true to false, since this appears to be a bug when not using a reference repo.  A more robust solution may be appropriate to make it work with AND without a reference.
        return $repo->createNew($repoPath, $remote, $remote, $reference, $branch);
    }

    /**
     * Checks if a variable is an instance of GitRepo
     *
     * Accepts a variable
     *
     * @access  public
     *
     * @param   mixed $var variable
     *
     * @return  bool
     */
    public function isRepo($var)
    {
        $this->setGitPathFromSettings();
        return (get_class($var) === 'GitRepo');
    }

}