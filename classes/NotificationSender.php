<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2/20/17
 * Time: 3:07 PM
 */

namespace Keios\PluginMother\Classes;

use Keios\SlackNotifications\Classes\SlackMessageSender;
use Keios\SlackNotifications\Plugin;

/**
 * Class NotificationSender
 * @package Keios\PluginSpawn\Classes
 */
class NotificationSender
{
    /**
     * @param $message
     */
    public function send($message)
    {
        $this->sendToSlack($message);
    }

    /**
     * @param $message
     * @return null
     */
    private function sendToSlack($message)
    {
        if (!class_exists(Plugin::class)) {
            return null;
        }

        /** @var Maknz\Slack\Message $slackMessage */
        $slackMessage = \App::make('slack_message');

        $slackMessage->setText($message);

        return $slackMessage->send();
    }
}