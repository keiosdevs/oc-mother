<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 3/4/17
 * Time: 7:33 PM
 */

namespace Keios\PluginMother\Classes\Connectors;

use Keios\PluginMother\Contracts\CloudConnectorInterface;
use Keios\PluginMother\Exceptions\CloudConnectionException;
use Keios\PluginMother\ValueObjects\VultrNodeConfig;
use Vultr\VultrClient;
use Vultr\Adapter\CurlAdapter;

/**
 * Class VultrConnector
 *
 * @package Keios\PluginMother\Classes
 */
class VultrConnector implements CloudConnectorInterface
{

    /**
     * @var VultrClient
     */
    private $client;

    /**
     * VultrConnector constructor.
     */
    public function __construct()
    {

    }

    /**
     * @param null $apiKey
     *
     * @return VultrClient
     */
    public function authorize($apiKey = null)
    {
        $this->client = new VultrClient(
            new CurlAdapter($apiKey)
        );

        return $this->client;
    }

    /**
     *
     */
    public function addCpu()
    {
        // TODO: Implement addCpu() method.
    }

    /**
     *
     */
    public function addRam()
    {
        // TODO: Implement addRam() method.
    }

    /**
     *
     */
    public function addSshKey()
    {
        // TODO: Implement addSshKey() method.
    }

    /**
     * @return array
     */
    public function getAccountInfo()
    {
        return $this->client->metaData()->getAccountInfo();
    }

    /**
     * @return array
     * @throws \Keios\PluginMother\Exceptions\CloudConnectionException
     * @throws CloudConnectionException
     * @throws \Exception
     */
    public function getNodes()
    {
        try {
            $response = $this->client->server()->getList();
        } catch (\Exception $e) {
            // TODO: Add exceptions handling
            throw $e;
        }
        if (!$response) {
            throw new CloudConnectionException('Could not create the cloud node');
        }

        return $response;
    }

    /**
     * @param VultrNodeConfig $config
     *
     * @return int
     * @throws \Vultr\Exception\ApiException
     * @throws \Keios\PluginMother\Exceptions\CloudConnectionException
     * @throws \Exception
     */
    public function createNode($config)
    {
        try {
            $response = $this->client->server()->create(
                [
                    'DCID'                   => $config->DCID,
                    'VPSPLANID'              => $config->VPSPLANID,
                    'OSID'                   => $config->OSID,
                    'ipxe_chain_url'         => $config->ipxe_chain_url,
                    'ISOID'                  => $config->ISOID,
                    'SCRIPTID'               => $config->SCRIPTID,
                    'SNAPSHOTID'             => $config->SNAPSHOTID,
                    'enable_ipv6'            => $config->enable_ipv6,
                    'enable_private_network' => $config->enable_private_network,
                    'label'                  => $config->label,
                    'SSHKEYID'               => $config->SSHKEYID,
                    'auto_backups'           => $config->auto_backups,
                    'APPID'                  => $config->APPID,
                    'userdata'               => $config->userdata,
                    'notify_activate'        => $config->notify_activate,
                    'ddos_protection'        => $config->ddos_protection,
                    'reserved_ip_v4'         => $config->reserved_ip_v4,
                    'hostname'               => $config->hostname,
                    'tag'                    => $config->tag,
                ]
            );
        } catch (\Exception $e) {
            // TODO: Add exceptions handling
            throw $e;
        }
        if (!$response) {
            throw new CloudConnectionException('Could not create the cloud node');
        }

        return $response;
    }

    /**
     *
     */
    public function destroyNode()
    {
        // TODO: Implement destroyNode() method.
    }

    /**
     *
     */
    public function shutdownNode()
    {
        // TODO: Implement shutdownNode() method.
    }

    /**
     *
     */
    public function rebootNode()
    {
        // TODO: Implement rebootNode() method.
    }

    /**
     *
     */
    public function snapshotNode()
    {
        // TODO: Implement snapshotNode() method.
    }

    /**
     * @return array
     */
    public function getRegions()
    {
        return $this->client->region()->getList();
    }

}