<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 3/4/17
 * Time: 7:33 PM
 */

namespace Keios\PluginMother\Classes\Connectors;

use Keios\PluginMother\Contracts\CloudConnectorInterface;

class AmazonConnector implements CloudConnectorInterface
{
    public function authorize($apiKey = null)
    {
        // TODO: implement
    }

    public function getAccountInfo()
    {
        // TODO: implement
    }

    public function createNode($configInterface)
    {
        // TODO: implement
    }

    public function snapshotNode()
    {
        // TODO: implement
    }

    public function destroyNode()
    {
        // TODO: implement
    }

    public function rebootNode()
    {
        // TODO: implement
    }

    public function shutdownNode()
    {
        // TODO: implement
    }

    public function addRam()
    {
        // TODO: implement
    }

    public function addCpu()
    {
        // TODO: implement
    }

    public function addSshKey()
    {
        // TODO: implement
    }

    public function getRegions()
    {
            // TODO: implement
    }
}