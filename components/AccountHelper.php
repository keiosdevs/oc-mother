<?php namespace Keios\PluginMother\Components;

use Cms\Classes\ComponentBase;

class AccountHelper extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AccountHelper Component',
            'description' => 'Provides helper methods for Market frontend'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
}
