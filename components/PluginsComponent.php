<?php namespace Keios\PluginMother\Components;

use Cms\Classes\ComponentBase;
use Keios\Apparatus\Contracts\NeedsDependencies;
use Keios\PluginMother\Models\Plugin;
use Keios\PluginMother\Repositories\ProjectRepository;

/**
 * Class PluginsComponent
 *
 * @package Keios\PluginMother\Components
 */
class PluginsComponent extends ComponentBase implements NeedsDependencies
{
    /**
     * @const string
     */
    const proj_plug_pivot = 'keios_pluginmother_proj_plug';

    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'PluginsComponent Component',
            'description' => 'Displays available plugins',
        ];
    }

    /**
     * @var ProjectRepository
     */
    protected $projectsRepo;

    /**
     * @param ProjectRepository $projectRepo
     */
    public function injectProjectRepo(ProjectRepository $projectRepo)
    {
        $this->projectsRepo = $projectRepo;
    }

    /**
     * @return array
     */
    public function defineProperties()
    {
        return [];
    }

    /**
     *
     */
    public function onRun()
    {
        $this->page['plugins'] = Plugin::paginate(20);
    }

    /**
     * @throws \ApplicationException
     * @throws \ValidationException
     */
    public function onAddToProject()
    {
        $data = post();
        $this->validateAddToProject($data);
        $pluginId = $data['plugin_id'];
        $project = $this->projectsRepo->getBySlug($data['project']);
        $projectId = $project->id;
        $this->addToProject($projectId, $pluginId);

        return \Redirect::to('/');
    }

    /**
     * @param $projectId
     * @param $pluginId
     *
     * @return bool
     * @throws \ApplicationException
     */
    public function addToProject($projectId, $pluginId)
    {
        if(!is_int($projectId) || !is_int($pluginId)){
            throw new \ApplicationException(\Lang::trans('keios.pluginmother::lang.errors.invalid_project_or_plugin'));
        }
        $exists = \DB::table(self::proj_plug_pivot)->where('project_id', $projectId)->where(
            'plugin_id',
            $pluginId
        )->first();
        if ($exists) {
            throw new \ApplicationException(\Lang::trans('keios.pluginmother::lang.errors.plugin_already_attached'));
        }
        return $this->attachPluginToProject($projectId, $pluginId);
    }

    /**
     * @param array $data
     *
     * @throws \ValidationException
     */
    private function validateAddToProject(array $data)
    {
        $rules = [
            'project'   => 'required',
            'plugin_id' => 'required',
        ];
        $v = \Validator::make($data, $rules);
        if ($v->fails()) {
            throw new \ValidationException($v);
        }
    }

    /**
     * Quick insert to pivot
     *
     * @param int $projectId
     * @param int $pluginId
     *
     * @return bool
     */
    private function attachPluginToProject($projectId, $pluginId)
    {
        \DB::table(self::proj_plug_pivot)->insert(['project_id' => $projectId, 'plugin_id' => $pluginId]);

        return true;
    }
}
