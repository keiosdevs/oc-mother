<?php namespace Keios\PluginMother\Components;

use Cms\Classes\ComponentBase;
use Keios\Apparatus\Contracts\NeedsDependencies;
use Keios\PluginMother\Repositories\ProjectRepository;
use Keios\PluginMother\Models\Project;
use RainLab\User\Models\User;
use Schema;

/**
 * Class ProjectsComponent
 *
 * @package Keios\PluginMother\Components
 */
class ProjectsComponent extends ComponentBase implements NeedsDependencies
{
    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'ProjectsComponent Component',
            'description' => 'No description provided yet...',
        ];
    }

    /**
     * @return array
     */
    public function defineProperties()
    {
        return [];
    }

    /**
     * @var ProjectRepository
     */
    protected $repo;

    /**
     * @param ProjectRepository $repo
     */
    public function injectProjectRepo(ProjectRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     *
     */
    public function onRun()
    {
        $user = \Auth::getUser();
        if (!$user) {
            return \Redirect::to('/account'); // TODO: change to property
        }
        $this->page['projects'] = Project::where('user_id', $user->id)->get();
    }

    /**
     * @throws \ValidationException
     */
    public function onCreateProject()
    {
        $user = \Auth::getUser();
        $data = post();
        $this->validateCreateProject($data);
        $this->createProjectFromPost($user, $data);

        return \Redirect::to('/');
    }

    /**
     * @throws \ApplicationException
     * @throws \ValidationException
     */
    public function onUpdateProject()
    {
        $data = post();
        $this->validateUpdateProject($data);
        $repo = new ProjectRepository();
        $project = $repo->getBySlug($data['slug']);
        if (!$project) {
            throw new \ApplicationException(\Lang::trans('keios.pluginmother::lang.errors.project_not_found'));
        }
        $this->updateProjectFromPost($project, $data);
    }

    /**
     * @throws \ValidationException
     */
    public function onDeleteProject()
    {
        $data = post();
        $this->validateDeleteProject($data);
        $project = $this->repo->getBySlug($data['slug']);
        if ($project) {
            $project->delete();
        }
    }

    /**
     * @param User  $user
     * @param array $data
     *
     * @return Project
     */
    public function createProjectFromPost(User $user, array $data)
    {
        $project = new Project();
        $project->title = $data['title'];
        $project->slug = $this->generateSlug($data['title']);
        $project->code = $this->generateCode();
        $project->user_id = $user->id;
        $project->save();

        return $project;
    }

    /**
     * @param $project
     * @param $data
     *
     * @return Project
     */
    public function updateProjectFromPost(Project $project, array $data)
    {

        foreach ($data as $key => $value) {
            if (Schema::hasColumn($project->getTable(), $key)) {
                $project->$key = $value;
            }

            if ($key === 'title') {
                $project->slug = $this->generateSlug($value);
            }
        }

        $project->save();

        return $project;
    }

    /**
     * @param $string
     *
     * @return string
     */
    private function generateSlug($string)
    {
        $slug = strtolower(str_replace([' ', '_'], ['-', '-'], $string));
        $no = 1;
        while ($this->getExisting($slug)) {
            $slug = strtolower(str_replace([' ', '_'], ['', '-'], $string)).$no;
            ++$no;
        }

        return $slug;
    }

    /**
     * @param $slug
     *
     * @return bool|null
     */
    private function getExisting($slug)
    {
        if ($this->repo) {
            return $this->repo->getBySlug($slug) !== null;
        }

        return null;
    }

    /**
     * Helper function
     *
     * @param int $length
     *
     * @return string
     */
    private function generateCode($length = 20)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[random_int(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    /**
     * @param $data
     *
     * @throws \ValidationException
     */
    private function validateCreateProject($data)
    {
        $rules = [
            'title' => 'required',
        ];
        $v = \Validator::make($data, $rules);
        if ($v->fails()) {
            throw new \ValidationException($v);
        }
    }

    /**
     * @param $data
     *
     * @throws \ValidationException
     */
    private function validateUpdateProject($data)
    {
        $rules = [
            'slug' => 'required',
        ];
        $v = \Validator::make($data, $rules);
        if ($v->fails()) {
            throw new \ValidationException($v);
        }
    }

    /**
     * @param $data
     *
     * @throws \ValidationException
     */
    private function validateDeleteProject($data)
    {
        $rules = [
            'slug' => 'required',
        ];
        $v = \Validator::make($data, $rules);
        if ($v->fails()) {
            throw new \ValidationException($v);
        }
    }
}
