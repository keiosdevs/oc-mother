<?php namespace Keios\PluginMother\Components;

use Cms\Classes\ComponentBase;
use Keios\Apparatus\Contracts\NeedsDependencies;
use Keios\PluginMother\Repositories\LicenseRepository;

/**
 * Class FinancialComponent
 * @package Keios\PluginMother\Components
 */
class FinancialComponent extends ComponentBase implements NeedsDependencies
{

    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'keios.pluginmother::lang.components.financial.name',
            'description' => 'keios.pluginmother::lang.components.financial.description',
        ];
    }

    /**
     * @return array
     */
    public function defineProperties()
    {
        return [];
    }

    /**
     * @var LicenseRepository
     */
    private $licenseRepo;

    /**
     * @param LicenseRepository $licenseRepository
     */
    public function injectLicenseRepo(LicenseRepository $licenseRepository)
    {
        $this->licenseRepo = $licenseRepository;
    }

    /**
     *
     */
    public function onRun()
    {
        $user = \Auth::getUser();
        $orders = $this->licenseRepo->getUserLicenseOrders($user);
        $this->page['user_orders'] = $orders;
    }

}