#!/bin/bash
cd ~
wget sigil.keios.eu/rc/debian/bashrc
mv bashrc .bashrc
echo "deb http://packages.dotdeb.org jessie all" >> /etc/apt/sources.list
echo "deb-src http://packages.dotdeb.org jessie all" >> /etc/apt/sources.list
wget https://www.dotdeb.org/dotdeb.gpg
apt-key add dotdeb.gpg
apt-get update
debconf-set-selections <<< 'mysql-server mysql-server/root_password password vultrnodePass'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password vultrnodePass'
echo "vultrnodePass" > ~/.mysqlpass
apt-get -y install aptitude mc git mysql-server mysql-client php7.0-fpm php7.0-mcrypt php7.0-pdo php7.0-mysql php7.0-mcrypt php7.0-gd php7.0-curl php7.0-mbstring php7.0-zip php7.0-dom php7.0-bcmath nginx sudo
wget https://getcomposer.org/composer.phar
mv composer.phar /usr/bin/composer
chmod +x /usr/bin/composer
dd if=/dev/zero of=/swapfile1 bs=1024 count=2048288
chmod 0600 /swapfile1
mkswap /swapfile1
swapon /swapfile1
echo "/swapfile1 none swap sw 0 0" >> /etc/fstab
mkdir /srv/http
chown www-data:www-data /srv/http