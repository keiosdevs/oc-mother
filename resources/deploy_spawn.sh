#!/bin/bash

function installOctober {
    instance_name={{ instance_name }}
    project_key={{ project_key }}
    instance_domain={{ project_domain }}
    dbname={{ db_name }}
    cd /srv/http/
    yes | composer create-project october/october $instance_name dev-develop
    cd $instance_name
    mkdir plugins/keios
    cd plugins/keios/
    git clone -b develop https://keiosweb@bitbucket.org/keiosdevs/oc-spawn.git pluginspawn
    mv pluginspawn/resources/nginx.conf /etc/nginx/sites-available/$instance_name
    cd /srv/http/$instance_name
    composer update
    php artisan vendor:publish
    mysql -u root -pvultrnodePass -e "create schema $dbname;"
    sed -i "s/'password'  => ''/'password'  => 'vultrnodePass'/g" config/database.php
    sed -i "s/'database'  => 'database'/'database'  => '$dbname'/g" config/database.php
    sed -i "s/'mother.dev'/'october-market.keios.eu'/g" config/keios/pluginspawn/config.php
    echo "Registering permissions..."
    chown www-data:www-data * -R
    echo "Migrating database..."
    php artisan october:up
    echo "Upadting config..."
    echo "Registering Exchange Project..."
    php artisan exchange:register $project_key
    echo "Updating Exchange Plugins..."
    php artisan exchange:update
    cd /etc/nginx/sites-available/
    sed -i "s/PROJECT_ROOT/$instance_name/g" $instance_name
    sed -i "s/PROJECT_DOMAIN/$instance_domain/g" $instance_name
    ln -s /etc/nginx/sites-available/$instance_name /etc/nginx/sites-enabled/$instance_name
    systemctl restart nginx
}

installOctober

