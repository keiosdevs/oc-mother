<?php
/**
 * MotherPlugin routes
 *
 * @plugin keios.motherplugin
 */
Route::group(
    ['prefix' => '/api/v1', 'middleware' => []],
    function () {
        Route::any(
            '/authorize',
            function () {
                $controller = new \Keios\PluginMother\Controllers\ApiController();

                return $controller->authorize();
            }
        );
        Route::any(
            '/get-plugin',
            function () {
                $controller = new \Keios\PluginMother\Controllers\ApiController();

                return $controller->getZip();
            }
        );
        Route::any(
            '/get-theme',
            function () {
                $controller = new \Keios\PluginMother\Controllers\ApiController();

                return $controller->getZip('theme');
            }
        );

        Route::any(
            '/status',
            function () {
                $controller = new \Keios\PluginMother\Controllers\ApiController();

                return $controller->receiveUpdate();
            }
        );

        Route::any(
            '/check-plugin',
            function () {
                $controller = new \Keios\PluginMother\Controllers\ApiController();

                return $controller->checkPlugin();
            }
        );
        Route::any(
            '/check-theme',
            function () {
                $controller = new \Keios\PluginMother\Controllers\ApiController();

                return $controller->checkTheme();
            }
        );

        Route::any(
            '/project',
            function () {
                $controller = new \Keios\PluginMother\Controllers\ApiController();

                return $controller->getProjectDetails();
            }
        );

        Route::any(
            '/project/theme',
            function () {
                $controller = new \Keios\PluginMother\Controllers\ApiController();

                return $controller->getProjectTheme();
            }
        );

        Route::any(
            '/plugin/version',
            function () {
                $controller = new \Keios\PluginMother\Controllers\ApiController();

                return $controller->getPluginVersion();
            }
        );

        Route::any(
            '/plugin/changelog',
            function () {
                $controller = new \Keios\PluginMother\Controllers\ApiController();

                return $controller->getPluginChangelog();
            }
        );

        Route::any(
            '/plugins',
            function () {
                $controller = new \Keios\PluginMother\Controllers\ApiController();

                return $controller->getCurrentState();
            }
        );

        Route::any(
            '/theme/version',
            function () {
                $controller = new \Keios\PluginMother\Controllers\ApiController();

                return $controller->getThemeVersion();
            }
        );


        Route::any(
            '/themes',
            function () {
                $controller = new \Keios\PluginMother\Controllers\ApiController();

                return $controller->getCurrentThemesState();
            }
        );

        Route::any(
            '/hook/{slug}',
            function ($slug) {
                $controller = new \Keios\PluginMother\Controllers\ApiController();

                return $controller->receiveHook($slug);
            }
        );

        Route::any(
            '/theme-hook/{slug}',
            function ($slug) {
                $controller = new \Keios\PluginMother\Controllers\ApiController();

                return $controller->receiveThemeHook($slug);
            }
        );

    }
);
